import React from 'react'
import {
  TouchableOpacity,
  Text,
  View,
  StatusBar,
  Platform,
  NetInfo,
} from 'react-native'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import {
  Font,
  AppLoading,
  Asset,
  Constants,
  Updates,
} from 'expo'
import {
  store,
  persistor,
} from './app/redux/createStore'
import DefaultStack from './app/configs/Router'


const images = [
  require('./app/assets/images/bg2.png'),
  require('./app/assets/images/bg3.png'),
  require('./app/assets/images/bg4.png'),
  require('./app/assets/images/bg5.png'),
  require('./app/assets/images/mic.png'),
  require('./app/assets/images/modalBG.png'),
  require('./app/assets/images/welcomeBG.png'),
  require('./app/assets/images/footer/dailies-selected.png'),
  require('./app/assets/images/footer/dailies.png'),
  require('./app/assets/images/footer/episodes.png'),
  require('./app/assets/images/footer/episodes-selected.png'),
  require('./app/assets/images/footer/recordings.png'),
  require('./app/assets/images/footer/recordings-selected.png'),
  require('./app/assets/images/stars/dailiesBackground.png'),
  require('./app/assets/images/skills/gratitude.png'),
  require('./app/assets/images/skills/resilience.png'),
  require('./app/assets/images/skills/focus.png'),
  require('./app/assets/images/upArrow.png'),
  require('./app/assets/images/recordPulse.png'),
  require('./app/assets/images/stopPulse.png'),
  require('./app/assets/images/homeBG.png'),
  require('./app/assets/images/stars/star.png'),
  require('./app/assets/images/allAudios_mountain.png'),
  require('./app/assets/images/downArrow.png'),
  require('./app/assets/images/listen_on_spotify.png'),
  require('./app/assets/images/story.png'),
  require('./app/assets/images/story_color.png'),
  require('./app/assets/images/search_icon.png'),
  require('./app/assets/images/search_icon_color.png'),
  require('./app/assets/images/defaultListenBG.png'),
  require('./app/assets/images/community.png'),
  require('./app/assets/images/play_purple.png'),
  require('./app/assets/images/community-color.png'),
  require('./app/assets/images/text_create_icon.png'),
  require('./app/assets/images/image_create_icon.png'),
  require('./app/assets/images/Stars1.png'),
  require('./app/assets/images/checkMark.png'),
  require('./app/assets/images/starBackground.png'),
  require('./app/assets/images/onboarding/1.png'),
  require('./app/assets/images/onboarding/2.png'),
  require('./app/assets/images/onboarding/3.png'),
  require('./app/assets/images/onboarding/4.png'),
  require('./app/assets/images/onboarding/5.png'),
  require('./app/assets/images/Stars2.png'),
  require('./app/assets/images/Stars3.png'),
  require('./app/assets/images/defaultAudioBG.png'),
  require('./app/assets/images/cliffs.png'),
  require('./app/assets/images/start-recording.png'),
  require('./app/assets/images/stop-recording.png'),
  require('./app/assets/images/replayButton.png'),
  require('./app/assets/images/sliderThumbTrack.png'),
  require('./app/assets/images/explore.png'),
  require('./app/assets/images/explore_background.png'),
  require('./app/assets/images/explore-color.png'),
  require('./app/assets/images/app_icon_cover.png'),
  require('./app/assets/images/arrow-back.png'),
  require('./app/assets/images/atlas_icon.png'),
  require('./app/assets/images/bg-atlas-opacity.png'),
  require('./app/assets/images/brand-black.png'),
  require('./app/assets/images/brand-white.png'),
  require('./app/assets/images/cancel.png'),
  require('./app/assets/images/cancel2.png'),
  require('./app/assets/images/chat-comment.png'),
  require('./app/assets/images/chat-love-empty.png'),
  require('./app/assets/images/chat-love-filled.png'),
  require('./app/assets/images/chat-notification.png'),
  require('./app/assets/images/chat-people.png'),
  require('./app/assets/images/checked.png'),
  require('./app/assets/images/checkedGradient.png'),
  require('./app/assets/images/checkEmpty.png'),
  require('./app/assets/images/checkFilled.png'),
  require('./app/assets/images/close.png'),
  require('./app/assets/images/closeEmpty.png'),
  require('./app/assets/images/default-profile-picture.png'),
  require('./app/assets/images/down-arrow-white.png'),
  require('./app/assets/images/down-arrow.png'),
  require('./app/assets/images/empty.png'),
  require('./app/assets/images/footer-checkin.png'),
  require('./app/assets/images/footer-checkins.png'),
  require('./app/assets/images/footer-pulse-gradient.png'),
  require('./app/assets/images/footer-pulse.png'),
  require('./app/assets/images/header-private.png'),
  require('./app/assets/images/header-profile.png'),
  require('./app/assets/images/logo.png'),
  require('./app/assets/images/marker.png'),
  require('./app/assets/images/search.png'),
  require('./app/assets/images/settings-icon.png'),
  require('./app/assets/images/settings.png'),
  require('./app/assets/images/triangle.png'),
  require('./app/assets/images/yesBG.png'),
  require('./app/assets/images/noBG.png'),
  require('./app/assets/images/yesTriangle.png'),
  require('./app/assets/images/noTriangle.png'),
  require('./app/assets/images/poll.png'),
  require('./app/assets/images/poll-color.png'),
  require('./app/assets/images/private.png'),
  require('./app/assets/images/private-color.png'),
  require('./app/assets/images/home.png'),
  require('./app/assets/images/home-color.png'),
  require('./app/assets/images/xbutton.png'),
  require('./app/assets/images/actionring1.png'),
  require('./app/assets/images/actionring2.png'),
  require('./app/assets/images/actionring3.png'),
  require('./app/assets/images/sound-bars.png'),
  require('./app/assets/images/star.png'),
  require('./app/assets/images/atlas_gold.png'),
  require('./app/assets/images/goldCardEarned.png'),
  require('./app/assets/images/goldonboard1.png'),
  require('./app/assets/images/thisisme.png'),
  require('./app/assets/images/education.png'),
  require('./app/assets/images/education-color.png'),
  require('./app/assets/images/anonMessage.png'),
  require('./app/assets/images/imageUpload.png'),
  require('./app/assets/images/upvote.png'),
  require('./app/assets/images/atlasPledge.png'),
  require('./app/assets/images/upvote-colored.png'),
  require('./app/assets/images/sameBoat.png'),
  require('./app/assets/images/sameBoat-colored.png'),
  require('./app/assets/images/youShared.png'),
  require('./app/assets/images/arrow-back-white.png'),
  require('./app/assets/images/step1.png'),
  require('./app/assets/images/step2.png'),
  require('./app/assets/images/step3.png'),
  require('./app/assets/images/back-angle.png'),
  require('./app/assets/images/forward-angle.png'),
  require('./app/assets/images/play-button.png'),
  require('./app/assets/images/pause-button.png'),
  require('./app/assets/images/sound-bars-inactive.png'),
]

const cacheResourcesAsync = async () => {
  const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync())

  try {
    await Promise.all(cacheImages)
  } catch (e) {
    console.error(e)
  }
}

export default class App extends React.Component {
  state = {
    fontLoaded: false,
    assetsLoaded: false,
    isConnected: false,
  }

  async componentDidMount() {
    try {
      await Font.loadAsync({
        'gt-walsheim-bold': require('./app/assets/fonts/GTWalsheim/gt-walsheim-bold.ttf'),
        'gt-walsheim-light': require('./app/assets/fonts/GTWalsheim/gt-walsheim-light.ttf'),
        'gt-walsheim-medium': require('./app/assets/fonts/GTWalsheim/gt-walsheim-medium.ttf'),
        'gt-walsheim-regular': require('./app/assets/fonts/GTWalsheim/gt-walsheim-regular.ttf'),
        'maisonNeue-Light': require('./app/assets/fonts/MaisonNeue/maisonNeue-Light.ttf'),
      })
    } catch (e) {
      console.error(e)
    }

    this.setState({ fontLoaded: true })

    // Check for internet connection
    NetInfo.isConnected.fetch().then((isConnected) => {
      this.setState({ isConnected })
    })

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange,
    )
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange,
    )
  }

  deleteUserKey = async () => {
  }

  handleFirstConnectivityChange = (isConnected) => {
    this.setState({ isConnected })
  }

  render() {
    // Uncomment this line of code if you're debugging auth states
    // this.deleteUserKey()
    if (!this.state.assetsLoaded) {
      return (
        <AppLoading
          startAsync={cacheResourcesAsync}
          onFinish={() => this.setState({ assetsLoaded: true })}
          onError={console.warn}
        />
      )
    }


    console.disableYellowBox = true
    if (this.state.isConnected) {
      const barStyle = Platform.OS === 'ios' && Constants.platform.ios.model.toLowerCase() === 'iphone x'
        ? 'light-content' : 'dark-content'

      return (
        this.state.fontLoaded
        && (
        <Provider store={store}>
          <PersistGate
            loading={null}
            persistor={persistor}
          >
            <StatusBar barStyle={barStyle} />
            <ActionSheetProvider>
              <DefaultStack />
            </ActionSheetProvider>
          </PersistGate>
        </Provider>
        )
      )
    }
    return (
      this.state.fontLoaded
        && (
        <View style={{
          width: '100%',
          justifyContent: 'center',
          flex: 1,
        }}
        >
          <Text style={{
            fontFamily: 'gt-walsheim-regular',
            fontSize: 18,
            color: '#4A4A4A',
            textAlign: 'center',
          }}
          >
            No connection detected. Atlas requires an internet connection to work.
          </Text>
          <TouchableOpacity
            onPress={() => Updates.reload()}
            style={{ marginTop: 20 }}
          >
            <Text style={{
              fontFamily: 'gt-walsheim-bold',
              fontSize: 18,
              color: 'red',
              textAlign: 'center',
            }}
            >
              Retry
            </Text>
          </TouchableOpacity>
        </View>
        )
    )
  }
}
