import React from 'react'
import { View, Text, Image } from 'react-native'

class AudioCard extends React.Component {
  render() {
    return (
      <View style={styles.wrapper}>
        <Text style={styles.title}>
          {this.props.title}
          {' '}
completed
        </Text>
        <Image style={styles.image} source={require('../../assets/images/checkMark.png')} />
      </View>
    )
  }
}

const styles = {
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontFamily: 'gt-walsheim-medium',
    textTransform: 'uppercase',
    fontSize: 14,
    letterSpacing: 3.4,
    color: '#FFF',
  },
  image: {
    width: 12,
    height: 9,
    bottom: -4,
  },
}

export default AudioCard
