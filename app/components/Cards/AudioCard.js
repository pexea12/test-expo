import React from 'react'
import {
  View, Text, TouchableOpacity, Image,
} from 'react-native'

class AudioCard extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={{ flexDirection: 'row' }}>
        <View style={styles.wrapper}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.subtitle}>{this.props.subtitle}</Text>
        </View>
        <View style={styles.playBtn}>
          <Image source={require('../../assets/images/playButton.png')} style={styles.playImg} />
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = {
  wrapper: {
    fontFamily: 'gt-walsheim-regular',
    flex: 3,
  },
  title: {
    fontFamily: 'gt-walsheim-medium',
    textTransform: 'capitalize',
    fontSize: 21,
    color: '#FFF',
  },
  subtitle: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 16,
    opacity: 0.7,
    color: '#FFF',
  },
  playBtn: {
    justifyContent: 'center',
  },
  playImg: {
    width: 54,
    height: 54,
  },
}

export default AudioCard
