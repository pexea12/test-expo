import React, { Component } from 'react'
import findIndex from 'lodash/findIndex'
import { SafeAreaView, View } from 'react-native'

const NextBtn = (BtnComponent, handler) => props => (
  <BtnComponent {...props} onPress={event => handler.onPress(event)} />
)

class Wizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentStepIndex: 0,
    }
  }

  currentStep = () => {
    const { currentStepIndex } = this.state
    const { steps } = this.props
    return steps[currentStepIndex]
  };

  handleOnPressButton = () => {
    const { currentStepIndex } = this.state
    const { steps } = this.props
    const nextStepIndex = currentStepIndex + 1
    if (!steps[currentStepIndex].blockNext) {
      if (this.onLastStep()) this.props.handleSubmitWizard()
      else this.setState({ currentStepIndex: nextStepIndex })
    }
  };

  onHandleNavChange = (routeName) => {
    const currentStepIndex = findIndex(this.props.steps, step => step.routeName === routeName)
    this.setState({ currentStepIndex })
  };

  onHandleNavRef = (navigator) => {
    this.navigator = navigator
  };

  onLastStep = () => {
    const { steps } = this.props
    const { currentStepIndex } = this.state
    return currentStepIndex + 1 === steps.length
  };

  renderNavigationActions = () => {
    const { steps } = this.props
    const { currentStepIndex } = this.state
    this.currentStep()

    return (
      <SafeAreaView style={styles.safeAreaView}>
        <View style={{ paddingBottom: 30 }}>
          {steps[currentStepIndex].nextBtns.map((btn, index) => {
            const NextBtnComponent = NextBtn(btn.wrapper, {
              onPress: (e) => {
                if (btn.callback) btn.callback(e)
                this.handleOnPressButton()
              },
            })
            return <NextBtnComponent key={index}>next</NextBtnComponent>
          })}
        </View>
      </SafeAreaView>
    )
  };

  render() {
    const current = this.currentStep()
    return (
      <View style={styles.container}>
        {current.component}
        {this.renderNavigationActions()}
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: '100%',
  },
  safeAreaView: {
    alignItems: 'center',
    borderColor: 'transparent',
    borderTopWidth: 1,
    paddingHorizontal: 15,
    paddingTop: 15,
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
  },
}

export default Wizard
