import React, { Component } from 'react'
import { Text, View, TouchableHighlight } from 'react-native'

class Step extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { content, handleOnPressButton } = this.props

    return (
      <TouchableHighlight onPress={handleOnPressButton} style={styles.touchableHighlight}>
        {content}
      </TouchableHighlight>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  touchableHighlight: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowRadius: 20,
    shadowColor: 'white',
    shadowOpacity: 0.75,
    width: 200,
    margin: 15,
    borderRadius: 25,
    padding: 15,
  },
}

export default Step
