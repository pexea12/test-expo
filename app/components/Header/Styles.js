const Styles = {
  flexRow: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    paddingLeft: '3%',
    paddingRight: '3%',
    paddingTop: 27,
    height: 80,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    backgroundColor: '#fff',
  },
  flexRowLarge: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingRight: '3%',
    paddingTop: 15,
    height: 100,
    backgroundColor: 'white',
    borderBottomWidth: 0,
    borderBottomColor: '#D8D8D8',
    backgroundColor: '#fff',
  },
  backImage: {
    width: 18,
    height: 16,
  },
  colorBlack: {
    color: '#343434',
  },
  colorGray: {
    color: '#A8A8A8',
  },
  colorPink: {
    color: '#F1636E',
  },
  center: {
    alignItems: 'center',
  },
  left: {
    alignItems: 'flex-end',
  },
  widthCenter: {
    width: '40%',
  },
  width30: {
    padding: 12,
    left: -10,
  },
  textRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 16,
  },
  textMedium: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 16,
  },
  textLarge: {
    fontFamily: 'gt-walsheim-bold',
    fontSize: 32,
  },
  textBold: {
    fontFamily: 'gt-walsheim-bold',
    fontSize: 15,
    marginTop: 2,
  },
  mainTitle: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 20,
    color: '#000000',
  },
  textLargeWrapper: {
    marginTop: 10,
  },
  textRight: {
    right: -22,
    top: 17,
  },
}

export default Styles
