import React from 'react'
import {
  View, Text, TouchableOpacity, Image,
} from 'react-native'
import Styles from './Styles'

class Header extends React.Component {
  constructor(props) {
    super(props)
  }

  renderSave() {
    switch (this.props.headerType) {
      case 'backArrow':
        return (
          <View style={[Styles.flexRow]}>
            <TouchableOpacity style={Styles.width30} onPress={this.props.onCancel}>
              <Image
                source={require('../../assets/images/arrow-back.png')}
                style={Styles.backImage}
              />
            </TouchableOpacity>
            <View style={[Styles.center, Styles.widthCenter]}>
              <Text style={[Styles.mainTitle]}>{this.props.title}</Text>
            </View>
            <TouchableOpacity
              style={[Styles.width30, Styles.left]}
              onPress={this.props.onSave}
              disabled={this.props.disabledRight}
            >
              <Text style={[Styles.textBold, Styles.colorPink]}>
                {this.props.rightText || 'Save'}
              </Text>
            </TouchableOpacity>
          </View>
        )
      case 'large':
        return (
          <View style={[Styles.flexRowLarge]}>
            <View style={Styles.textLargeWrapper}>
              <Text style={[Styles.textLarge]}>{this.props.title}</Text>
            </View>
            <TouchableOpacity
              style={[Styles.width30, { bottom: 10 }]}
              onPress={this.props.onSave}
              disabled={this.props.disabledRight}
            >
              <Text style={[Styles.textBold, Styles.colorPink, Styles.textRight]}>
                {this.props.rightText || 'Save'}
              </Text>
            </TouchableOpacity>
          </View>
        )
      default:
        return (
          <View style={[Styles.flexRow]}>
            <TouchableOpacity style={Styles.width30} onPress={this.props.onCancel}>
              <Text style={[Styles.textRegular, Styles.colorGray]}>
                {this.props.leftText || 'Cancel'}
              </Text>
            </TouchableOpacity>
            <View style={[Styles.center, Styles.widthCenter, { left: -20 }]}>
              <Text style={[Styles.mainTitle]}>{this.props.title}</Text>
            </View>
            <TouchableOpacity
              style={[]}
              onPress={this.props.onSave}
              disabled={this.props.disabledRight}
            >
              <Text style={[Styles.textBold, Styles.colorPink]}>
                {this.props.rightText || 'Save'}
              </Text>
            </TouchableOpacity>
          </View>
        )
    }
  }

  render() {
    return this.renderSave()
  }
}

export default Header
