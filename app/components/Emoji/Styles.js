const Styles = {
  flexRow: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    paddingLeft: 10,
    paddingRight: 10,
    height: '100%',
    backgroundColor: '#F3F3F3',
    borderTopWidth: 1,
    borderTopColor: '#D8D8D8',
    flex: 4,
  },
  flexView: {
    height: 60,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '50%',
  },
  row: {
    flexDirection: 'row',
  },
  title: {
    minWidth: '100%',
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
    color: '#4C4C4C',
    marginTop: 10,
    marginBottom: 15,
    paddingLeft: 10,
  },
  colorRed: {
    color: '#E74C3C',
  },
  colorGreen: {
    color: '#2ECC71',
  },
  colorOrange: {
    color: '#F89406',
  },
  colorBlack: {
    color: '#575757',
  },
  textMedium: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 15,
    marginTop: 2,
  },
  check: {
    height: 23,
    width: 23,
    marginRight: 5,
  },
  emoji: {
    height: 20,
    width: 20,
  },
}

export default Styles
