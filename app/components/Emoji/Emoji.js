import React from 'react'
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Keyboard,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native'
import filter from 'lodash/filter'
import Styles from './Styles'

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 150
  return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom
}

class Emoji extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      keyboard: false,
      shortHeight: 0,
      count: 10,
    }

    this._keyboardDidShow = this.keyboardDidShow.bind(this)
    this._keyboardDidHide = this.keyboardDidHide.bind(this)
    this._onScroll = this.onScroll.bind(this)
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow(e) {
    const shortHeight = Dimensions.get('window').height - e.endCoordinates.height
    this.setState({ keyboard: true, shortHeight })
  }

  keyboardDidHide() {
    this.setState({ keyboard: false })
  }

  onScroll({ nativeEvent }) {
    if (isCloseToBottom(nativeEvent)) {
      this.setState({ count: this.state.count + 30 })
    }
  }

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  renderMoods(moods) {
    return moods.map((item) => {
      let color
      if (Number(item.quotient) <= -0.3) {
        color = Styles.colorRed
      } else if (Number(item.quotient) >= 0.3) {
        color = Styles.colorGreen
      } else {
        color = Styles.colorOrange
      }

      return (
        <View style={Styles.flexView}>
          <TouchableOpacity
            style={Styles.row}
            onPress={() => {
              this.props.onPress(item)
            }}
          >
            {item.checked ? (
              <Image source={require('../../assets/images/checkFilled.png')} style={Styles.check} />
            ) : (
              <Image source={require('../../assets/images/checkEmpty.png')} style={Styles.check} />
            )}
            <Text style={[Styles.textMedium, item.checked ? color : Styles.colorBlack]}>
              {this.capitalize(item.mood)}
              {' '}
            </Text>
            <Image style={Styles.emoji} source={{ uri: item.emoji_url }} />
          </TouchableOpacity>
        </View>
      )
    })
  }

  render() {
    const { keyboard, shortHeight, count } = this.state
    const populars = filter(this.props.emojis, { popular: true })
    const allMoods = filter(this.props.emojis, { popular: false })
    allMoods.length = count

    return (
      <ScrollView keyboardShouldPersistTaps keyboardDismissMode="on-drag" onScroll={this._onScroll}>
        <TouchableWithoutFeedback onPress={this.props.fullView}>
          <View style={keyboard ? [Styles.flexRow, { height: shortHeight }] : Styles.flexRow}>
            <Text style={Styles.title}>Popular</Text>
            {this.renderMoods(populars)}
            <Text style={Styles.title}>All Moods</Text>
            {(!keyboard || this.props.isSearching) && this.renderMoods(allMoods)}
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    )
  }
}

export default Emoji
