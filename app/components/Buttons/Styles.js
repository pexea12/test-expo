const Styles = {
  btn: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1.0,
    width: '100%',
    marginBottom: '5%',
  },
  text: {
    fontFamily: 'gt-walsheim-regular',
    letterSpacing: -0.2,
    fontSize: 17,
    paddingTop: '3.5%',
    paddingBottom: '5.5%',
    color: 'white',
    textAlign: 'center',
  },
}

export default Styles
