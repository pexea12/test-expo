import React from 'react'
import { Text } from 'react-native'
import { LinearGradient } from 'expo'
import Styles from './Styles'

class GradientButton extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <LinearGradient
        colors={this.props.disabled ? ['#979797', '#979797'] : ['#F54EA2', '#FA638B', '#F36070']}
        start={[0, 1]}
        end={[1, 0]}
        style={this.props.style ? [Styles.btn, this.props.style.btn] : Styles.btn}
      >
        <Text style={this.props.style ? [Styles.text, this.props.style.text] : Styles.text}>
          {this.props.text}
        </Text>
      </LinearGradient>
    )
  }
}

export default GradientButton
