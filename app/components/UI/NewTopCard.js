import React from 'react'
import { View } from 'react-native'

class NewTopCard extends React.Component {
  render() {
    return <View style={styles.wrapper}>{this.props.content}</View>
  }
}

const styles = {
  wrapper: {
    backgroundColor: 'rgba(255,255,255, 0.3)',
    margin: 15,
    marginTop: 40, // TODO: Validate topbar height for > iPhone X
    padding: 20,
    borderRadius: 8,
  },
}

export default NewTopCard
