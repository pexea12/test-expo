import React from 'react'
import { Image, Dimensions } from 'react-native'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

class StarBackground extends React.Component {
  render() {
    return (
      <Image
        resizeMode="contain"
        source={require('../../assets/images/stars/dailiesBackground.png')}
        style={styles.starImage}
      />
    )
  }
}

const styles = {
  starImage: {
    position: 'absolute',
    top: -windowHeight * 0.09,
    left: -windowHeight * 0.02,
    opacity: 0.95,
    width: windowWidth * 1.08,
  },
}

export default StarBackground
