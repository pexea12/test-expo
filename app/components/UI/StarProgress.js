import React from 'react'
import {
  View, AppState, Animated, Dimensions,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Svg } from 'expo'
import posed from 'react-native-pose'
import { listenConstellations } from '../../redux/checkIn/actions'
import AnimatedStar from './AnimatedStar'
import AnimatedLine from './AnimatedLine'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const isiPhoneX = windowHeight > 800
const isiPhoneSE = windowHeight < 600

const {
  Line,
  Mask,
  Circle,
  Image,
  Defs,
  Stop,
  RadialGradient,
  Ellipse,
  Text,
} = Svg
// An array of XY Coordinates
// Below is an example of a seven point constellation
const defaultConstellation = [
  [0.05, 0.05],
  [0.13, 0.15],
  [0.26, 0.6],
  [0.4, 0.75],
  [0.53, 0.9],
  [0.68, 0.4],
  [0.83, 0.8],
]
const weekDays = ['SUN', 'MON', 'TUES', 'WED', 'THURS', 'FRI', 'SAT']
const completed = [false, true, true, false, true, true, true]

const AnimatedImage = Animated.createAnimatedComponent(Image)
// TO-DO: A function to generate these coordinates randomly for a NEW constellation each Sunday.

class StarProgress extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      constellations: [],
      progress: [],
    }
  }

  componentDidMount() {
    this.props.listenConstellations()
    this.getConstellations(this.props.constellations)
  }

  getConstellations = (constellations = []) => {
    // Problem Line
    // pendingRecordings.forEach(recording => cards.push(recording));
    this.setState({ constellations })
  };

  renderLine = (coordinates, index, complete, isDaily) => (
    <AnimatedLine index={index} coordinates={coordinates} isDaily={isDaily} complete={complete} />
  );

  renderStar = (coordinates, index, complete, isDaily) => (
    <AnimatedStar index={index} coordinates={coordinates} isDaily={isDaily} complete={complete} />
  );

  renderTextLabel = (coordinates, index, complete) => {
    // Image is spaced (in respect to line coordinate) as (x-13,y-15)
    const xCoord = parseInt(coordinates[index][0], 10) - 13
    const yCoord = parseInt(coordinates[index][1], 10) + 30
    return (
      <Text
        x={`${xCoord}`}
        y={`${yCoord}`}
        fontSize="11"
        fontWeight="bold"
        fontFamily="gt-walsheim-bold"
        fill={complete ? 'white' : 'rgba(255,255,255,0.3)'}
      >
        {weekDays[index]}
      </Text>
    )
  };

  render() {
    const { constellations, progress } = this.state
    const { isDaily } = this.props

    if (constellations.length > 0) {
      const temp = []
      const temp2 = []
      constellations.forEach((item) => {
        temp.push(item)
      })
      temp.map((item) => {
        temp2.push(Object.values(item))
      })
    }
    chosenConstellation = defaultConstellation

    const finalConstellation = chosenConstellation.map(subarray => subarray.map(number => number * windowWidth))

    const finalProgress = this.props.progress

    return (
      <View
        style={{
          marginLeft:
            windowWidth * 0.035 + isiPhoneX * windowWidth * 0.035 + isiPhoneSE * windowWidth * 0.04,
        }}
      >
        <Svg height={windowWidth} width={windowWidth}>
          {this.renderLine(finalConstellation, 0, finalProgress[0], isDaily)}
          {this.renderLine(finalConstellation, 1, finalProgress[1] && finalProgress[2], isDaily)}
          {this.renderLine(finalConstellation, 2, finalProgress[2] && finalProgress[3], isDaily)}
          {this.renderLine(finalConstellation, 3, finalProgress[3] && finalProgress[4], isDaily)}
          {this.renderLine(finalConstellation, 4, finalProgress[4] && finalProgress[5], isDaily)}
          {this.renderLine(finalConstellation, 5, finalProgress[5] && finalProgress[6], isDaily)}

          {this.renderStar(finalConstellation, 0, finalProgress[0], isDaily)}

          {this.renderTextLabel(finalConstellation, 0, finalProgress[0], isDaily)}

          {this.renderStar(finalConstellation, 1, finalProgress[1], isDaily)}
          {this.renderTextLabel(finalConstellation, 1, finalProgress[1], isDaily)}

          {this.renderStar(finalConstellation, 2, finalProgress[2], isDaily)}
          {this.renderTextLabel(finalConstellation, 2, finalProgress[2], isDaily)}

          {this.renderStar(finalConstellation, 3, finalProgress[3], isDaily)}
          {this.renderTextLabel(finalConstellation, 3, finalProgress[3], isDaily)}

          {this.renderStar(finalConstellation, 4, finalProgress[4], isDaily)}
          {this.renderTextLabel(finalConstellation, 4, finalProgress[4], isDaily)}

          {this.renderStar(finalConstellation, 5, finalProgress[5], isDaily)}
          {this.renderTextLabel(finalConstellation, 5, finalProgress[5], isDaily)}

          {this.renderStar(finalConstellation, 6, finalProgress[6], isDaily)}
          {this.renderTextLabel(finalConstellation, 6, finalProgress[6], isDaily)}
        </Svg>
      </View>
    )
  }
}

const styles = {}

const mapStateToProps = state => ({
  constellations: state.checkIn.constellations,
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    listenConstellations,
  },
  dispatch,
)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StarProgress)
