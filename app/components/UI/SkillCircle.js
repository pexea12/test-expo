import React from 'react'
import {
  View, Animated, Text, ImageBackground,
} from 'react-native'

class SkillCircle extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pointsValue: this.props.points - 1,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        pointsValue: this.props.points,
      })
    }, 800)
  }

  getSkill = () => {
    switch (this.props.skill) {
      case 'gratitude':
        return require('../../assets/images/skills/gratitude.png')
      case 'focus':
        return require('../../assets/images/skills/focus.png')
      case 'resilience':
        return require('../../assets/images/skills/resilience.png')
      default:
        return require('../../assets/images/skills/gratitude.png')
    }
  };

  render() {
    const theURI = this.getSkill()
    if (this.props.animated) {
      return (
        <ImageBackground
          resizeMode="contain"
          source={theURI}
          style={{
            width: this.props.size,
            height: this.props.size,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Animated.Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontFamily: 'gt-walsheim-regular',
                shadowOffset: { width: 2, height: 4 },
                shadowColor: 'rgba(92, 92, 92, 0.50)',
                shadowOpacity: 0.9,
                right: -1,
                fontSize: this.props.size * 0.5,
              }}
            >
              {this.state.pointsValue}
            </Animated.Text>
          </View>
        </ImageBackground>
      )
    }
    return (
      <ImageBackground
        resizeMode="contain"
        source={theURI}
        style={{
          width: this.props.size,
          height: this.props.size,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text
            style={{
              textAlign: 'center',
              color: 'white',
              fontFamily: 'gt-walsheim-regular',
              shadowOffset: { width: 2, height: 4 },
              shadowColor: 'rgba(92, 92, 92, 0.50)',
              shadowOpacity: 0.9,
              right: -1,
              fontSize: this.props.size * 0.5,
            }}
          >
            {this.props.points}
          </Text>
        </View>
      </ImageBackground>
    )
  }
}

export default SkillCircle
