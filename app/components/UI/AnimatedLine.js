import React from 'react'
import { Animated } from 'react-native'
import { 
  Constants,
  Svg, 
} from 'expo'

const AnimatedView = Animated.createAnimatedComponent(Svg.Line)
const iPhoneModelYear = Constants.deviceYearClass
const shouldAnimate = iPhoneModelYear > 2015

class AnimatedLine extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      scaleValue: new Animated.Value(0.05),
    }
  }

  componentWillReceiveProps(props) {
    this.setState({
      complete: props.complete,
    })
    if (!shouldAnimate) {
      if (props.complete) {
        this.setState({ scaleValue: new Animated.Value(1.0) })
      } else {
        this.setState({ scaleValue: new Animated.Value(0.2) })
      }
    }
  }

  render() {
    if (shouldAnimate) {
      Animated.timing(this.state.scaleValue, {
        toValue: 0.2 + this.state.complete * 0.8,
        duration: 600,
        delay: this.props.index * 375 * this.props.isDaily,
      }).start()
    }

    const xCoord = parseInt(this.props.coordinates[this.props.index + 1][0], 10)
    const yCoord = parseInt(this.props.coordinates[this.props.index + 1][1], 10) - 2
    return (
      <AnimatedView
        x1={this.props.coordinates[this.props.index][0]}
        y1={this.props.coordinates[this.props.index][1]}
        x2={`${xCoord}`}
        y2={`${yCoord}`}
        stroke="#BAF0FF"
        opacity={this.state.scaleValue}
        strokeWidth="1.5"
      />
    )
  }
}

const styles = {}

export default AnimatedLine
