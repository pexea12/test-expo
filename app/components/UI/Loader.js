import React, { Component } from 'react'
import {
  StyleSheet, 
  View, 
  Modal, 
  ActivityIndicator, 
  Dimensions,
} from 'react-native'

const windowHeight = Dimensions.get('window').height

const Loader = (props) => {
  const { 
    loading, 
    style,
  } = props

  return (
    <Modal
      transparent
      animationType="none"
      visible={loading}
      onRequestClose={() => {}}
    >
      <View style={[
        styles.modalBackground,
        style,
      ]}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator animating={loading} />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    height: windowHeight - 60,
    zIndex: 90,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})

export default Loader
