import React from 'react'
import { Animated } from 'react-native'
import { 
  Constants,
  Svg, 
} from 'expo'

const AnimatedImage = Animated.createAnimatedComponent(Svg.Image)
const starWidth = 30
const pulseSize = 5

const iPhoneModelYear = Constants.deviceYearClass
const shouldAnimate = iPhoneModelYear > 2015

class AnimatedStar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      scaleValue: new Animated.Value(0.1 + !shouldAnimate * 0.9),
      widthValue: new Animated.Value(starWidth),
    }
  }

  comoponentWillUnmount() {
    this.setState({
      stopAnimation: true,
    })
  }

  componentWillReceiveProps(props) {
    this.setState({
      complete: props.complete,
      widthValue: new Animated.Value(starWidth),
    })
  }

  render() {
    const durationOne = Math.floor(Math.random() * (3000 - 1500 + 1)) + 1500
    const delayTwo = Math.floor(Math.random() * (1500 - 500 + 1)) + 500
    const isComplete = this.state.complete
    if (shouldAnimate) {
      Animated.sequence([
        Animated.timing(this.state.scaleValue, {
          toValue: 1.0 * isComplete,
          duration: 700,
          delay: this.props.index * 350 * this.props.isDaily,
        }),
        Animated.timing(this.state.scaleValue, {
          toValue: 1 * isComplete + 0.1,
          duration: 700,
          delay: 1000,
        }),
      ]).start()
    }
    const xCoord = parseInt(this.props.coordinates[this.props.index][0], 10) - 13
    const yCoord = parseInt(this.props.coordinates[this.props.index][1], 10) - 15
    return (
      <AnimatedImage
        x={`${xCoord}`}
        y={`${yCoord}`}
        width={this.props.complete ? this.state.widthValue : starWidth}
        height={this.props.complete ? this.state.widthValue : starWidth}
        preserveAspectRatio="xMidYMid slice"
        opacity={this.props.complete ? this.state.scaleValue : 0.1}
        href={require('../../assets/images/stars/star.png')}
        clipPath="url(#clip)"
      />
    )
  }
}

export default AnimatedStar
