import { firebase } from '../../configs/Firebase'
import { processErr } from '../../configs/Helpers'

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const ACCEPTED_TERM = 'ACCEPTED_TERM'


const db = firebase.firestore()

const userObserver = {
  userId: null,
  observer: null,
}

export const onUserChanged = (userId) => (dispatch) => {
  if (userId !== userObserver.userId) {
    if (userObserver.observer)
      userObserver.observer() // unscribe

    userObserver.userId = userId
    userObserver.observer = db.collection('users')
      .doc(userId)
      .onSnapshot(
        (doc) => {
          const user = doc.data()
          dispatch({
            type: LOGIN,
            payload: {
              ...user,
              id: userId,
            },
          })
        },
        (e) => processErr(e, 'Unable to login'),
      )
  }
}

export const acceptedTerm = payload => ({
  type: ACCEPTED_TERM,
  payload,
})

export const logout = () => ({
  type: LOGOUT,
})
