import { combineReducers } from 'redux'

import auth from './auth/reducer'
import school from './school/reducer'
import checkIn from './checkIn/reducer'
import dailies from './dailies/reducer'

const reducers = combineReducers({
  auth,
  school,
  checkIn,
  dailies,
})

export default reducers
