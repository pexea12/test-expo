import { PUT } from './actions'

const initialState = {
  user: null,
  acceptedTerm: false,
}

export default function reducer(state = initialState, action) {
  // NOTE: DEBUG REDUX. Don't remove
  // console.warn(action.type, JSON.stringify(action, (key, value) => {
  // if (key === 'socket') return 'socket val'
  // return value
  // }, 2))

  switch (action.type) {
    case PUT:
      return { ...state, currentSchool: action.payload }
    default:
      return state
  }
}
