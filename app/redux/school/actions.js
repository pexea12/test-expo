export const PUT = 'PUT'
export const STORE = 'STORE'

export const putSchool = payload => ({
  type: PUT,
  payload,
})
