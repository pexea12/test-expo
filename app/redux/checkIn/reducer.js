import {
  LOADING,
  UPDATE,
  UPDATE_PRIVATE,
  STORE,
  COMMENTS,
  MEMBERS,
  POLLS,
  POSTS,
  POLLSTORIES,
  STORIES,
  VOTES,
  CARDS,
  PERSONAL_CARDS,
  LESSONS,
  STACKS,
  STACK_CARDS,
  GOLD_CARDS,
  GLOBAL_GOLD_CARDS,
  MYRECORDINGS,
  UPDATE_MY_RECORDING,
  UPDATE_PENDING_RECORDING,
  COMPLETED_CARD_IDS,
  USER_STATS,
  CONSTELLATION,
} from './actions'

const initialState = {
  loading: false,
  checkIns: false,
  privateCheckIns: false,
  comments: [],
  emojis: [],
  members: [],
  polls: [],
  pollstories: [],
  stories: [],
  votes: [],
  cards: [],
  personal_cards: [],
  lessons: [],
  gold_cards: [],
  posts: [],
  global_gold_cards: [],
  stacks: [],
  myRecordings: [],
  pendingRecordings: [],
  stack_cards: [],
  completedCardIds: [],
  user_stats: [],
  constellations: [],
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING:
      return { ...state, loading: action.payload }
    case UPDATE:
      return { ...state, checkIns: action.payload }
    case UPDATE_PRIVATE:
      return { ...state, privateCheckIns: action.payload }
    case STORE:
      return { ...state, emojis: action.payload }
    case COMMENTS:
      return { ...state, comments: action.payload }
    case MEMBERS:
      return { ...state, members: action.payload }
    case POLLS:
      return { ...state, polls: action.payload }
    case POSTS:
      return { ...state, posts: action.payload }
    case POLLSTORIES:
      return { ...state, pollstories: action.payload }
    case STORIES:
      return { ...state, stories: action.payload }
    case VOTES:
      return { ...state, votes: action.payload }
    case CARDS:
      return { ...state, cards: action.payload }
    case MYRECORDINGS:
      return { ...state, myRecordings: action.payload }
    case PERSONAL_CARDS:
      return { ...state, personal_cards: action.payload }
    case LESSONS:
      return { ...state, lessons: action.payload }
    case GOLD_CARDS:
      return { ...state, gold_cards: action.payload }
    case GLOBAL_GOLD_CARDS:
      return { ...state, global_gold_cards: action.payload }
    case STACKS:
      return { ...state, stacks: action.payload }
    case STACK_CARDS:
      return { ...state, stack_cards: action.payload }
    case UPDATE_MY_RECORDING:
      return { ...state, myRecordings: action.payload }
    case UPDATE_PENDING_RECORDING:
      return { ...state, pendingRecordings: action.payload }
    case COMPLETED_CARD_IDS:
      return { ...state, completedCardIds: action.payload }
    case USER_STATS:
      return { ...state, user_stats: action.payload }
    case CONSTELLATION:
      return { ...state, constellations: action.payload }
    default:
      return state
  }
}

export default reducer