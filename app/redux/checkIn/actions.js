import find from 'lodash/find'
import { firebase } from '../../configs/Firebase'
import { processErr } from '../../configs/Helpers'

export const LOADING = 'LOADING'
export const UPDATE = 'UPDATE'
export const UPDATE_PRIVATE = 'UPDATE_PRIVATE'
export const STORE = 'STORE'
export const COMMENTS = 'COMMENTS'
export const MEMBERS = 'MEMBERS'
export const POLLS = 'POLLS'
export const STORIES = 'STORIES'
export const POLLSTORIES = 'POLLSTORIES'
export const VOTES = 'VOTES'
export const CARDS = 'CARDS'
export const POSTS = 'POSTS'
export const PERSONAL_CARDS = 'PERSONAL_CARDS'
export const LESSONS = 'LESSONS'
export const GOLD_CARDS = 'GOLD_CARDS'
export const GLOBAL_GOLD_CARDS = 'GLOBAL_GOLD_CARDS'
export const STACKS = 'STACKS'
export const STACK_CARDS = 'STACK_CARDS'
export const MYRECORDINGS = 'MYRECORDINGS'
export const UPDATE_MY_RECORDING = 'UPDATE_MY_RECORDING'
export const UPDATE_PENDING_RECORDING = 'UPDATE_PENDING_RECORDING'
export const COMPLETED_CARD_IDS = 'COMPLETED_CARD_IDS'
export const USER_STATS = 'USER_STATS'
export const CONSTELLATION = 'CONSTELLATION'

const db = firebase.firestore()
let commentsListener
let pollStoriesListener
let votesListener
let stackListener

export const listenGoldCards = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .doc(userId)
    .collection('goldCards')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })

        dispatch({
          type: GOLD_CARDS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (error) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenLessons = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .doc(userId)
    .collection('lessons')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: LESSONS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (error) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenPersonalCards = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .doc(userId)
    .collection('cards')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: PERSONAL_CARDS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (error) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenGlobalGoldCards = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('goldCards').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      dispatch({
        type: GLOBAL_GOLD_CARDS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

export const listenGlobalStacks = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('stacks').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      dispatch({
        type: STACKS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

const completedCardsObserver = {
  userId: null,
  observer: null,
}

export const listenCompletedCardIds = () => (dispatch) => {
  const { 
    userId,
    observer,
  } = completedCardsObserver

  if (firebase.auth().currentUser.uid === userId) {
    return
  }

  completedCardsObserver.userId = firebase.auth().currentUser.uid

  if (observer) observer() // unsubscribe

  completedCardsObserver.observer = db.collection('users')
    .doc(firebase.auth().currentUser.uid)
    .collection('completedCards')
    .onSnapshot(
      (res) => {
        const cards = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id 
          obj.key = doc.key
          cards.push(obj)
        })

        dispatch({
          type: COMPLETED_CARD_IDS,
          payload: cards,
        })
      },
      (e) => {
        processErr(e, '', false)
      },
    )
}

export const listenUserStats = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .doc(userId)
    .collection('skills')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: USER_STATS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (error) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenCards = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('cards2').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      dispatch({
        type: CARDS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

const audioCardsObserver = {
  observer: null, 
}

export const listenAudioCards = () => (dispatch) => {
  if (audioCardsObserver.observer) return

  audioCardsObserver.observer = db.collection('cards').onSnapshot(
    (res) => {
      const cards = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id 
        obj.key = doc.id 
        cards.push(obj)
      })

      dispatch({
        type: CARDS,
        payload: cards,
      })
    },
    (e) => {
      processErr(e, '', false)
    },
  )
}

export const listenConstellations = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('constellations').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })

      dispatch({
        type: CONSTELLATION,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

const myRecordingsObserver = {
  userId: null,
  observer: null,
}

export const listenMyRecordings = () => (dispatch, getState) => {
  const {
    userId,
    observer,
  } = myRecordingsObserver

  if (firebase.auth().currentUser.uid === userId) return 

  myRecordingsObserver.userId = firebase.auth().currentUser.uid 

  if (observer) observer() // unsubscribe

  myRecordingsObserver.observer = db.collection('users')
    .doc(firebase.auth().currentUser.uid)
    .collection('recordings')
    .onSnapshot(
      (res) => {
        const audioStore = getState().checkIn.myRecordings
        const recordings = []

        res.forEach((doc) => {
          const obj = doc.data()
          const matchedArr = obj.audios.filter((audio) => {
            const matched = find(audioStore, { id: obj.id })
            if (!matched) return false
            return audio.recording === matched.audios[index].recording
          })

          if (matchedArr.length > 0)
            return recordings.push(matchedArr[0])

          obj.id = doc.id 
          obj.key = doc.id 
          recordings.push(obj)
        })

        dispatch({
          type: MYRECORDINGS,
          payload: recordings,
        })
      },
      (e) => {
        processErr(e, '', false)
      },
    )
}

export const listenCardsFromStack = stackName => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  stackListener = db
    .collection('cards2')
    .where('stack', '==', stackName)
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: STACK_CARDS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenCheckups = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .where('userId', '==', userId)
    .collection('checkups')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: UPDATE,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenCheckIns = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('checkIns')
    .where('shareWith', '==', 'everyone')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: UPDATE,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenPosts = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('posts').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      dispatch({
        type: POSTS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

export const listenPrivateCheckIns = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('checkIns')
    .where(`shareWith.${userId}`, '==', true)
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: UPDATE_PRIVATE,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenMyCards = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('users')
    .doc(userId)
    .collection('cards')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: CARDS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenJustMeCheckIns = userId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('checkIns')
    .where('userId', '==', userId)
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: UPDATE_PRIVATE,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenMembers = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })

  db.collection('users').onSnapshot(
    (res) => {
      const promises = []

      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        if (firebase.auth().currentUser.uid != obj.id) {
          promises.push(obj)
        }
      })
      dispatch({
        type: MEMBERS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (error) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

export const listenComments = checkInId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  commentsListener = db
    .collection('posts')
    .doc(checkInId)
    .collection('comments')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          promises.push(obj)
        })
        dispatch({
          type: COMMENTS,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenVotes = pollId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  votesListener = db
    .collection('polls')
    .doc(pollId)
    .collection('votes')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          promises.push(obj)
        })
        dispatch({
          type: VOTES,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const listenPolls = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('polls').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        promises.push(obj)
      })
      dispatch({
        type: POLLS,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

export const listenStories = () => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  db.collection('stories').onSnapshot(
    (res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        promises.push(obj)
      })
      dispatch({
        type: STORIES,
        payload: promises,
      })
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
    (e) => {
      dispatch({
        type: LOADING,
        payload: false,
      })
    },
  )
}

export const listenPollStories = pollId => (dispatch) => {
  dispatch({
    type: LOADING,
    payload: true,
  })
  pollStoriesListener = db
    .collection('polls')
    .doc(pollId)
    .collection('pollStories')
    .onSnapshot(
      (res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          promises.push(obj)
        })
        dispatch({
          type: POLLSTORIES,
          payload: promises,
        })
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
      (e) => {
        dispatch({
          type: LOADING,
          payload: false,
        })
      },
    )
}

export const stopListenPollStories = pollId => (dispatch) => {
  pollStoriesListener()
  dispatch({
    type: POLLSTORIES,
    payload: [],
  })
}

export const stopListenStories = pollId => (dispatch) => {
  storiesListener()
  dispatch({
    type: STORIES,
    payload: [],
  })
}

export const stopListenVotes = pollId => (dispatch) => {
  votesListener()
  dispatch({
    type: VOTES,
    payload: [],
  })
}

export const stopListenComments = checkInId => (dispatch) => {
  commentsListener()
  dispatch({
    type: COMMENTS,
    payload: [],
  })
}

export const stopListenStack = stackId => (dispatch) => {
  stackListener()
  dispatch({
    type: STACK_CARDS,
    payload: [],
  })
}

export const storeEmojis = payload => ({
  type: STORE,
  payload,
})

export const updateMyRecording = payload => ({
  type: UPDATE_MY_RECORDING,
  payload,
})

export const updatePendingRecordings = payload => ({
  type: UPDATE_PENDING_RECORDING,
  payload,
})

export const setLoading = (status) => ({
  type: LOADING,
  payload: status,
})