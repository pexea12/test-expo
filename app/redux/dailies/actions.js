import find from 'lodash/find'

import moment from '../../configs/Moment'
import { firebase } from '../../configs/Firebase'


export const GET_MORNING_AUDIO = 'GET_MORNING_AUDIO'
export const GET_NIGHT_AUDIO = 'GET_NIGHT_AUDIO'
export const GET_RANDOM_QUOTE = 'GET_RANDOM_QUOTE'
export const UPDATE_CURRENT_SKILL = 'UPDATE_CURRENT_SKILL'
export const UPDATE_SHOW_INSTRUCTION = 'UPDATE_SHOW_INSTRUCTION'
export const UPDATE_COMPLETED_STATUS = 'UPDATE_COMPLETED_STATUS'
export const UPDATE_FETCHED_DATE = 'UPDATE_FETCHED_DATE'


const MORNING_CATEGORY = 'morning'
const NIGHT_CATEGORY = 'night'

const db = firebase.firestore()


const formatDaily = (daily) => {
  const {
    audioUrl,
    question,
    audioTitle,
    starts,
  } = daily.data()

  const cardDate = new Date(starts.seconds * 1000)
  return {
    id: daily.id,
    title: `${moment(cardDate).format('dddd')} atlas`,
    isDaily: true,
    completionTime: '2m',
    audios: [{ url: audioUrl }],
    blurb: `${moment(cardDate).format('MMM DD')} · "${audioTitle}"`,
    questions: [question],
    createdAt: 0,
    featuredId: 0,
    audioSubheaders: [`${moment(cardDate).format('MMM DD')} · "${audioTitle}"`],

    ...daily.data(),
  }
}

const observer = {
  morning: null,
  night: null,
}

const inRange = (value, left, right) => value >= left && value < right 

const selectCard = (dailies, completedCards) => {
  const now = moment()
  const beginDay = now.startOf('day')
  const endDay = now.endOf('day')

  const cards = []
  dailies.forEach((daily) => {
    cards.push(formatDaily(daily))
  })

  // check whether the user has viewed a card today
  const completedItem = find(completedCards, (item) => {
    return inRange(item.createdAt, beginDay.valueOf(), endDay.valueOf())
  })

  if (completedItem) {
    const matchedCard = find(cards, { id: completedItem.cardId })
    if (matchedCard) return matchedCard
  }

  // select card
  const completedIds = completedCards.map(item => item.id)
  let incompleteCard, randomCard
  for (let i = 0; i < cards.length; i += 1) {
    // if today has a card
    if (inRange(cards[i].starts, beginDay.valueOf(), endDay.valueOf()))
      return cards[i]

    // Find incomplete card
    if (!completedIds.includes(cards[i].id))
      incompleteCard = cards[i]
    else randomCard = cards[i]
  }

  if (incompleteCard) return incompleteCard
  return randomCard
}

export const getMorningAudio = () => async (dispatch, getState) => {
  const completedCards = getState().checkIn.completedCardIds

  const query = db.collection('dailies')
    .where('category', '==', MORNING_CATEGORY)

  // if (observer.morning) observer.morning() // unsubscribe
  // observer.morning = query.onSnapshot((dailies) => {
  //   const card = selectCard(dailies, completedCards)
  //   dispatch({
  //     type: GET_MORNING_AUDIO,
  //     payload: card,
  //   })
  // })

  const dailies = await query.get()
  const card = selectCard(dailies, completedCards)
  dispatch({
    type: GET_MORNING_AUDIO,
    payload: card,
  })
}

export const getNightAudio = () => async (dispatch, getState) => {
  const completedCards = getState().checkIn.completedCardIds

  const query = db.collection('dailies')
    .where('category', '==', NIGHT_CATEGORY)

  // if (observer.night) observer.night() // unsubscribe
  // observer.night = query.onSnapshot((dailies) => {
  //   const card = selectCard(dailies, completedCards)
  //   dispatch({
  //     type: GET_NIGHT_AUDIO,
  //     payload: card,
  //   })
  // })

  const dailies = await query.get()
  const card = selectCard(dailies, completedCards)
  dispatch({
    type: GET_NIGHT_AUDIO,
    payload: card,
  })
}

export const getRandomQuote = () => async (dispatch) => {
  const quotes = await db.collection('quotes').get()
  const randomIndex = Math.floor(Math.random() * quotes.docs.length)
  const quoteData = quotes.docs[randomIndex].data()
  dispatch({
    type: GET_RANDOM_QUOTE,
    payload: quoteData,
  })
}

export const updateShowInstruction = status => ({
  type: UPDATE_SHOW_INSTRUCTION,
  payload: status,
})

export const updateCurrentSkill = newSkill => ({
  type: UPDATE_CURRENT_SKILL,
  payload: newSkill,
})

export const updateFetchedDate = date => ({
  type: UPDATE_FETCHED_DATE,
  payload: date,
})
