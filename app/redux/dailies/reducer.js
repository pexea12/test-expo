import {
  GET_MORNING_AUDIO,
  GET_NIGHT_AUDIO,
  GET_RANDOM_QUOTE,
  UPDATE_CURRENT_SKILL,
  UPDATE_FETCHED_DATE,
  UPDATE_SHOW_INSTRUCTION,
} from './actions'

const initialState = {
  loading: false,
  morningDaily: {},
  nightDaily: {},
  quote: {},
  skill: {},
  showInstruction: true,
  fetchedDate: null,
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_MORNING_AUDIO:
      return { ...state, morningDaily: payload }
    case GET_NIGHT_AUDIO:
      return { ...state, nightDaily: payload }
    case GET_RANDOM_QUOTE:
      return { ...state, quote: payload }
    case UPDATE_CURRENT_SKILL:
      return { ...state, skill: payload }
    case UPDATE_SHOW_INSTRUCTION:
      return { ...state, showInstruction: payload }
    case UPDATE_FETCHED_DATE:
      return { ...state, fetchedDate: payload }

    default:
      return state
  }
}

export default reducer