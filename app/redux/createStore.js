import { 
  createStore, 
  applyMiddleware, 
  compose, 
} from 'redux'
import { 
  persistStore, 
  persistReducer, 
} from 'redux-persist'
import thunk from 'redux-thunk'
import { AsyncStorage } from 'react-native'
import promise from 'redux-promise-middleware'

import rootReducer from '.'


const enhancer = compose(applyMiddleware(thunk, promise()))

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer, enhancer)
const persistor = persistStore(store)


export { 
  store, 
  persistor,
}

// You can use subscribe() to update the UI in response to state changes.
// Normally you'd use a view binding library (e.g. React Redux) rather than subscribe() directly.
// However it can also be handy to persist the current state in the localStorage.
