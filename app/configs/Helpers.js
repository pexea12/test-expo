import { Alert } from 'react-native'

export const generateId = () => {
  const S4 = () => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }
  return `${S4() + S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`
}

export const processErr = (e, customMessage = '', alert = true) => {
  if (alert)
    Alert.alert(
      'Error',
      `${customMessage} Please restart the application and try again`,
    )
  
  console.error(e)
}
