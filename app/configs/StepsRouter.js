import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import { last } from 'lodash'

export default class StepRouter extends Component {
  constructor(props) {
    super(props)

    this.generateNavigator = this.generateNavigator.bind(this)
    this.handleRef = this.handleRef.bind(this)
    this.handleNavigationChange = this.handleNavigationChange.bind(this)
  }

  componentWillMount() {
    this.Navigator = this.generateNavigator()
  }

  generateNavigator() {
    const { steps } = this.props
    const navigationRoutes = {}
    const navigationOptions = {
      headerTransparent: true,
    }
    steps.forEach((step, index) => {
      navigationRoutes[step.routeName] = { screen: () => step.component, navigationOptions }
    })

    return createStackNavigator(navigationRoutes)
  }

  handleRef(navigator) {
    this.props.handleNavRef(navigator)
  }

  handleNavigationChange(prevState, currentState, action) {
    const { isTransitioning, routes } = currentState
    if (isTransitioning) {
      const { routeName } = last(routes)
      this.props.handleNavChange(routeName)
    }
  }

  render() {
    const { Navigator } = this
    const App = createAppContainer(Navigator)
    return <App onNavigationStateChange={this.handleNavigationChange} ref={this.handleRef} />
  }
}
