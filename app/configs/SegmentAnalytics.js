import { Segment } from 'expo'

const segmentAnalyzePage = async (user, screenName, props) => {
  try {
    Segment.initialize({
      androidWriteKey: 'ca8k4oFzWYikYJ5qN45F9lp1ph7QQdoR',
      iosWriteKey: 'ca8k4oFzWYikYJ5qN45F9lp1ph7QQdoR',
    })

    if (user == 'anon') {
      Segment.alias(Expo.Constants.deviceId)
    } else {
      Segment.identifyWithTraits(user.id, {
        firstName: user.first_name,
        lastName: user.last_name,
        email: user.email,
        name: `${user.first_name} ${user.last_name}`,
      })
    }

    if (props) {
      Segment.screenWithProperties(screenName, props)
    } else if (screenName) {
      Segment.screen(screenName)
    }
  } catch (e) {
    console.error(e)
  }
}

const segmentAnalyzeEvent = async (eventName, eventTraits) => {
  if (eventTraits) {
    Segment.trackWithProperties(eventName, eventTraits)
  } else {
    Segment.track(eventName)
  }
}

const segmentSignup = async (user) => {
  Segment.track('Completed Signup')
  Segment.alias(Expo.Constants.deviceId, user.id)
  Segment.identifyWithTraits(user.id, {
    firstName: user.first_name,
    lastName: user.last_name,
    email: user.email,
    name: `${user.first_name} ${user.last_name}`,
  })
}

export { segmentAnalyzePage, segmentAnalyzeEvent, segmentSignup }
