/**
 * Created by Mohammad Kashif on 7/19/2018.
 */
import { Permissions, Notifications } from 'expo'

export const getToken = async () => {
  try {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.

    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
      if (status !== 'granted') return
    }

    const token = await Notifications.getExpoPushTokenAsync()
    return token
  } catch (e) {
    console.error(e)
  }

  return 
}