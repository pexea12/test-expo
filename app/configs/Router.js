import React from 'react'
import {
  ActivityIndicator,
  AsyncStorage,
  Animated,
  Easing,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Platform,
} from 'react-native'
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  StackViewTransitionConfigs,
} from 'react-navigation'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import StackViewStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator'

import * as Routes from '../screens'
import { onUserChanged } from '../redux/auth/actions'

const {
  forHorizontal,
  forFadeFromBottomAndroid,
  forVertical,
  forFade,
} = StackViewStyleInterpolator

const NoTransitionSpec = {
  duration: 0,
  timing: Animated.timing,
  easing: Easing.step0,
}

const TransitionSpec = {
  duration: 500,
  easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
  timing: Animated.timing,
}

const TransitionConfig = () => {
  let currentTransitionSpec = TransitionSpec
  return {
    screenInterpolator: (sceneProps) => {
      const params = sceneProps.scene.route.params || {}
      const transition = params.transition || Platform.OS
      if (transition == 'none') {
        currentTransitionSpec = NoTransitionSpec
      }
      return {
        none: TransitionSpec,
        vertical: forVertical(sceneProps),
        modal: forVertical(sceneProps),
        fade: forFade(sceneProps),
        ios: forHorizontal(sceneProps),
        android: forFadeFromBottomAndroid(sceneProps),
      }[transition]
    },

    transitionSpec: currentTransitionSpec,
  }
}

const dynamicModalTransition = (transitionProps, prevTransitionProps, sceneProps) => {
  const isModal = MODAL_ROUTES.some(
    screenName => screenName === transitionProps.scene.route.routeName
      || (prevTransitionProps && screenName === prevTransitionProps.scene.route.routeName),
  )
  const defaultTransition = StackViewTransitionConfigs.defaultTransitionConfig(
    transitionProps,
    prevTransitionProps,
    isModal,
  )
  return defaultTransition
}

const AppStack = createStackNavigator(
  {
    AudioHome: {
      screen: Routes.AudioHome,
    },
    FirstScreen: {
      screen: Routes.FirstScreen,
    },
    Episodes: {
      screen: Routes.Episodes,
    },
    Dailies: {
      screen: Routes.Dailies,
    },
    Recordings: {
      screen: Routes.Recordings,
    },
    AudioCard: {
      screen: Routes.AudioCard,
    },
    IntroAudioCard: {
      screen: Routes.IntroAudioCard,
    },
    AllAudios: {
      screen: Routes.AllAudios,
    },
    Login: {
      screen: Routes.Login,
    },
    ForgotPassword: {
      screen: Routes.ForgotPassword,
    },
    FeedbackForm: {
      screen: Routes.FeedbackForm,
    },
    Account: {
      screen: Routes.Account,
    },
    EditAccount: {
      screen: Routes.EditAccount,
    },
    ResetPassword: {
      screen: Routes.ResetPassword,
    },
    Privacy: {
      screen: Routes.Privacy,
    },
    TermsAndConditions: {
      screen: Routes.TermsAndConditions,
    },
    SupportResources: {
      screen: Routes.SupportResources,
    },
    Feedback: {
      screen: Routes.Feedback,
    },
    TextFeedback: {
      screen: Routes.TextFeedback,
    },
    ListenAllAudios: {
      screen: Routes.ListenAllAudios,
    },
    Slide: {
      screen: Routes.Slide,
    },
    CreateNotification: {
      screen: Routes.CreateNotification,
    },
    Onboarding: {
      screen: Routes.Onboarding,
    },
    Recap: {
      screen: Routes.Recap,
    },
    RegisterForm: {
      screen: Routes.RegisterForm,
    },
  },
  {
    initialRouteName: 'AudioHome',
    transitionConfig: TransitionConfig,
  },
)

const AuthStack = createStackNavigator(
  {
    FirstScreen: {
      screen: Routes.FirstScreen,
    },
    Register: {
      screen: Routes.Register,
    },
    Login: {
      screen: Routes.Login,
    },
    AudioHome: {
      screen: Routes.AudioHome,
    },
    Episodes: {
      screen: Routes.Episodes,
    },
    Dailies: {
      screen: Routes.Dailies,
    },
    Recordings: {
      screen: Routes.Recordings,
    },
    AudioCard: {
      screen: Routes.AudioCard,
    },
    IntroAudioCard: {
      screen: Routes.IntroAudioCard,
    },
    AllAudios: {
      screen: Routes.AllAudios,
    },
    ForgotPassword: {
      screen: Routes.ForgotPassword,
    },
    FeedbackForm: {
      screen: Routes.FeedbackForm,
    },
    Account: {
      screen: Routes.Account,
    },
    EditAccount: {
      screen: Routes.EditAccount,
    },
    ResetPassword: {
      screen: Routes.ResetPassword,
    },
    Privacy: {
      screen: Routes.Privacy,
    },
    TermsAndConditions: {
      screen: Routes.TermsAndConditions,
    },
    SupportResources: {
      screen: Routes.SupportResources,
    },
    Feedback: {
      screen: Routes.Feedback,
    },
    TextFeedback: {
      screen: Routes.TextFeedback,
    },
    ListenAllAudios: {
      screen: Routes.ListenAllAudios,
    },
    Slide: {
      screen: Routes.Slide,
    },
    CreateNotification: {
      screen: Routes.CreateNotification,
    },
    Onboarding: {
      screen: Routes.Onboarding,
    },
    Recap: {
      screen: Routes.Recap,
    },
    RegisterForm: {
      screen: Routes.RegisterForm,
    },
  },
  {
    initialRouteName: 'FirstScreen',
    transitionConfig: TransitionConfig,
  },
)

const mapStateToProps = state => ({
  user: state.auth.user,
})


class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this.bootstrap()
  }

  // Fetch the token from storage then navigate to our appropriate place
  bootstrap = () => {
    try {
      const {
        user,
        navigation,
      } = this.props
      console.log('Saved user', user)
      this.props.navigation.navigate(user ? 'App' : 'Auth')
    } catch (e) {
      console.error(e)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const NavStack = createSwitchNavigator({
  AuthLoading: connect(mapStateToProps)(AuthLoadingScreen),
  Auth: AuthStack,
  App: AppStack,
})

const DefaultStack = createAppContainer(NavStack)

export default DefaultStack
