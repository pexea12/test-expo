import { Audio } from 'expo'


export default RecordingConfig = {
  android: {
    ...Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY.android,
    extension: '.wav',
    outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_DEFAULT,
    sampleRate: 16000,
  },
  ios: {
    ...Audio.RECORDING_OPTIONS_PRESET_LOW_QUALITY.ios,
    extension: '.wav',
    outputFormat: Audio.RECORDING_OPTION_IOS_OUTPUT_FORMAT_LINEARPCM,
    sampleRate: 16000,
    bitRateStrategy: Audio.RECORDING_OPTION_IOS_BIT_RATE_STRATEGY_CONSTANT,
  },
}
