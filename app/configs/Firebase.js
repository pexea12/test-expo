import * as firebase from 'firebase'
import uuidv4 from 'uuid/v4'
import 'firebase/firestore'
import { getToken } from './PushNotification'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyCQhJvYSVk8bPtMGGybGvBzEiFn0GPXIn4',
  authDomain: 'atlasaudio-2e675.firebaseapp.com',
  databaseURL: 'https://atlasaudio-2e675.firebaseio.com',
  projectId: 'atlasaudio-2e675',
  storageBucket: 'atlasaudio-2e675.appspot.com',
  messagingSenderId: '19860647334',
}

firebase.initializeApp(config)

// Initialize Firestore
const db = firebase.firestore()

// Methods
export const register = async ({
  firstName, lastName, schoolEmail, password,
}) => {
  const firstRes = await firebase
    .auth()
    .createUserWithEmailAndPassword(schoolEmail, password)

  await db.collection('users')
    .doc(firstRes.user.uid)
    .set({
      first_name: firstName,
      last_name: lastName,
      email: schoolEmail,
    })

  await firstRes.user.updateProfile({ displayName: firstName })
  const secondRes = await firebase
    .auth()
    .signInWithEmailAndPassword(schoolEmail, password)

  const token = await getToken()
  await updateProfile(secondRes.user.uid, { token })

  const userRes = await db.collection('users')
    .doc(secondRes.user.uid)
    .get()

  const user = userRes.data()
  user.id = secondRes.user.uid
  return user
}

export const login = async ({ email, password }) => {
  const res = await firebase
    .auth()
    .signInWithEmailAndPassword(email, password)

  const token = await getToken()
  if (token) {
    await db.collection('users')
      .doc(res.user.uid)
      .update({ token })
  }

  const userRes = await db.collection('users')
    .doc(res.user.uid)
    .get()

  const user = userRes.data()
  user.id = res.user.uid
  return user
}

export const logout = async () => {
  await db.collection('users')
    .doc(firebase.auth().currentUser.uid)
    .update({ token: '' })
  return firebase.auth().signOut()
}

export const forgotPassword = async (email) => {
  await firebase
    .auth()
    .sendPasswordResetEmail(email)
}

export const resetPassword = async (oldPassword, newPassword) => {
  const user = firebase.auth().currentUser
  await firebase
    .auth()
    .signInWithEmailAndPassword(user.email, oldPassword)

  user.updatePassword(newPassword)
}

export const getSchools = () => new Promise((resolve, reject) => {
  db.collection('schools')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })

      return Promise.all(promises).then((schools) => {
        resolve(schools)
      })
    })
    .catch((e) => {
      reject(e.message)
    })
})

export const getMembers = () => new Promise((resolve, reject) => {
  db.collection('users')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      return Promise.all(promises).then((members) => {
        resolve(members)
      })
    })
    .catch((e) => {
      reject({ error: e.message })
    })
})

export const postRecordings = async (card) => {
  const storageRef = firebase.storage().ref()
  const userId = firebase.auth().currentUser.uid


  const promises = card.audios.map(async (audio) => {
    const uuid = uuidv4()
    const audioRef = storageRef.child(`recordings/${card.title}${userId}_${uuid}.m4a`)

    const blob = await blobMaker(audio.recording)
    await audioRef.put(blob)
    const url = await audioRef.getDownloadURL()
    return url
  })

  const allRecordings = await Promise.all(promises)
  allRecordings.forEach((recording, index) => {
    card.audios[index].recording = recording
  })

  card.createdAt = Date.now()
  // card.textFeedback = textFeedback
  // card.meaningfulValue = meaningfulValue

  await db
    .collection('users')
    .doc(userId)
    .collection('recordings')
    .doc(card.firestoreKey)
    .set(card) 

  return card.firestoreKey
}

export const blobMaker = uri => new Promise((resolve, reject) => {
  const xhr = new XMLHttpRequest()
  xhr.onload = function () {
    resolve(xhr.response)
  }
  xhr.onerror = function (e) {
    console.log(e)
    reject(new TypeError('Network request failed'))
  }
  xhr.responseType = 'blob'
  xhr.open('GET', uri, true)
  xhr.send(null)
})

export const updateProfile = (userId, params) => db
  .collection('users')
  .doc(userId)
  .update(params)

export const uploadImage = (userId, image) => {
  const storageRef = firebase.storage().ref()
  const imagesRef = storageRef.child(`images/profile_${userId}.jpg`)

  return new Promise((resolve, reject) => {
    imagesRef
      .put(image)
      .then(() => {
        imagesRef
          .getDownloadURL()
          .then((url) => {
            updateProfile(userId, { profile_picture: url })
            resolve(url)
          })
          .catch((error) => {
            reject({ error: error.message })
          })
      })
      .catch((e) => {
        reject({ error: e.message })
      })
  })
}

export const uploadFeedImage = (userId, image, post, notifications) => {
  const storageRef = firebase.storage().ref()
  const pictureId = uuidv4()
  const imagesRef = storageRef.child(`images/feed/${pictureId}.jpg`)

  return new Promise((resolve, reject) => {
    imagesRef
      .put(image)
      .then(() => {
        imagesRef
          .getDownloadURL()
          .then((url) => {
            if (post) {
              post.picture = url
              if (notifications) {
                postPost(post, notifications)
              }
            }
            resolve(url)
          })
          .catch((error) => {
            reject({ error: error.message })
          })
      })
      .catch((e) => {
        reject({ error: e.message })
      })
  })
}

export const deleteUser = () => new Promise((resolve, reject) => {
  const user = firebase.auth().currentUser
  const { uid } = user

  db.collection('users')
    .doc(uid)
    .delete()
    .then(() => {
      user.delete().catch((error) => {
        reject({ error: error.message })
      })
      firebase
        .auth()
        .signOut()
        .then(() => {
          resolve()
        })
    })
    .catch((error) => {
      reject({ error: error.message })
    })
})

export const postCheckIn = (checkIn, body) => db
  .collection('checkIns')
  .add(checkIn)
  .then(() => {
    fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
  })

export const postPost = (post, body) => db
  .collection('posts')
  .add(post)
  .then(() => {
    fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
  })

export const updatePost = (postId, params) => db
  .collection('posts')
  .doc(postId)
  .update(params)

export const postStory = story => db
  .collection('stories')
  .add(story)
  .then(() => {
    fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  })

export const sendFeedback = feedback => db.collection('feedback').add(feedback)

export const getEmojis = () => new Promise((resolve, reject) => {
  getPopularEmojis().then((popular_emojis) => {
    db.collection('emojis')
      .get()
      .then((res) => {
        const promises = []
        res.forEach((doc) => {
          const obj = doc.data()
          obj.id = doc.id
          obj.key = doc.id
          obj.popular = popular_emojis.includes(obj.mood)
          promises.push(obj)
        })

        return Promise.all(promises).then((schools) => {
          resolve(schools)
        })
      })
      .catch((e) => {
        reject(e.message)
      })
  })
})

export const getPopularEmojis = () => new Promise((resolve, reject) => {
  db.collection('popular_emojis')
    .doc('popular')
    .get()
    .then((res) => {
      resolve(res.data().emojis)
    })
    .catch((e) => {
      reject(e.message)
    })
})

// Realtime CheckIns methods are in redux.
export const getAllCheckIns = () => new Promise((resolve, reject) => {
  db.collection('checkIns')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      resolve(promises)
    })
    .catch((e) => {
      reject(e.message)
    })
})

// Realtime Post methods are in redux.
export const getAllPosts = () => new Promise((resolve, reject) => {
  db.collection('posts')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      resolve(promises)
    })
    .catch((e) => {
      reject(e.message)
    })
})

// Realtime CheckIns methods are in redux.
export const getStories = () => new Promise((resolve, reject) => {
  db.collection('stories')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      resolve(promises)
    })
    .catch((e) => {
      reject(e.message)
    })
})

export const getPolls = () => new Promise((resolve, reject) => {
  db.collection('polls')
    .get()
    .then((res) => {
      const promises = []
      res.forEach((doc) => {
        const obj = doc.data()
        obj.id = doc.id
        obj.key = doc.id
        promises.push(obj)
      })
      resolve(promises)
    })
    .catch((e) => {
      reject(e.message)
    })
})

export const reportStory = (pollId, storyId, users) => db
  .collection('polls')
  .doc(pollId)
  .collection('pollStories')
  .doc(storyId)
  .update({ reportedBy: users })

export const reportFeedStory = (storyId, users) => db
  .collection('stories')
  .doc(storyId)
  .update({ reportedBy: users })

export const reportCheckIn = (checkInId, users) => db
  .collection('checkIns')
  .doc(checkInId)
  .update({ reportedBy: users })

export const reportPost = (postId, users) => db
  .collection('posts')
  .doc(postId)
  .update({ reportedBy: users })

export const deleteCheckIn = checkInId => db
  .collection('checkIns')
  .doc(checkInId)
  .delete()

export const deleteFeedStory = storyId => db
  .collection('stories')
  .doc(storyId)
  .delete()

export const deleteStory = (pollId, storyId) => db
  .collection('polls')
  .doc(pollId)
  .collection('pollStories')
  .doc(storyId)
  .delete()

export const updateHearts = (checkInId, hearts, body) => db
  .collection('posts')
  .doc(checkInId)
  .update({ hearts })
  .then(() => {
    if (body) {
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    }
  })

export const updateUpvotes = (postId, commentId, upvotes, body) => db
  .collection('posts')
  .doc(postId)
  .collection('comments')
  .doc(commentId)
  .update({ upvotes })
  .then(() => {
    if (body) {
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    }
  })

export const updateCommentCount = async (checkInId, decrease) => {
  db.collection('checkIns')
    .doc(checkInId)
    .get()
    .then((res) => {
      db.collection('checkIns')
        .doc(checkInId)
        .update({
          commentsCount: decrease
            ? (res.data().commentsCount - 1) || 1
            : (res.data().commentsCount + 1) || 1,
        })
    })
}

export const updateCommentCountPost = (checkInId, decrease) => {
  db.collection('posts')
    .doc(checkInId)
    .get()
    .then((res) => {
      db.collection('posts')
        .doc(checkInId)
        .update({
          commentsCount: decrease
            ? (res.data().commentsCount - 1) || 1
            : (res.data().commentsCount + 1) || 1,
        })
    })
}

export const postComment = (checkInId, comment, body) => db
  .collection('posts')
  .doc(checkInId)
  .collection('comments')
  .add(comment)
  .then(() => {
    updateCommentCountPost(checkInId)
    if (body) {
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    }
  })

export const deleteComment = (checkInId, commentId) => db
  .collection('checkIns')
  .doc(checkInId)
  .collection('comments')
  .doc(commentId)
  .delete()
  .then(() => {
    updateCommentCount(checkInId, true)
  })

export const deletePost = postId => db
  .collection('posts')
  .doc(postId)
  .update({ hidden: true })

export const readCheckIn = (checkInId, read) => db
  .collection('checkIns')
  .doc(checkInId)
  .update({ read })

export const submitPoll = (poll, body) => db
  .collection('polls')
  .add(poll)
  .then(() => {
    if (body) {
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    }
  })

export const votePoll = (pollId, vote) => db
  .collection('polls')
  .doc(pollId)
  .collection('votes')
  .add(vote)

export const addStory = (pollId, story, body) => db
  .collection('polls')
  .doc(pollId)
  .collection('pollStories')
  .add(story)
  .then(() => {
    if (body) {
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    }
  })

export const sendCustomNotification = (notificationText, body) => db
  .collection('notifications')
  .add(notificationText)
  .then(() => {
    if (body) {
      body.forEach((notification) => {
        fetch('https://exp.host/--/api/v2/push/send', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(notification),
        })
      })
    }
  })

export const updateVotes = (pollId, voteIds) => db
  .collection('polls')
  .doc(pollId)
  .update({ voteIds })

export const postCard = (userId, card) => db
  .collection('users')
  .doc(userId)
  .collection('cards')
  .add(card)

export const addLesson = (userId, lesson) => db
  .collection('users')
  .doc(userId)
  .collection('lessons')
  .add(lesson)

export const addGoldCard = (userId, goldCard) => db
  .collection('users')
  .doc(userId)
  .collection('goldCards')
  .add(goldCard)

export const addCompletedCardId = (card) => {
  const userId = firebase.auth().currentUser.uid
  const completedCard = {
    title: card.title,
    audioTitle: card.audioTitle,
    cardId: card.id,
    createdAt: Date.now(),
    featuredId: card.featuredId,
  }
  
  return db
    .collection('users')
    .doc(userId)
    .collection('completedCards')
    .add(completedCard)
}

export const addSkill = async (skill) => {
  const userId = firebase.auth().currentUser.uid
  const skillDocRef = await db
    .collection('users')
    .doc(userId)
    .collection('skills')
    .doc(skill)

  const newSkill = await db
    .runTransaction(async (transaction) => {
      const skillDoc = await transaction.get(skillDocRef)
      if (!skillDoc.exists) {
        const newSkill = { name: skill, level: 1 }
        await transaction.set(skillDocRef, newSkill)
        return newSkill
      }

      const level = skillDoc.data().level + 1
      await transaction.update(skillDocRef, { level })
      return { 
        name: skill, 
        level, 
      }
    })

  return newSkill

  // return db
  //   .runTransaction(transaction => transaction.get(skillDocRef).then((skillDoc) => {
  //     if (!skillDoc.exists) {
  //       const newSkill = { name: skill, level: 1 }
  //       transaction.set(skillDocRef, newSkill)
  //       return newSkill
  //     }
  //     const level = skillDoc.data().level + 1
  //     transaction.update(skillDocRef, { level })
  //     return { name: skill, level }
  //   }))
  //   .then(newSkill => newSkill)
  //   .catch((e) => {
  //     console.error('Skill transaction failed: ', e)
  //   })
}

export {
  firebase,
}
