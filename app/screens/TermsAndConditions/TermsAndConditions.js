import React from 'react'
import {
  WebView,
} from 'react-native'


export default class TermsAndConditions extends React.Component {
  static navigationOptions = {
    title: 'Terms and Conditions',

    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 1,
      borderBottomColor: '#D8D8D8',
    },

    headerTitleStyle: {
      color: '#000',
      fontFamily: 'gt-walsheim-medium',
      fontWeight: '200',
      fontSize: 20,
      alignSelf: 'center',
    },
  }

  render() {
    return (
      <WebView
        source={{ uri: 'https://www.atlasmh.com/terms-of-use' }}
        style={{ flex: 1 }}
      />
    )
  }
}
