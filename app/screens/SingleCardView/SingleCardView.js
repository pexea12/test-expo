import React from 'react'
import {
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Notifications, LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import moment from '../../configs/Moment'
import BaseStyles from '../BaseStyles/Styles'
import Styles from '../MyAtlas/Styles'

const windowHeight = Dimensions.get('window').height

class SingleCardView extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  renderCardType = () => {
    const { card } = this.props
    const cardMap = {
      form: 'WONDER',
      song: 'MUSIC',
      quote: 'QUOTE',
    }
    const verbMap = {
      form: 'Answered',
      song: 'Listened',
      quote: 'Read',
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
        <Text style={Styles.typeComment}>{card.type != null ? cardMap[card.type] : 'QUOTE'}</Text>
        {card.type != null && card.createdAt && (
          <Text style={Styles.timeComment}>
            |
            {' '}
            {card.type != null && card.createdAt == null && verbMap[card.type]}
            {' '}
            {moment(card.createdAt).fromNow(true)}
            {' '}
          </Text>
        )}
      </View>
    )
  };

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  cardNavigation = () => {
    const {
      isMyAtlas, isExplore, isComment, stacks, card, post,
    } = this.props
    const parent_stack = stacks && stacks.find(x => x.id == card.stack)
    if (isExplore) {
      return (this.props.navigation.navigate('CardStackView', { stackId: card.stack, cardId: card.id, stack: parent_stack }))
    }
    if (isMyAtlas) {
      return (this.props.navigation.navigate('SavedCardDetailView', { card, stacks, stack: parent_stack }))
    } if (isComment) {
      return (this.props.navigation.navigate('CardStackView', {
        stackId: card.stack, cardId: card.id, stack: parent_stack, post, isComment: true,
      }))
    }
    return (this.props.navigation.navigate('SavedCardDetailView', { card, stacks, stack: parent_stack }))
  }

  render() {
    const { stacks } = this.props
    const { card } = this.props
    const { isMyAtlas } = this.props
    const parent_stack = stacks && stacks.find(x => x.id == card.stack)
    const color1 = (parent_stack && parent_stack.color1) ? parent_stack.color1 : '#F6B05C'
    const color2 = (parent_stack && parent_stack.color2) ? parent_stack.color2 : '#F27069'
    const img_url = parent_stack && parent_stack.image_url
    return (
      <TouchableOpacity
        onPress={() => this.cardNavigation()}
        style={{
          flexDirection: 'row',
          backgroundColor: 'white',
          zIndex: 1,
          marginBottom: 10,
          justifyContent: 'space-between',
          padding: 10,
          shadowOffset: { width: 0, height: 2 },
          shadowColor: 'rgba(0, 0, 0, 0.25)',
          shadowOpacity: 0.3,
        }}
      >
        <View style={{ flexDirection: 'row' }}>
          <View>
            <LinearGradient
              colors={[color1, color2]}
              start={[0, 1]}
              end={[1, 0]}
              style={[
                Styles.cardUI,
                {
                  width: 66, height: 80, flex: 1, justifyContent: 'space-between', padding: 12,
                },
              ]}
            >
              {img_url && (
                <Image
                  resizeMode="contain"
                  source={{ uri: img_url || '' }}
                  style={{
                    alignSelf: 'center', zIndex: 100, width: 60, height: 60,
                  }}
                />
              )}
            </LinearGradient>
          </View>
          <View>
            {this.renderCardType()}
            <View style={{ top: 14, flexWrap: 'wrap' }}>
              {card.title && (
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={2}
                  style={{
                    textAlign: 'left',
                    maxWidth: 240,
                    fontFamily: 'gt-walsheim-regular',
                    fontSize: 18,
                  }}
                >
                  {card.title}
                </Text>
              )}
              <Text
                ellipsizeMode="tail"
                numberOfLines={2}
                style={{
                  textAlign: 'left',
                  maxWidth: 240,
                  fontFamily: 'gt-walsheim-regular',
                  fontSize: 18,
                }}
              >
                {card.content}
              </Text>
              {card.response && (
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={2}
                  style={{
                    textAlign: 'left',
                    maxWidth: 240,
                    fontFamily: 'gt-walsheim-regular',
                    opacity: 0.7,
                    fontSize: 15,
                    flexWrap: 'wrap',
                    marginTop: 8,
                    marginBottom: 12,
                  }}
                >
                  {card.response}
                </Text>
              )}
              {card.caption && (
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={2}
                  style={{
                    textAlign: 'left',
                    maxWidth: 240,
                    fontFamily: 'gt-walsheim-regular',
                    opacity: 0.7,
                    fontSize: 15,
                    flexWrap: 'wrap',
                    marginTop: 8,
                    marginBottom: 12,
                  }}
                >
                  {card.caption}
                </Text>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SingleCardView)
