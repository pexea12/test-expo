import React from 'react'
import {
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Notifications, LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import moment from '../../configs/Moment'
import BaseStyles from '../BaseStyles/Styles'
import Styles from '../MyAtlas/Styles'

const analytics = new Analytics('UA-131293770-1')
const windowWidth = Dimensions.get('window').width

class SingleCardViewAudio extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {}
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  cardNavigation() {
    if (this.props.isLocked) {
      Alert.alert('Locked Episode 🔒', "You'll need to complete the previous episodes for access!")
    } else if (this.props.card.featuredId == 0) {
      this.props.navigation.navigate('Slide', { index: 0, title: 'Basics', card: this.props.card })
    } else {
      this.props.navigation.navigate('AudioCard', { card: this.props.card })
    }
  }

  renderEpisodeButton = () => {
    const { card } = this.props
    const color1 = card.color1 || '#F6B05C'
    const color2 = card.color2 || '#F27069'
    const cardOpacity = this.props.isLocked ? 0.3 : 1
    const cardCompleted = this.props.isCardCompleted

    if (this.props.isLocked) {
      return (
        <Image
          style={{
            width: 16, height: 22, alignSelf: 'flex-end', marginRight: 15,
          }}
          source={require('../../assets/images/lock.png')}
        />
      )
    }
    if (!this.props.isLocked && !cardCompleted) {
      return (
        <TouchableOpacity
          onPress={() => this.cardNavigation()}
          style={{
            border: 1,
            width: 65,
            height: 30,
            top: 7,
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#5F62B4',
            borderWidth: 1,
            borderRadius: 6,
          }}
        >
          <Text
            style={{
              fontSize: 14,
              alignSelf: 'center',
              fontFamily: 'gt-walsheim-bold',
              color: '#5F62B4',
            }}
          >
            Begin
          </Text>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        onPress={() => this.cardNavigation()}
        style={{
          border: 1,
          width: 65,
          height: 30,
          opacity: 0.4,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#5F62B4',
          borderRadius: 6,
        }}
      >
        <Text
          style={{
            fontSize: 14,
            alignSelf: 'center',
            fontFamily: 'gt-walsheim-bold',
            color: 'white',
          }}
        >
          Repeat
        </Text>
      </TouchableOpacity>
    )
  };

  render() {
    const { card } = this.props
    const color1 = card.color1 || '#F6B05C'
    const color2 = card.color2 || '#F27069'
    const cardOpacity = this.props.isLocked ? 0.3 : 1
    const cardCompleted = this.props.isCardCompleted

    return (
      <View style={{ marginBottom: 20 }}>
        <View
          style={{
            opacity: cardOpacity,
            borderRadius: 8,
            backgroundColor: 'rgba(216, 216, 216, 0.33)',
          }}
        >
          <TouchableOpacity onPress={() => this.cardNavigation()}>
            <View
              style={{
                flex: 1,
                width: '100%',
                maxWidth: windowWidth * 0.9,
                padding: 18,
                paddingBottom: 22,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View>
                <Text
                  style={{
                    fontFamily: 'gt-walsheim-medium',
                    opacity: 0.7,
                    letterSpacing: 3.43,
                    fontSize: 15,
                    color: 'black',
                    textAlign: 'left',
                    left: 5,
                  }}
                >
                  EPISODE
                  {' '}
                  {card.featuredId}
                </Text>
                <Text
                  selectable
                  style={{
                    color: '#404040',
                    top: 8,
                    fontFamily: 'gt-walsheim-regular',
                    fontSize: 18,
                    textAlign: 'left',
                    flexDirection: 'row',
                    width: '100%',
                    flex: 1,
                    left: 5,
                  }}
                >
                  {card.title}
                </Text>
              </View>
              <View>{this.renderEpisodeButton()}</View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SingleCardViewAudio)
