import React from 'react'
import { connect } from 'react-redux'
import {
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'


class FirstScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  componentDidMount() {
    segmentAnalyzePage('anon', 'Onboarding Home')
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('../../assets/images/welcomeBG.png')}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={{ justifyContent: 'space-around', flex: 1 }}>
            <View>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'flex-end',
                    paddingRight: 30,
                  }}
                >
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: 'gt-walsheim-medium',
                        fontSize: 16,
                      }}
                    >
                      Login
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    height: 300,
                    top: 130,
                    justifyContent: 'flex-end',
                  }}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'center',
                    }}
                  >
                    <View
                      style={{
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                      }}
                    >
                      <Image 
                        source={require('../../assets/images/welcomeToAtlas.png')} 
                        style={styles.welcomeImg} 
                      />
                      <View style={styles.startButton}>
                        <TouchableOpacity
                          style={styles.touchableHighlight}
                          onPress={() => this.props.navigation.navigate('Register', { transition: 'modal' })}
                        >
                          <Text style={styles.nextText}>START</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View />
            <View />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#27204E',
  },
  welcomeImg: {
    width: 190,
    height: 115,
  },
  startButton: {
    top: 30,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
  },
  touchableHighlight: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowRadius: 20,
    shadowColor: 'white',
    shadowOpacity: 0.75,
    width: 150,
    margin: 15,
    borderRadius: 25,
    padding: 15,
  },
  nextText: {
    color: '#55599B',
    fontFamily: 'gt-walsheim-bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: 2.5,
    fontSize: 14,
  },
})

export default connect()(FirstScreen)
