import { StyleSheet } from 'react-native'


const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },

  modalView: {
    maxWidth: 350,
    width: '85%',
    height: '90%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  backImage: {
    height: 12.8,
    width: 23,
    bottom: -6,
  },

  flexView: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
  },

  form: {
    width: '100%',
    justifyContent: 'center',
  },

  textInput: {
    height: 50,
    paddingLeft: 5,
    paddingRight: 5,
    color: '#FFFFFF',
    fontFamily: 'gt-walsheim-regular',
    fontSize: 17,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    marginTop: '5%',
  },

  button: {
    marginTop: '12%',
  },

  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 20,
  },

  footerText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'gt-walsheim-regular',
    fontSize: 14,
  },

  buttonText: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 14,
    color: '#ED4D96',
  },
})

export default Styles
