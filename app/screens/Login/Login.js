import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native'
import {
  acceptedTerm,
  onUserChanged,
} from '../../redux/auth/actions'
import GradientButton from '../../components/Buttons/GradientButton'
import Styles from './Styles'
import { login } from '../../configs/Firebase'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'
import Loader from '../../components/UI/Loader'
import { processErr } from '../../configs/Helpers'

class Login extends React.Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      loading: false,
    }
  }

  componentDidMount() {
    segmentAnalyzePage('anon', 'Login')
  }

  onAccept() {
    const { termAccepted } = this.props
    termAccepted()
  }

  signin = async () => {
    const {
      email,
      password,
    } = this.state

    if (this.validate()) {
      this.setState({ loading: true })
      try {
        const user = await login({ email, password })
        this.setState({ loading: false })
        console.log('signin', user.id)
        this.props.onUserChanged(user.id)
        this.props.navigation.navigate('AudioHome')
      } catch (e) {
        this.setState({
          loading: false,
        })

        processErr(e, e.message)
      }
    }
  }

  forgot = () => {
    const { navigation } = this.props
    navigation.navigate('ForgotPassword')
  }

  validate() {
    const { email, password } = this.state

    if (!email) {
      Alert.alert('Error', 'Enter Email.')
      return false
    } if (!password) {
      Alert.alert('Error', 'Enter Password.')
      return false
    }
    return true
  }

  renderLogin() {
    const {
      email,
      password,
      loading,
    } = this.state

    const { navigation } = this.props

    return (
      <View style={Styles.container}>
        {loading && <Loader style={{ height: '100%' }} />}

        <View style={Styles.modalView}>
          <View style={{ width: '100%' }}>
            <View style={{ width: '100%', justifyContent: 'flex-start' }}>
              <TouchableOpacity onPress={() => navigation.navigate('FirstScreen')}>
                <Image
                  source={require('../../assets/images/arrow-back-white.png')}
                  style={Styles.backImage}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                fontSize: 36,
                top: 75,
                textAlign: 'center',
                letterSpacing: 7.0,
                fontFamily: 'gt-walsheim-medium',
                color: 'white',
              }}
            >
              ATLAS
            </Text>
          </View>

          <View style={Styles.flexView}>
            <View style={Styles.form}>
              <TextInput
                underlineColorAndroid="transparent"
                keyboardType="email-address"
                placeholder="Email"
                placeholderTextColor="#fff"
                maxlength={200}
                style={Styles.textInput}
                autoCapitalize="none"
                onChangeText={emailData => this.setState({ email: emailData })}
                value={email}
              />
            </View>
            <View style={Styles.form}>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Password"
                placeholderTextColor="#fff"
                secureTextEntry
                maxlength={16}
                style={Styles.textInput}
                onChangeText={passwordData => this.setState({ password: passwordData })}
                value={password}
              />
            </View>
            <View style={[Styles.form, Styles.button]}>
              <TouchableOpacity onPress={this.signin}>
                <GradientButton text="Sign in" />
              </TouchableOpacity>
            </View>
            <View style={{ alignItems: 'center' }}>
              <TouchableOpacity onPress={this.forgot}>
                <Text style={Styles.buttonText}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={Styles.footer}>
          <Text style={Styles.footerText}>
            Don&apos;t have an account yet?
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={Styles.buttonText}> Sign Up </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={Styles.container}>
        <ImageBackground
          source={require('../../assets/images/bg5.png')}
          style={{ width: '100%', height: '100%' }}
        >
          {this.renderLogin()}
        </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  acceptedTerm: state.auth.acceptedTerm,
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  onUserChanged,
  termAccepted: acceptedTerm,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)
