import React from 'react'
import {
  ScrollView,
  Modal,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ImageBackground,
  Alert,
  WebView,
} from 'react-native'

import { LinearGradient } from 'expo'
import Styles from './Styles'
import GradientButton from '../../components/Buttons/GradientButton'

export default class Privacy extends React.Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = function ({ navigation }) {
    return {
      title: 'Privacy Policy',

      headerStyle: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
      },
      headerTitleStyle: {
        color: '#000',
        fontFamily: 'gt-walsheim-medium',
        fontWeight: '200',
        fontSize: 20,
        alignSelf: 'center',
      },
    }
  };

  render() {
    return (
      <WebView source={{ uri: 'https://www.atlasmh.com/privacy-policy' }} style={{ flex: 1 }} />
    )
  }
}
