const Styles = {
  modal: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    flex: 1,
  },
  modalView: {
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '90%',
    maxWidth: 310,
    paddingLeft: '4%',
    paddingRight: '4%',
    borderRadius: 5,
    borderColor: '#979797',
  },
  scrollWrapper: {
    backgroundColor: '#FFFFFF',
    paddingLeft: '4%',
    paddingRight: '4%',
  },
  heading: {
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
    color: '#343434',
    fontSize: 22,
    marginTop: '12%',
  },
  paragraphText: {
    fontFamily: 'gt-walsheim-regular',
    textAlign: 'left',
    fontSize: 14,
    lineHeight: 17,
  },
  subtitle: {
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
    color: '#9B9B9B',
    fontSize: 15,
    letterSpacing: 0.2,
    marginTop: '8%',
    marginBottom: '5%',
  },
  paragraph: {
    fontFamily: 'gt-walsheim-regular',
    textAlign: 'center',
    color: '#2A2A2A',
    fontSize: 15,
    letterSpacing: 0.2,
    marginLeft: '4%',
    marginRight: '4%',
  },
  buttonView: {
    width: '100%',
    marginTop: '8%',
    marginBottom: '4%',
  },
}

export default Styles
