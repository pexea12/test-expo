import React from 'react'
import {
  ScrollView,
  Text,
  Dimensions,
  View,
  ImageBackground,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LinearGradient } from 'expo'
import posed from 'react-native-pose'
import Styles from './Styles'
import Header from '../../components/Header/Header'
import moment from '../../configs/Moment'
import { postCard } from '../../configs/Firebase'

const fastTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 400,
})

const completionConfig = {
  state0: {
    opacity: 1.0,
    transition: fastTween,
  },
  state1: {
    opacity: 0.0,
    transition: fastTween,
  },
}

const OverallWrapper = posed.View(completionConfig)

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const categories = ['Basics', 'Stress', 'Asking for Help', 'Thoughts']

class Goldcard extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      question: '',
      response: '',
      loading: false,
      animateBox: false,
      isVisible: false,
      ringActivate: false,
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
  }

  componentWillMount() {}

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isVisible: !this.state.isVisible,
      })
    }, 50)
  }

  navigate() {
    this.props.navigation.goBack()
  }

  submit() {
    this.props.navigation.navigate('MyAtlas')
  }

  render() {
    const { description, isVisible, ringActivate } = this.state
    const { card } = this.props.navigation.state.params

    return (
      <View style={Styles.container}>
        <LinearGradient
          colors={['white', 'white']}
          locations={[0, 1]}
          start={[0.55, 0.3]}
          end={[0.7, 1]}
          style={{
            position: 'absolute',
            zIndex: -200,
            width: '103%',
            height: '103%',
            left: -10,
            top: '-3%',
          }}
        />

        <OverallWrapper pose={!isVisible ? 'state1' : 'state0'}>
          <View
            style={{
              height: 80,
              paddingTop: '12%',
              flexDirection: 'row',
              alignSelf: 'flex-start',
              marginLeft: 20,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack()
              }}
              style={{ marginTop: '5%' }}
            >
              <Image
                source={require('../../assets/images/arrow-back.png')}
                style={{ width: 21, height: 21 }}
              />
            </TouchableOpacity>
          </View>
          <View style={{
            padding: 20, flex: 1, width: '100%', marginBottom: 60,
          }}
          >
            <Text style={[Styles.motivational, { color: 'white' }]}>{card.id}</Text>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
              <LinearGradient
                colors={['black', 'black']}
                locations={[0, 1]}
                start={[0.55, 0.3]}
                end={[0.7, 1]}
                style={{
                  padding: 30,
                  width: windowWidth - 40,
                  height: windowHeight - 150,
                  top: 30,
                  shadowOffset: { width: -8, height: 10 },
                  shadowColor: 'rgba(0, 0, 0, 0.30)',
                  shadowOpacity: 0.8,
                  borderRadius: 12,
                  alignSelf: 'center',
                }}
              >
                <View>
                  <View
                    style={{
                      alignSelf: 'center',
                      backgroundColor: 'black',
                      borderRadius: 8,
                      width: windowWidth - 72,
                      height: windowHeight - 186,
                      bottom: 15,
                      borderColor: '#DE9F00',
                      borderWidth: 2,
                    }}
                  >
                    <View
                      style={{
                        padding: 8,
                        width: '100%',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}
                    >
                      <Image
                        source={require('../../assets/images/atlas_gold.png')}
                        style={{ width: 60, height: 60, marginRight: 12 }}
                      />
                      <Text
                        style={{
                          fontFamily: 'gt-walsheim-medium',
                          fontSize: 16,
                          marginTop: '5%',
                          letterSpacing: 3,
                          color: '#DE9F00',
                        }}
                      >
                        ATLAS GOLD
                      </Text>
                    </View>
                    <View style={[{ paddingLeft: 12, alignItems: 'center' }]}>
                      <Text
                        style={[
                          {
                            marginTop: '5%',
                            justifyContent: 'center',
                            alignSelf: 'center',
                            textAlign: 'center',
                            color: '#DE9F00',
                            fontSize: 32,
                            fontFamily: 'gt-walsheim-bold',
                            letterSpacing: 1.5,
                          },
                        ]}
                      >
                        {card.key.toUpperCase()}
                      </Text>
                      <Text
                        style={[
                          {
                            marginTop: '12%',
                            fontFamily: 'gt-walsheim-regular',
                            fontSize: 18,
                            lineHeight: 23,
                            alignSelf: 'flex-start',
                            color: 'white',
                          },
                        ]}
                      >
                        {card.content}
                      </Text>
                    </View>
                  </View>
                </View>
                3
                {' '}
              </LinearGradient>
            </View>
          </View>
        </OverallWrapper>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  emojis: state.checkIn.emojis,
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Goldcard)
