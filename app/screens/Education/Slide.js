import React from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  Modal,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  FlatList,
  Dimensions,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import posed from 'react-native-pose'
import { LinearGradient } from 'expo'
import Styles from './SlideStyles'
import Header from '../../components/Header/Header'
import Emoji from '../../components/Emoji/Emoji'

import {
  postPoll,
  votePoll,
  updateVotes,
  updateProfile,
  addLesson,
  addGoldCard,
} from '../../configs/Firebase'

import { listenCards, listenGoldCards, listenGlobalGoldCards } from '../../redux/checkIn/actions'


const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height


const slowerTween2 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 200,
  delay: 350,
})

const captionTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1000,
  delay: 380,
})


const buttonTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1500,
  delay: 390,
})


const titleConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: slowerTween2,
  },
  exit: {
    scale: 1.0,
    opacity: 0.4,
    transition: slowerTween2,
  },
}

const captionConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: captionTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0,
    transition: captionTween,
  },
}

const buttonConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: buttonTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0,
    transition: buttonTween,
  },
}


const FadeOutTitle = posed.View(titleConfig)
const FadeOutCaption = posed.View(captionConfig)
const FadeOutButton = posed.View(buttonConfig)


const iPhoneXPadding = (windowHeight > 800) * 26


const basics = [
  {
    index: 0,
    img: require('../../assets/images/onboarding/1.png'),
    title: 'Welcome to Atlas',
    caption:
      'Speaking out loud helps you own up to thoughts and feelings you might otherwise ignore.',
  },
  {
    index: 1,
    img: require('../../assets/images/onboarding/2.png'),
    title: 'Listen',
    caption: 'You’ll explore your identity through a series of episodes.',
  },
  {
    index: 2,
    img: require('../../assets/images/onboarding/3.png'),
    title: 'Record',
    caption:
      'In each episode, you’ll learn more about yourself by talking through a series of questions.',
  },
  {
    index: 3,
    img: require('../../assets/images/onboarding/4.png'),
    title: 'Your data is completely private',
    caption:
      'Atlas is committed to your privacy. We do not listen to, sell, or market your recordings.',
  },
  {
    index: 4,
    img: require('../../assets/images/onboarding/5.png'),
    title: 'Let’s begin your journey',
  },
]


class Slide extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)

    this.state = {
      text: '',
      mood: '',
      description: '',
      onText: false,
      emoji: {},
      emojis: [],
      searched: [],
      loading: false,
      value: 5,
      card: props.navigation.state.params.card,
      startAnimation: false,
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._fullView = this.fullView.bind(this)
  }

  navigate() {
    this.props.navigation.goBack()
  }

  componentWillUnmount() {
    StatusBar.setHidden(false)
  }

  componentWillMount() {
    StatusBar.setHidden(true)
  }

  componentDidMount() {
    const oldState = this.state.startAnimation
    this.setState({ startAnimation: !oldState })
  }

  fullView() {
    Keyboard.dismiss()
  }

  async submit(value, length, forward) {
    let { index, lessonTitle, card } = this.props.navigation.state.params

    if ((index == length - 1) && forward) {
      // update profile if user has seen onboarding
      updateProfile(this.props.user.id, { viewedEducationCarousel: true })
      this.props.navigation.navigate('AudioCard', { card })
    } else {
      if (forward) {
        index++
      } else {
        index--
      }
      if (index > -1) {
        this.props.navigation.push('Slide', { index, card })
      } else {
        this.props.navigation.navigate('AudioHome')
      }
    }
  }

  skip() {
    const { card } = this.props.navigation.state.params
    updateProfile(this.props.user.id, { skippedEducationCarousel: true })
    this.props.navigation.navigate('AudioCard', { card })
  }

  renderLoadingBar = (data, currIndex) => {
    const marginLength = 1.4
    const indicatorLength = (windowWidth - (data.length - 1) * marginLength) / data.length
    let topSpace = 0

    const d = Dimensions.get('window')
    const { height, width } = d

    if (height === 812 || width === 812) {
      topSpace = 33
    }

    return (
      <View
        style={{
          zIndex: 9,
          position: 'absolute',
          top: topSpace + iPhoneXPadding,
          width: windowWidth,
        }}
      >
        <FlatList
          horizontal
          data={data}
          style={{ width: windowWidth }}
          extraData={this.state}
          renderItem={({ item: card, index }) => (
            <View style={{ marginLeft: marginLength }}>
              {index == currIndex && (
              <View style={{ height: 5, width: indicatorLength, backgroundColor: '#FFC100' }} />
              )}
              {index != currIndex && (
              <View style={{ height: 5, width: indicatorLength, backgroundColor: 'white' }} />
              )}
            </View>
          )}
        />
      </View>
    )
  };

  getHeightMargin = () => {
    if (windowHeight >= 800) return windowHeight * 0.40
    if (windowHeight < 600) return windowHeight * 0.43
    return windowHeight * 0.37
  }

  getFlex = () => {
    if (windowHeight >= 800) return 1.2
    if (windowHeight < 600) return 1.5
    return 1
  }


  render() {
    const {
      text, description, onText, emoji, emojis, searched, loading, value,
    } = this.state
    const { index, lessonTitle } = this.props.navigation.state.params
    const backIndex = index - 1
    const data = basics
    const url = data[index].img
    const currIndex = index
    const heightMargin = this.getHeightMargin()
    const screenFlex = this.getFlex()

    return (
      <View style={Styles.container}>
        <View style={Styles.topContainer}>
          <View style={Styles.container}>
            <ImageBackground
              onLoadStart={e => this.setState({ loading: true })}
              onLoadEnd={e => this.setState({ loading: false })}
              resizeMode="cover"
              source={url}
              style={{ width: windowWidth, height: windowHeight, backgroundColor: 'black' }}
            >
              {loading && (
              <ActivityIndicator
                size="large"
                style={{
                  position: 'absolute',
                  marginTop: '5%',
                  zIndex: 100,
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}
              />
              )}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '100%',
                  top: '7%',
                  zIndex: 5,
                  marginBottom: 30,
                  flex: 1,
                }}
              >
                <FadeOutButton pose={this.state.startAnimation ? 'enter' : 'exit'}>
                  {index > 0 && (
                  <TouchableOpacity onPress={() => this.submit(value, data.length)}>
                    <Text
                      style={{
                        color: 'white',
                        letterSpacing: 1.3,
                        textTransform: 'uppercase',
                        opacity: 0.75,
                        fontFamily: 'gt-walsheim-medium',
                        fontSize: 16,
                        textAlign: 'center',
                        width: 120,
                      }}
                    >
                        Back
                    </Text>
                  </TouchableOpacity>
                  )}
                </FadeOutButton>
                <FadeOutButton pose={this.state.startAnimation ? 'enter' : 'exit'}>
                  {index < 4 && (
                  <TouchableOpacity onPress={() => this.skip()}>
                    <Text
                      style={{
                        color: 'white',
                        letterSpacing: 1.3,
                        textTransform: 'uppercase',
                        opacity: 0.75,
                        fontFamily: 'gt-walsheim-medium',
                        fontSize: 16,
                        textAlign: 'center',
                        width: 120,
                      }}
                    >
                        Skip
                    </Text>
                  </TouchableOpacity>
                  )}
                </FadeOutButton>
              </View>

              <View
                style={{
                  padding: 20,
                  flexDirection: 'column',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: '100%',
                  zIndex: 5,
                  flex: screenFlex,
                }}
              >
                <View
                  style={{
                    flex: 0.75,
                    flexDirection: 'column',
                    width: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <FadeOutTitle
                    pose={this.state.startAnimation ? 'enter' : 'exit'}
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignSelf: 'center',
                    }}
                  >
                    <Text style={Styles.title}>{data[index].title}</Text>
                  </FadeOutTitle>

                  <FadeOutCaption
                    pose={this.state.startAnimation ? 'enter' : 'exit'}
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignSelf: 'center',
                    }}
                  >
                    <Text
                      style={{
                        marginTop: '9%',
                        textAlign: 'center',
                        fontFamily: 'gt-walsheim-regular',
                        lineHeight: 28,
                        fontSize: 20,
                        color: 'white',
                      }}
                    >
                      {data[index].caption}
                    </Text>
                  </FadeOutCaption>
                </View>

                <FadeOutButton
                  pose={this.state.startAnimation ? 'enter' : 'exit'}
                  pose={this.state.startAnimation ? 'enter' : 'exit'}
                  style={{
                    position: 'absolute',
                    top: heightMargin,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.submit(value, data.length, 'forward')}
                    style={[
                      Styles.glowButton,
                      { alignItems: 'center', justifyContent: 'center', flexDirection: 'row' },
                    ]}
                  >
                    {index == 4 ? (
                      <Text
                        style={{
                          fontFamily: 'gt-walsheim-bold',
                          color: '#D84835',
                          top: -5,
                          alignSelf: 'center',
                        }}
                      >
                        Start
                      </Text>
                    ) : (
                      <Text
                        style={{
                          fontFamily: 'gt-walsheim-bold',
                          color: '#8C6BC4',
                          top: -5,
                          alignSelf: 'center',
                        }}
                      >
                        Next
                      </Text>
                    )}
                  </TouchableOpacity>
                </FadeOutButton>
              </View>
            </ImageBackground>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Slide)
