import React from 'react'
import {
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import map from 'lodash/map'
import { Notifications, LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import moment from '../../configs/Moment'
import Footer from '../Footer/Footer'
import Styles from './Styles'
import BaseStyles from '../BaseStyles/Styles'
import {
  listenCards,
  listenLessons,
  listenGoldCards,
  listenGlobalGoldCards,
} from '../../redux/checkIn/actions'


const windowHeight = Dimensions.get('window').height
const windowWidth = Dimensions.get('window').width

const categories = ['Basics', 'Thoughts', 'Asking for Help', 'Stress']

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 30
  return layoutMeasurement.height + contentOffset.y
        >= contentSize.height - paddingToBottom
}

const analytics = new Analytics('UA-118205605-4')

const covers = {
  Basics: {
    index: 0,
    img:
      'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fstorycovers%2Fstory1.png?alt=media&token=bb1f6828-292f-484c-94f6-c9aedaa0133f',
  },
  Stress: {
    index: 1,
    img:
      'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fstorycovers%2Fstory3.png?alt=media&token=d8c90ac1-5ea7-4443-bf1f-167d1d2f96d9',
  },
  'Asking for Help': {
    index: 2,
    img:
      'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fstorycovers%2Fstory4.png?alt=media&token=ffc6517d-04e8-4183-a9aa-171274584cf1',
  },
  Thoughts: {
    index: 3,
    img:
      'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fstorycovers%2Fstory2.png?alt=media&token=c4fbf05e-45e5-4646-b9a5-825ba159ba73',
  },
}


class Education extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  static transitionConfig = {
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      appState: AppState.currentState,
      modal: false,
      cards: [],
      lessons: [],
      gold_cards: [],
      global_gold_cards: [],
    }
    this._navigate = this.navigate.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.cards || nextProps.cards) {
      if (this.props.cards && this.props.cards.length != nextProps.cards.length) {
        this.getCards(nextProps.cards)
      } else {
        this.getCards(this.props.cards)
      }
    }

    if (this.props.lessons || nextProps.lessons) {
      if (this.props.lessons && this.props.lessons.length != nextProps.lessons.length) {
        this.getLessons(nextProps.lessons)
      } else if (nextProps.lessons.length) {
        this.getLessons(nextProps.lessons)
      } else {
        this.getLessons(this.props.lessons)
      }
    }

    if (this.props.gold_cards || nextProps.gold_cards) {
      if (
        this.props.gold_cards
        && this.props.gold_cards.length != nextProps.gold_cards.length
      ) {
        this.getGoldCards(nextProps.gold_cards)
      } else {
        this.getGoldCards(this.props.gold_cards)
      }
    }

    if (this.props.global_gold_cards || nextProps.global_gold_cards) {
      if (
        this.props.global_gold_cards
        && this.props.global_gold_cards.length != nextProps.global_gold_cards.length
      ) {
        this.getGlobalGoldCards(nextProps.global_gold_cards)
      } else {
        this.getGlobalGoldCards(this.props.global_gold_cards)
      }
    }
  }

  componentWillMount() {
    this.props.listenCards()
    this.getCards(this.props.cards)
    this.props.listenLessons(this.props.user.id)
    this.getLessons(this.props.lessons)
    this.props.listenGoldCards(this.props.user.id)
    this.getGoldCards(this.props.gold_cards)
    this.props.listenGlobalGoldCards(this.props.user.id)
    this.getGlobalGoldCards(this.props.global_gold_cards)
  }

  async getGlobalGoldCards(global_gold_cards = []) {
    try {
      this.setState({ global_gold_cards })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  async getGoldCards(gold_cards = []) {
    try {
      this.setState({ gold_cards })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  async getLessons(lessons = []) {
    try {
      this.setState({ lessons })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  async getCards(cards = []) {
    try {
      this.setState({ cards })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  componentDidMount() {
    this._notificationSubscription = Notifications.addListener(this._handleNotification)
    // log each time someone logs on
    analytics.hit(new PageHit('Education'))
      .then(() => console.log('success'))
      .catch(e => console.log(e.message))
    AppState.addEventListener('change', this._handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // analytics.event(new Event('PageView', 'Home', this.props.user ? this.props.user.id : null))
      // .then(() => console.log("success"))
      // .catch(e => console.log(e.message));
    }
    this.setState({ appState: nextAppState })
  }

  _handleNotification = (notification) => {
    // if(notification.origin == "selected") {
    //     if(notification.data.isComment) {
    //         this.props.navigation.navigate('Comments', notification.data);
    //     }
    // }
  };

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  renderRow = data => (
    <TouchableOpacity>
      <LinearGradient
        colors={['#F6B05C', '#F27069']}
        start={[0, 1]}
        end={[1, 0]}
        style={Styles.cardUI}
      >
        <Text style={[Styles.categoryText]}>#CATEGORY</Text>
        <Text style={[Styles.timeText]}>{data}</Text>
      </LinearGradient>
    </TouchableOpacity>
  )

  renderCards() {
    const { cards } = this.state
    if (cards) {
      return cards.map(card => (
        <TouchableOpacity>
          <LinearGradient
            colors={['#F6B05C', '#F27069']}
            start={[0, 1]}
            end={[1, 0]}
            style={Styles.cardUI}
          >
            <Text style={[Styles.categoryText]}>#CATEGORY</Text>
            <Text style={[Styles.timeText]}>{card.title}</Text>
          </LinearGradient>
        </TouchableOpacity>
      ))
    }
  }

  getExperienceCards(experienceName) {
    const { cards } = this.state
    let filteredCards
    if (cards) {
      filteredCards = cards.filter(card => card.experience == experienceName)
      return filteredCards
    }
  }

  openLesson(index, lessonTitle) {
    analytics
      .event(new Event('PageView', 'Story', lessonTitle))
      .then(() => console.log('success'))
      .catch(e => console.log(e.message))
    this._navigate('Slide', { index, lessonTitle })
  }

  render() {
    const {
      modal, cards, lessons, gold_cards, global_gold_cards,
    } = this.state
    const lessonTitles = map(lessons, 'lessonTitle')
    const goldCardIds = [...new Set(map(gold_cards, 'goldCardId'))]
    // for card in global_cards, if in goldcardds, render it)
    let myGoldCards = []
    for (let i = 0; i < global_gold_cards.length; i++) {
      if (goldCardIds && goldCardIds.includes(global_gold_cards[i].id)) {
        myGoldCards = [...myGoldCards, global_gold_cards[i]]
      }
    }
    return (
      <View style={Styles.container}>
        <ScrollView style={Styles.homeList} onScroll={this._onScroll}>
          <View style={[Styles.headerHome]}>
            <ImageBackground
              source={require('../../assets/images/explore_background.png')}
              style={{ width: '100%', height: '100%' }}
            >
              <View style={Styles.homeTitle}>
                <Text style={[Styles.mainTitle]}>Stories</Text>
                <Text style={[Styles.checkInDescription]}>
                  Discover and understand what makes you you.
                </Text>
              </View>
            </ImageBackground>
          </View>
          <View style={[Styles.contentContainer]}>
            <FlatList
              extraData={this.state}
              horizontal
              data={categories}
              renderItem={({ item: card }) => {
                const isRead = lessonTitles && lessonTitles.includes(card)
                const url = covers[card].img
                return (
                  <TouchableOpacity onPress={() => this.openLesson(0, card)}>
                    <ImageBackground
                      resizeMode="cover"
                      source={{ uri: url || '' }}
                      style={[
                        Styles.cardUI,
                        {
                          borderRadius: 8,
                          overflow: 'hidden',
                          height: 300,
                          width: 225,
                          backgroundColor: '#4F2E83',
                        },
                      ]}
                    >
                      <View style={{ flexDirection: 'row', flex: 1, width: '100%' }}>
                        <View
                          style={{
                            justifyContent: 'center',
                            alignSelf: 'flex-end',
                            width: '100%',
                          }}
                        >
                          {isRead ? (
                            <View
                              style={[Styles.viewBtn, { justifyContent: 'center', width: 100 }]}
                            >
                              <Text style={[Styles.categoryTitle, { color: 'white' }]}>
                                Read Again
                              </Text>
                            </View>
                          ) : (
                            <LinearGradient
                              style={[Styles.viewBtn, { justifyContent: 'center', width: 100 }]}
                              colors={['#F9AB54', '#F05D70']}
                              start={[0, 1]}
                              end={[1, 0]}
                            >
                              <Text style={[Styles.categoryTitle, { color: 'white' }]}>
                                Tap To Start
                              </Text>
                            </LinearGradient>
                          )}
                        </View>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                )
              }}
            />
            <View
              style={{
                paddingRight: 20,
                flexDirection: 'row',
                width: '100%',
                flex: 1,
                marginTop: 20,
                justifyContent: 'space-between',
                maxWidth: '100%',
                marginBottom: 20,
              }}
            >
              <Text
                style={{
                  fontFamily: 'gt-walsheim-bold',
                  alignSelf: 'flex-start',
                  color: '#DE9F00',
                  fontSize: 18,
                  letterSpacing: 3,
                }}
              >
                ATLAS GOLD
              </Text>
              <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Text style={{ fontFamily: 'gt-walsheim-medium', fontSize: 17, color: '#DE9F00' }}>
                  {goldCardIds.length}
                </Text>
                <Image
                  style={{
                    width: 18, height: 17, bottom: -2, right: -5,
                  }}
                  source={require('../../assets/images/star.png')}
                />
              </View>
            </View>
            {(!goldCardIds || goldCardIds.length < 1) && (
              <View>
                <Text style={[Styles.headerOption, { marginRight: 20, marginBottom: 20 }]}>
                  Gold Cards contain the key concepts to help you navigate your thoughts, feelings,
                  and behaviors
                </Text>
                <View
                  style={{
                    height: 100,
                    borderRadius: 10,
                    borderColor: '#DE9F00',
                    borderWidth: 2,
                    marginRight: 20,
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <View style={{ padding: 12 }}>
                    <Image
                      source={require('../../assets/images/atlas_gold.png')}
                      style={{ width: 60, height: 60, marginRight: 12 }}
                    />
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1, flexWrap: 'wrap' }}>
                    <Text style={[Styles.headerOption]}>
                      Read Stories to unlock Atlas Gold Cards
                    </Text>
                  </View>
                </View>
              </View>
            )}
          </View>

          <View style={[Styles.contentContainer, { marginRight: 20 }]}>
            <FlatList
              numColumns={2}
              data={myGoldCards}
              renderItem={({ item: card }) => {
                const isRead = lessonTitles && lessonTitles.includes(card)
                return (
                  <TouchableOpacity onPress={() => this._navigate('Goldcard', { card })}>
                    <LinearGradient
                      colors={['black', 'black']}
                      start={[0, 1]}
                      end={[1, 0]}
                      style={[
                        Styles.cardUI,
                        {
                          shadowColor: '#F3AE00',
                          shadowOpacity: 0.4,
                          shadowOffset: { width: 1, height: 2 },
                          height: windowHeight / 3,
                          width: (windowWidth - 52) / 2,
                        },
                      ]}
                    >
                      <View style={{ flexDirection: 'row', flex: 1, width: '100%' }}>
                        <View
                          style={{
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            width: '100%',
                          }}
                        >
                          <Text
                            style={[
                              Styles.cardTitle,
                              {
                                textTransform: 'uppercase',
                                fontSize: 13,
                                textAlign: 'center',
                                alignSelf: 'center',
                                lineHeight: 18,
                                color: '#DE9F00',
                              },
                            ]}
                          >
                            {categories[card.category]}
                          </Text>
                          <Text
                            style={[
                              Styles.cardTitle,
                              {
                                fontFamily: 'gt-walsheim-bold',
                                fontSize: 17,
                                lineHeight: 20,
                                textAlign: 'center',
                                color: '#DE9F00',
                              },
                            ]}
                          >
                            {card.id}
                          </Text>
                        </View>
                      </View>
                    </LinearGradient>
                  </TouchableOpacity>
                )
              }}
            />
          </View>
        </ScrollView>
        <Footer navigation={this.props.navigation} isEducation="true" />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  cards: state.checkIn.cards,
  lessons: state.checkIn.lessons,
  gold_cards: state.checkIn.gold_cards,
  global_gold_cards: state.checkIn.global_gold_cards,

})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenCards,
  listenLessons,
  listenGoldCards,
  listenGlobalGoldCards,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Education)
