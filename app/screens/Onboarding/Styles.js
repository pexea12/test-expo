const Styles = {
  topContainer: {
    height: '100%',
  },
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  modalView: {
    width: '90%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 7,
  },
  category: {
    fontFamily: 'gt-walsheim-medium',
    opacity: 0.6,
  },
  categoryBG: {
    padding: 4,
    height: 30,
    width: '100%',
    backgroundColor: '#F2F2F2',
  },
  category: {
    fontFamily: 'gt-walsheim-bold',
    textAlign: 'left',
    fontSize: 18,
    letterSpacing: 3.75,
    textTransform: 'all-caps',
    opacity: 0.6,
    marginBottom: 12,
  },
  feelingLabel: {
    fontFamily: 'gt-walsheim-regular',
    textAlign: 'left',
    fontSize: 28,
  },
  feelingLabelContainer: {
    marginTop: '10%',
    marginBottom: '10%',
  },
  flexView: {
    flex: 1,
    width: '100%',
  },
  flexView2: {
    flex: 3,
    paddingTop: 5,
    justifyContent: 'flex-start',
    width: '110%',
  },
  flexRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
  },
  marginForm: {
    width: '100%',
  },
  name: {
    paddingTop: 5,
    paddingBottom: 5,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 18,
    color: '#575757',
  },
  nameRow: {},
  marginTop: {
    marginTop: '5%',
  },
  center: {
    textAlign: 'center',
  },
  fontGTBold: {
    fontFamily: 'gt-walsheim-bold',
    color: '#343434',
  },
  fontGTRegular: {
    fontFamily: 'gt-walsheim-regular',
    color: '#575757',
    fontSize: 14,
  },
  moodValue: {
    fontFamily: 'gt-walsheim-medium',
    color: '#F27468',
    fontSize: 36,
    textAlign: 'center',
  },
  opacity: {
    opacity: 0.6,
  },
  colorPink: {
    color: '#F36F7A',
  },
  fontSizeBigger: {
    fontSize: 18,
  },
  fontSizeSmaller: {
    fontSize: 14,
  },
  actions: {
    height: 70,
    paddingTop: 30,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  textInput: {
    width: '100%',
    paddingLeft: 5,
    paddingBottom: 10,
    fontSize: 18,
    letterSpacing: -0.16,
    fontFamily: 'gt-walsheim-regular',
  },
  searchTextInput: {
    height: 23,
    width: '100%',
    paddingLeft: 5,
    fontSize: 16,
    letterSpacing: -0.16,
    fontFamily: 'gt-walsheim-regular',
  },
  link: {
    color: '#ED5198',
  },
  selected: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  search: {
    backgroundColor: '#F3F3F3',
    paddingLeft: 8,
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 8,
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 6,
    marginTop: '5%',
    marginBottom: '3%',
  },
  chip: {
    flexDirection: 'row',
    borderRadius: 10,
    height: 50,
    padding: 10,
    margin: 6,
    backgroundColor: '#ED5198',
  },
  chipText: {
    color: 'white',
  },
  favorites: {
    paddingLeft: '1%',
    paddingRight: '1%',
  },
  paddingRow: {
    paddingRight: 5,
    paddingLeft: 5,
  },
  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 5,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  delete: {
    color: '#9B9B9B',
    fontSize: 15,
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
  },
  slider: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
    zIndex: 10,
  },
  backImage: {
    height: 12.8,
    width: 23,
    bottom: -6,
  },
  nextButton: {
    width: 72,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#6B95D2',
    justifyContent: 'center',
  },
  subquestion: {
    fontFamily: 'gt-walsheim-regular',
    textAlign: 'center',
    fontSize: 18,
    opacity: 0.5,
    marginTop: 30,
    marginBottom: 30,
  },
}

export default Styles
