import React from 'react'
import {
  FlatList,
  CheckBox,
  AppState,
  Animated,
  Easing,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  ListView,
  StatusBar,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { LinearGradient } from 'expo'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import Styles from './Styles'
import { postCheckup, updateProfile } from '../../configs/Firebase'
import moment from '../../configs/Moment'

const windowHeight = Dimensions.get('window').height
const windowWidth = Dimensions.get('window').width

class Onboarding extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)

    this.state = {
      modal: false,
      bucket: '',
      title: '',
      subtitle: '',
      stage: '',
      onText: false,
      searched: [],
      loading: false,
      isDailyCheckIn: false,
      activeSlide: 0,
      pledges: [
        'I recognize that everyone has a mental health and things can happen to anyone at any time.',
        ' I will work to keep my child healthy and safe by checking up on their mental health.',
      ],
      selected: (new Map(): Map<string, Object>),
      nextButtonValue: 'Next',
      backButtonValue: '',
      onboardingContent: [
        {
          image_url: '../../assets/images/onboarding/1.png',
        },
        {
          image_url:
            'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fonboarding%2Fonboarding2.png?alt=media&token=35433c9a-65b5-434f-bf4d-c07de95ffb18',
        },
        {
          image_url:
            'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fonboarding%2Fonboarding3.png?alt=media&token=d9a992e4-e8dc-4934-b86d-a9c7375f7091',
        },
        {
          image_url:
            'https://firebasestorage.googleapis.com/v0/b/atlasmentalhealth-5d962.appspot.com/o/stories%2Fonboarding%2FiPhone%208%20Plus%20Copy%2012-min.png?alt=media&token=327bcfe3-d5d1-434a-a133-9c309d30c593',
        },
      ],
    }
    this._submit = this.submit.bind(this)
  }

  componentWillMount() {
    StatusBar.setHidden(true)
  }

  componentWillUnmount() {
    StatusBar.setHidden(false)
  }

  componentDidMount() {
    if (this.props.user && !this.props.user.viewedOnboarding) {
      updateProfile(this.props.user.id, { viewedOnboarding: true })
    }
  }

  bucketChooser = (selected) => {
    // Loop through all indicators
    // If contains any resources, then 'getting help'
    // If contains "crisis", then "crisis"
    const buckets = [0]
    selected.forEach((value) => {
      if (value.includes('crisis')) {
        buckets.push(2)
      } else if (value.length > 0) {
        buckets.push(2)
      } else {
        buckets.push(1)
      }
    })
    return Math.max(...buckets)
  };

  navigate = () => {
    this.props.navigation.goBack()
  };

  async submitHelper(checkup) {
    try {
      await postCheckup(this.props.user.id, checkup)
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  submit = () => {
    this.props.navigation.navigate('AudioHome')
  };

  get pagination() {
    const { onboardingContent, activeSlide } = this.state
    return (
      <Pagination
        dotsLength={onboardingContent.length}
        activeDotIndex={activeSlide}
        containerStyle={{ backgroundColor: 'clear' }}
        dotStyle={{
          width: 9,
          height: 9,
          borderRadius: 5,
          borderColor: 'white',
          borderWidth: 0.5,
          marginHorizontal: 7,
          backgroundColor: 'rgba(216,216,216,0.5)',
        }}
        inactiveDotStyle={
          {
            // Define styles for inactive dots here
          }
        }
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    )
  }

  setNextButton = (index) => {
    if (index == 3) {
      this.setState({ nextButtonValue: 'Start', activeSlide: index })
    } else if (index == 0) {
      this.setState({ nextButtonValue: 'Next', activeSlide: index, backButtonValue: '' })
    } else {
      this.setState({ nextButtonValue: 'Next', activeSlide: index, backButtonValue: 'Back' })
    }
  };

  renderList = ({ item }) => (
    <View style={Styles.nameRow}>
      <TouchableOpacity
        style={[Styles.flexRow, Styles.paddingRow]}
        onPress={() => this.check({ key: item })}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
          <View style={Styles.checked}>
            {this.state.selected.get(item) ? (
              <Image
                style={Styles.checkImage}
                source={require('../../assets/images/checked.png')}
              />
            ) : (
              <Image style={Styles.checkImage} source={require('../../assets/images/empty.png')} />
            )}
          </View>
          <View style={{ left: 10, maxWidth: 0.7 * windowWidth }}>
            <Text style={{ color: '#4A4A4A', fontFamily: 'gt-walsheim-regular', fontSize: 16 }}>
              {item}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );

  renderPage = ({ item, index }) => {
    const indicators = [...Object.keys(item)]
    const url = item.image_url
    const { activeSlide, pledges } = this.state
    return (
      <View style={{}}>
        {index == 0 && (
          <Image
            onLoadStart={e => this.setState({ loading: true })}
            onLoadEnd={e => this.setState({ loading: false })}
            resizeMode="contain"
            style={{ width: windowWidth + 2, height: windowHeight + 2 }}
            source={require('../../assets/images/onboarding/1.png')}
          />
        )}
        {index == 1 && (
          <Image
            onLoadStart={e => this.setState({ loading: true })}
            onLoadEnd={e => this.setState({ loading: false })}
            resizeMode="contain"
            style={{ width: windowWidth + 2, height: windowHeight + 2 }}
            source={require('../../assets/images/onboarding/2.png')}
          />
        )}
        {index == 2 && (
          <Image
            onLoadStart={e => this.setState({ loading: true })}
            onLoadEnd={e => this.setState({ loading: false })}
            resizeMode="contain"
            style={{ width: windowWidth + 2, height: windowHeight + 2 }}
            source={require('../../assets/images/onboarding/3.png')}
          />
        )}
        {index == 3 && (
          <Image
            onLoadStart={e => this.setState({ loading: true })}
            onLoadEnd={e => this.setState({ loading: false })}
            resizeMode="contain"
            style={{ width: windowWidth + 2, height: windowHeight + 2 }}
            source={require('../../assets/images/onboarding/4.png')}
          />
        )}
        {this.state.loading && (
          <ActivityIndicator
            size="large"
            style={{
              position: 'absolute',
              marginTop: '10%',
              zIndex: 100,
              alignSelf: 'center',
              justifyContent: 'center',
            }}
          />
        )}
      </View>
    )
  };

  async nextAction() {
    if (!this.props.user.viewedOnboarding) {
      updateProfile(this.props.user.id, { viewedOnboarding: true })
    }
    const { activeSlide } = this.state
    this._carousel.snapToNext()
    if (activeSlide == 3) {
      this.props.navigation.navigate('AudioHome')
    } else {
      this._carousel.snapToNext()
    }
  }

  render() {
    const { activeSlide } = this.state
    let iPhoneXTop = 0
    if (windowHeight < 812) {
      iPhoneXTop = -23
    }
    return (
      <View style={[Styles.container]}>
        <View
          style={{
            top: iPhoneXTop,
            padding: 15,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            zIndex: 5,
          }}
        >
          <View style={{ justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={() => {
                this._carousel.snapToPrev()
              }}
            >
              <Animated.Text
                style={[
                  {
                    fontSize: 16,
                    textAlign: 'center',
                    fontFamily: 'gt-walsheim-regular',
                    opacity: 0.8,
                    color: 'white',
                  },
                ]}
              >
                {this.state.backButtonValue}
              </Animated.Text>
            </TouchableOpacity>
          </View>
          <View>{this.pagination}</View>
          <View style={{ justifyContent: 'center' }}>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                width: 60,
                height: 30,
                backgroundColor: '#4A90E2',
                borderRadius: 15,
              }}
              onPress={() => this.nextAction()}
            >
              <Animated.Text
                style={[
                  {
                    fontSize: 15,
                    textAlign: 'center',
                    fontFamily: 'gt-walsheim-bold',
                    color: 'white',
                  },
                ]}
              >
                {this.state.nextButtonValue}
              </Animated.Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ position: 'absolute' }}>
          <Carousel
            ref={(c) => {
              this._carousel = c
            }}
            onSnapToItem={(index) => {
              this.setNextButton(index)
            }}
            extraData={this.state}
            data={this.state.onboardingContent}
            renderItem={this.renderPage}
            sliderWidth={windowWidth}
            itemWidth={windowWidth}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  bucket: state.bucket,
  selected: state.selected,
  onboardingContent: state.onboardingContent,
  checkup: state.checkup,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Onboarding)
