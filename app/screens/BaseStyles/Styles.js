const Styles = {
  backImage: {
    width: 18,
    height: 16,
  },
  iconFooter: {
    height: 34,
    width: 34,
  },
  iconFooterProfile: {
    height: 34,
    width: 34,
  },
  iconFooterExplore: {
    height: 38,
    width: 38,
  },
  iconFooterHome: {
    height: 24,
    width: 28,
  },
  iconFooterEducation: {
    height: 35,
    width: 29,
  },
  iconFooterCheckIn: {
    height: 38,
    width: 38,
    right: -3,
  },
  iconFooterPulse: {
    height: 27,
    width: 30,
  },
  footerHome: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    borderTopColor: '#D8D8D8',
    borderTopWidth: 1,
    paddingTop: 8,
    paddingBottom: 8,
    flex: 1,
    height: 60,
  },
  menuButtonPadding: {
    padding: 5,
  },
}

export default Styles
