import React from 'react'
import {
  Linking,
  ScrollView,
  Modal,
  FlatList,
  Text,
  Dimensions,
  View,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import Carousel from 'react-native-snap-carousel'
import posed from 'react-native-pose'
import Communications from 'react-native-communications'
import { listenCardsFromStack, stopListenStack } from '../../redux/checkIn/actions'
import Header from '../../components/Header/Header'
import moment from '../../configs/Moment'
import { postComment, getMembers, postCard } from '../../configs/Firebase'
import Styles from '../CardStackView/Styles'

const windowHeight = Dimensions.get('window').height
const windowWidth = Dimensions.get('window').width

class CardPreviewModal extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
    }
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  render() {
    const { card, stack, response } = this.props
    const color1 = stack && stack.color1 ? stack.color1 : '#F6B05C'
    const color2 = stack && stack.color2 ? stack.color2 : '#F6B05C'
    let timeString = moment().format('MMMM D, dddd')
    if (card.createdAt != null) {
      timeString = moment(card.createdAt).format('MMMM D, dddd')
    }
    return (
      <LinearGradient
        colors={[color1, color2]}
        locations={[0, 1]}
        start={[0.55, 0.3]}
        end={[0.7, 1]}
        style={{
          shadowOffset: { width: 0, height: 0 },
          shadowColor: color1,
          shadowOpacity: 0.8,
          height: '80%',
          left: windowWidth * 0.035,
          paddingTop: 10,
          flexDirection: 'row',
          borderRadius: 8,
          justifyContent: 'center',
          padding: 20,
          width: '100%',
          height: windowHeight * 0.66,
        }}
      >
        <View style={{ height: '100%', flex: 1 }}>
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ flex: 1, top: 15 }}>
              <Text
                style={{
                  fontFamily: 'gt-walsheim-bold',
                  opacity: 0.7,
                  letterSpacing: 3.1,
                  fontSize: 15,
                  color: 'white',
                  textAlign: 'center',
                  width: '100%',
                }}
              >
                {timeString.toUpperCase()}
              </Text>
              <Text
                style={{
                  fontFamily: 'gt-walsheim-bold',
                  opacity: 0.7,
                  fontSize: 15,
                  color: 'white',
                  textAlign: 'center',
                  width: '100%',
                }}
              >
                {card.createdAt != null
                  ? moment(card.createdAt).format('h:mm a')
                  : moment().format('h:mm a')}
              </Text>
            </View>
            <View style={{ flex: 4, top: -25, justifyContent: 'center' }}>
              {card && card.content && (
                <Text
                  style={{
                    color: 'white',
                    fontFamily: 'gt-walsheim-regular',
                    fontSize: 24,
                    lineHeight: 32,
                    textAlign: 'center',
                  }}
                >
                  {card.content}
                </Text>
              )}
              {card && card.caption && (
                <Text
                  style={{
                    color: 'white',
                    top: 15,
                    opacity: 0.6,
                    letterSpacing: 3.1,
                    fontFamily: 'gt-walsheim-bold',
                    fontSize: 18,
                    textAlign: 'center',
                  }}
                >
                  {card.caption.toUpperCase()}
                </Text>
              )}
              {!!response && (
                <Text
                  style={{
                    color: 'white',
                    top: 15,
                    opacity: 0.9,
                    fontFamily: 'gt-walsheim-regular',
                    fontSize: 16,
                    textAlign: 'center',
                  }}
                >
                  {response}
                </Text>
              )}
            </View>
          </ScrollView>
        </View>
      </LinearGradient>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardPreviewModal)
