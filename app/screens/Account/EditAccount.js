import React from 'react'
import {
  Text, View, Image, TouchableOpacity, Alert, TextInput,
} from 'react-native'
import { ImagePicker, Permissions, ImageManipulator } from 'expo'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NavigationActions, StackActions } from 'react-navigation'
import { connectActionSheet } from '@expo/react-native-action-sheet'
import Styles from './Styles'
import {
  updateProfile, uploadImage, deleteUser, blobMaker,
} from '../../configs/Firebase'
import { logout } from '../../redux/auth/actions'
import Favorites from '../Favorites/Favorites'
import Header from '../../components/Header/Header'

@connectActionSheet
class Account extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      firstName: props.user.first_name,
      lastName: props.user.last_name,
      schoolEmail: props.user.email,
      profilePicture: props.user.profile_picture,
      isFocused: false,
    }

    this._onOpenActionSheet = this.onOpenActionSheet.bind(this)
    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._removeImage = this.removeImage.bind(this)
    this._deleteAccount = this.deleteAccount.bind(this)
  }

  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL)
    await Permissions.askAsync(Permissions.CAMERA)
  }

  onOpenActionSheet = () => {
    const options = ['Gallery', 'Camera', 'Cancel']
    const destructiveButtonIndex = 2
    const cancelButtonIndex = 2
    this.props.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
        destructiveButtonIndex,
      },
      (buttonIndex) => {
        this.pickImage(buttonIndex == 0)
      },
    )
  };

  onFocus(index) {
    this.setState({ isFocused: index })
  }

  navigate() {
    this.props.navigation.goBack()
  }

  pickImage = async (fromGallery) => {
    const options = {
      allowsEditing: true,
      aspect: [3, 3],
      base64: true,
    }
    let result
    if (fromGallery) {
      result = await ImagePicker.launchImageLibraryAsync(options)
    } else {
      result = await ImagePicker.launchCameraAsync(options)
    }

    if (!result.cancelled) {
      const manipResult = await ImageManipulator.manipulate(result.uri, [
        { resize: { width: 100, height: 100 } },
      ])
      this.setState({ profilePicture: manipResult.uri })
    }
  };

  removeImage() {
    this.setState({ profilePicture: '' })
    updateProfile(this.props.user.id, { profile_picture: '' })
  }

  async deleteAccount() {
    try {
      await deleteUser()
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      })
      this.props.navigation.dispatch(resetAction)
      this.props.logout()
    } catch (e) {
      Alert.alert('Error', e.error)
    }
  }

  async submit() {
    const {
      firstName, lastName, schoolEmail, profilePicture,
    } = this.state
    let url = ''

    if (!firstName) {
      Alert.alert('First name should not be empty!')
      return
    }
    try {
      if (profilePicture) {
        const blob = await blobMaker(profilePicture)
        url = await uploadImage(this.props.user.id, blob)
      }
      await updateProfile(this.props.user.id, {
        first_name: firstName,
        last_name: lastName,
        email: schoolEmail,
        profile_picture: url,
      })
      this.props.navigation.state.params.updateHeader()
      this.props.navigation.goBack()
    } catch (e) {
      Alert.alert(e.message || e)
    }
  }

  render() {
    const { user } = this.props
    const {
      firstName, lastName, schoolEmail, profilePicture,
    } = this.state

    return (
      <View style={Styles.topContainer}>
        <Header onCancel={this._navigate} onSave={this._submit} title="Edit Account" />
        <View style={Styles.container}>
          <View style={Styles.modalView}>
            <View style={[Styles.flexView, Styles.marginTop]}>
              <View style={Styles.profileImage}>
                <View style={Styles.editImage}>
                  <Image
                    style={{ width: 100, height: 100 }}
                    source={{ uri: profilePicture || '' }}
                  />
                </View>
                <View style={Styles.position}>
                  <Text style={Styles.title}>Profile Picture</Text>
                  <View style={Styles.actionImage}>
                    <TouchableOpacity onPress={this._onOpenActionSheet}>
                      <Text style={Styles.options}>Edit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._removeImage}>
                      <Text style={Styles.options}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={Styles.form}>
                <TextInput
                  underlineColorAndroid="transparent"
                  autoFocus
                  placeholder="First Name"
                  placeholderTextColor="#D8D8D8"
                  maxlength={200}
                  style={
                    this.state.isFocused == 0
                      ? [Styles.textInput, Styles.textInputFocus]
                      : [Styles.textInput, Styles.textInputGray]
                  }
                  onFocus={() => this.onFocus(0)}
                  onChangeText={firstName => this.setState({ firstName })}
                  value={firstName}
                />
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Last Name"
                  placeholderTextColor="#D8D8D8"
                  maxlength={200}
                  style={
                    this.state.isFocused == 1
                      ? [Styles.textInput, Styles.textInputFocus]
                      : [Styles.textInput, Styles.textInputGray]
                  }
                  onFocus={() => this.onFocus(1)}
                  onChangeText={lastName => this.setState({ lastName })}
                  value={lastName}
                />
                <TextInput
                  underlineColorAndroid="transparent"
                  keyboardType="email-address"
                  editable={false}
                  placeholder="School Email"
                  placeholderTextColor="#D8D8D8"
                  maxlength={200}
                  style={[Styles.textInput, Styles.colorGray]}
                  onChangeText={schoolEmail => this.setState({ schoolEmail })}
                  value={schoolEmail}
                />
              </View>
              <View style={Styles.form}>
                <TouchableOpacity onPress={this._deleteAccount}>
                  <Text style={Styles.delete}>Delete Account</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  logout,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Account)
