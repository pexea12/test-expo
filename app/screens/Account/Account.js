import React from 'react'
import {
  Text, View, Image, TouchableOpacity, Alert,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NavigationActions, StackActions } from 'react-navigation'
import Communications from 'react-native-communications'
import Styles from './Styles'
import { logout } from '../../configs/Firebase'
import { logout as logoutUser } from '../../redux/auth/actions'
import Favorites from '../Favorites/Favorites'
import { updateFetchedDate } from '../../redux/dailies/actions'

class Account extends React.Component {
  static navigationOptions = function ({ navigation }) {
    return {
      title: navigation.getParam('myTitle'),
      headerStyle: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingTop: 26,
        height: 55,
      },
      headerTitleStyle: {
        color: '#000',
        fontFamily: 'gt-walsheim-medium',
        fontWeight: '200',
        fontSize: 20,
        alignSelf: 'center',
      },
    }
  };

  constructor(props) {
    super(props)
    this._navigate = this.navigate.bind(this)
    this._logout = this.logout.bind(this)
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  componentDidMount() {
    this.updateHeader()
  }

  updateHeader() {
    this.props.navigation.setParams({
      myTitle: `${this.props.user.first_name} ${this.props.user.last_name}`,
    })
  }

  async logout() {
    try {
      this.props.updateFetchedDate(null)

      await logout()
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'FirstScreen' })],
      })
      this.props.navigation.dispatch(resetAction)
      this.props.logoutUser()
    } catch (e) {
      Alert.alert(e.message || e)
    }
  }

  render() {
    const { user } = this.props

    if (!user)
      return <View></View>

    return (
      <View style={Styles.container}>
        <TouchableOpacity
          style={[Styles.flexRowLeft, { marginTop: '10%' }]}
          onPress={() => {
            this.navigate('SupportResources')
          }}
        >
          <View style={[Styles.chip, Styles.grayBackground]}>
            <Text style={[Styles.textRegular, Styles.chipText]}>Support Resources</Text>
          </View>
        </TouchableOpacity>
        <View style={Styles.modalView}>
          <View style={Styles.flexRow}>
            <Text style={[Styles.fontGTBold, Styles.fontSizeBigger]}>Account</Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('EditAccount', {
                  updateHeader: this.updateHeader.bind(this),
                })
              }}
            >
              <Text style={[Styles.fontGTBold, Styles.opacity, Styles.fontSizeSmaller]}>Edit</Text>
            </TouchableOpacity>
          </View>
          <View style={[Styles.flexView]}>
            <View style={Styles.profileImage}>
              <View style={Styles.image}>
                <Image
                  style={{ width: 70, height: 70 }}
                  source={{ uri: user.profile_picture || '' }}
                />
              </View>
              <View style={Styles.userInformation}>
                <Text style={Styles.fontGTRegular}>
                  {user.first_name}
                  {' '}
                  {user.last_name}
                </Text>
                <View style={Styles.email}>
                  <Text style={Styles.fontGTRegular}>{user.email}</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={[Styles.flexRow, Styles.actions]}>
            <TouchableOpacity onPress={this._logout}>
              <Text style={[Styles.fontGTBold, Styles.opacity, Styles.fontSizeSmaller]}>
                Log Out
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('ResetPassword')
              }}
            >
              <Text style={[Styles.fontGTBold, Styles.opacity, Styles.fontSizeSmaller]}>
                Reset Password
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => Communications.text(
              '',
              'Check out Atlas at https://itunes.apple.com/us/app/atlas-inc/id1423178843?mt=8!',
            )
            }
          >
            <View style={Styles.holder}>
              <Text style={[Styles.fontGTBold, Styles.opacity, Styles.description]}>
                Know someone who'd ❤️ Atlas?
                {' '}
                <Text style={[Styles.fontGTBold, { color: 'red' }]}> Invite</Text>
              </Text>
            </View>
          </TouchableOpacity>
          {/* <View style={Styles.favorites}>
                        <Favorites isChild={true} navigation={this.props.navigation}/>
                    </View> */}
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    logoutUser,
    updateFetchedDate,
  },
  dispatch,
)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Account)
