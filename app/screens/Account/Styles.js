const Styles = {
  topContainer: {
    height: '100%',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    width: '90%',
    height: '95%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: '3%',
    paddingRight: '3%',
  },
  flexView: {
    justifyContent: 'flex-start',
    width: '100%',
  },
  flexRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
  },
  flexRowLeft: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: '100%',
  },
  form: {
    width: '100%',
    marginTop: '5%',
  },
  marginTop: {
    marginTop: '5%',
  },
  center: {
    textAlign: 'center',
  },
  fontGTBold: {
    fontFamily: 'gt-walsheim-bold',
    color: '#343434',
  },
  fontGTRegular: {
    fontFamily: 'gt-walsheim-regular',
    color: '#575757',
    fontSize: 14,
  },
  opacity: {
    opacity: 0.6,
  },
  colorPink: {
    color: '#F36F7A',
  },
  colorGray: {
    color: '#D8D8D8',
  },
  fontSizeBigger: {
    fontSize: 18,
  },
  fontSizeSmaller: {
    fontSize: 14,
  },
  actions: {
    height: 50,
    paddingTop: 25,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  textInput: {
    height: 40,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 16,
    borderBottomWidth: 1,
    marginTop: '5%',
  },
  textInputGray: {
    color: '#9B9B9B',
    borderBottomColor: '#D8D8D8',
  },
  textInputFocus: {
    color: '#F6529D',
    borderBottomColor: '#F6529D',
    fontFamily: 'gt-walsheim-medium',
  },
  selected: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  grayBackground: {
    backgroundColor: '#A8A8A8',
  },
  textRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 14,
    color: 'white',
  },
  chip: {
    flexDirection: 'row',
    borderRadius: 16,
    height: 32,
    paddingTop: 5,
    paddingLeft: 14,
    paddingRight: 8,
    marginBottom: 5,
    marginRight: 6,
    backgroundColor: '#FC636B',
  },
  chipText: {
    color: 'white',
  },
  image: {
    width: 70,
    height: 70,
    overflow: 'hidden',
    borderRadius: 35,
    backgroundColor: '#D8D8D8',
  },
  editImage: {
    width: 100,
    height: 100,
    overflow: 'hidden',
    borderRadius: 50,
    backgroundColor: '#D8D8D8',
  },
  profileImage: {
    width: '100%',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginBottom: '5%',
    marginTop: '5%',
  },
  actionImage: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingTop: 15,
  },
  title: {
    color: '#000',
    fontSize: 17,
    fontFamily: 'gt-walsheim-regular',
  },
  userInformation: {
    paddingTop: 14,
    paddingLeft: 12,
  },
  email: {
    marginTop: 3,
    width: '100%',
    opacity: 0.8,
  },
  position: {
    paddingTop: 20,
    paddingLeft: 20,
  },
  options: {
    color: '#707070',
    fontSize: 15,
    fontFamily: 'gt-walsheim-medium',
    paddingRight: 20,
  },
  favorites: {
    paddingLeft: '1%',
    paddingRight: '1%',
    flex: 2,
    width: '100%',
  },
  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 5,
  },
  delete: {
    color: '#9B9B9B',
    fontSize: 15,
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
    marginTop: 10,
  },
}

export default Styles
