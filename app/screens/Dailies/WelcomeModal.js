import React, { Component } from 'react'
import {
  View,
} from 'react-native'
import Swiper from 'react-native-swiper'

import {
  Introduction,
  Morning,
  Night,
} from '../DailiesModals'


const Dot = (
  <View style={{
    backgroundColor: '#D8D8D8',
    width: 5,
    height: 5,
    borderRadius: 2,
    marginLeft: 19,
    marginRight: 19,
  }}
  />
)

const ActiveDot = (
  <View style={{
    backgroundColor: '#D8D8D8',
    width: 11,
    height: 11,
    borderRadius: 5,
    marginLeft: 19,
    marginRight: 19,
  }}
  />
)

export default class WelcomeModal extends Component {
  scrollTo = (index) => {
    this.swiper.scrollBy(index)
  }

  render() {
    return (
      <Swiper
        loop={false}
        paginationStyle={{ top: 45, bottom: 'auto' }}
        activeDot={ActiveDot}
        dot={Dot}
        ref={(ref) => { this.swiper = ref }}
      >
        <Introduction scrollNext={() => this.scrollTo(1)} />
        <Morning scrollNext={() => this.scrollTo(1)} />
        <Night />
      </Swiper>
    )
  }
}
