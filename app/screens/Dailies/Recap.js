import React from 'react'
import {
  Text,
  Dimensions,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { 
  LinearGradient, 
  Audio, 
  Permissions, 
} from 'expo'
import sortBy from 'lodash/sortBy'
import StarProgress from '../../components/UI/StarProgress'
import moment from '../../configs/Moment'
import { 
  postRecordings, 
  addCompletedCardId, 
  addSkill, 
} from '../../configs/Firebase'
import { segmentAnalyzePage, segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'
import { 
  generateId,
  processErr,
} from '../../configs/Helpers'
import { 
  updateMyRecording, 
  updatePendingRecordings, 
} from '../../redux/checkIn/actions'
import { updateCurrentSkill, getRandomQuote } from '../../redux/dailies/actions'
import StarBackground from '../../components/UI/StarBackground'
import Styles from './Styles'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const iPhoneXPadding = (windowHeight > 800) * 20
const isiPhoneSE = windowHeight < 600

class Recap extends React.Component {
  static navigationOptions() {
    return {
      header: null,
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
      swipeEnabled: false,
    }
  }

  constructor(props) {
    super(props)

    const { navigation } = props
    this.state = {
      loadingAudio: false,
      index: 0,
      playing: false,
      allAudios: [],
      card: navigation.state.params.card,
      isEpisode: navigation.state.params.isEpisode,
      dontSave: navigation.state.params.dontSave,
      saveLoading: false,
      meaningfulValue: navigation.state.params.meaningfulValue,
      textFeedback: navigation.state.params.textFeedback,
      starProgress: [],
    }
  }

  componentWillMount() {
    const { card } = this.state
    if (card.isDaily) {
      segmentAnalyzePage(this.props.user, 'Daily Constellation', { card: card.title })
    } else {
      segmentAnalyzePage(this.props.user, 'Ep Congrats', { card: card.title })
    }
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)

    if (status !== 'granted') {
      alert('Hey! You have not enabled selected permissions')
      this.props.navigation.goBack()
      return
    }

    // segmentAnalyzePage(this.props.user, 'AudioCard Constellation')

    this.loadAudio()
    this.getCards(this.props.myRecordings)
  }

  getStarProgress = (cards) => {
    const starProgress = []
    const completedWeekdays = []
    cards.map((card) => {
      completedWeekdays.push(moment(card.createdAt).isoWeekday())
    })
    for (i = 0; i < 7; i++) {
      if (completedWeekdays.includes(i)) {
        starProgress.push(true)
      } else {
        starProgress.push(false)
      }
    }
    starProgress[moment().isoWeekday()] = true
    return starProgress
  }

  getCards(myRecordings = []) {
    // Problem Line
    // pendingRecordings.forEach(recording => cards.push(recording));
    if (myRecordings.length > 0) {
      const sortedCards = sortBy(myRecordings, 'createdAt').reverse()
      const firstCardDate = sortedCards[sortedCards.length - 1].createdAt
      const daysToSubstract = 0
      const startOfWeek = moment()
        .startOf('isoWeek')
        .subtract(daysToSubstract, 'days')
        .toDate()
      const endOfWeek = moment()
        .endOf('isoWeek')
        .subtract(daysToSubstract, 'days')
        .toDate()
      const dailyCards = this.filterDailyCards(sortedCards, startOfWeek, endOfWeek)
      const starProgress = this.getStarProgress(dailyCards)
      this.setState({ starProgress })
    } else {
      const starProgress = []
      for (i = 0; i < 7; i++) {
        starProgress.push(false)
      }
      starProgress[moment().isoWeekday()] = true
      this.setState({ starProgress })
    }
    // const starProgress = this.setStarProgress(dailyCards)
  }

  componentWillUnmount() {}

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000)
    const seconds = ((millis % 60000) / 1000).toFixed(0)
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  millisToSeconds = millis => millis / 1000


  analyzeCompletedAudio = (index, duration) => {
    const { card } = this.state
    if (card.isDaily) {
      if (!this.state.dailyAnalyzed) {
        console.log('DAILY ANALYZED')
        segmentAnalyzeEvent('Daily Completed', { card: card.title, duration })
      }
      this.setState({ dailyAnalyzed: true })
    } else if (index == 2) {
      if (!this.state.episodeAnalyzed) {
        const allAudioLengths = []
        for (i = 0; i < duration.length; i++) {
          const audioLength = duration[i]
          allAudioLengths.push(audioLength.totalSeconds)
        }
        console.log('EPISODE ANALYZED')
        segmentAnalyzeEvent('Episode Completed', { card: card.title, duration: allAudioLengths })
        this.setState({ episodeAnalyzed: true })
      }
    }
  }

  loadAudio = async () => {
    const { card } = this.state
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })
    Audio.setIsEnabledAsync(true)
    card.audios.map(async (audioObj, index) => {
      const audio = new Audio.Sound()

      this.setState({ loadingAudio: true })

      try {
        await audio.loadAsync({ uri: audioObj.recording })
        audio.setOnPlaybackStatusUpdate((status) => {
          const totalDuration = this.millisToMinutesAndSeconds(status.durationMillis)
          const totalSeconds = this.millisToSeconds(status.durationMillis)
          const currentPosition = this.millisToMinutesAndSeconds(
            status.durationMillis - status.positionMillis,
          )
          const percent = (status.positionMillis / status.durationMillis) * 100
          const { allAudios } = this.state
          const allSounds = [...allAudios]
          allSounds[index] = {
            ...status,
            audio,
            loadingAudio: false,
            currentPosition,
            totalDuration,
            totalSeconds,
            percent,
          }
          this.setState({ allAudios: allSounds }, () => {
            if (card.isDaily) {
              this.analyzeCompletedAudio(index, totalSeconds)
            } else {
              this.analyzeCompletedAudio(index, allSounds)
            }
          })
        })
        audio.setProgressUpdateIntervalAsync(1000)
      } catch (error) {
        // An error occurred!
        // alert('error' + JSON.stringify(error))
      }
    })
  }

  play = async (index) => {
    const { allAudios } = this.state

    allAudios.map(async (obj) => {
      await obj.audio.stopAsync()
    })
    if (allAudios[index].percent === 100) {
      allAudios[index].audio.replayAsync()
    } else {
      allAudios[index].audio.setPositionAsync(allAudios[index].positionMillis)
      allAudios[index].audio.playAsync()
    }
  }

  pause = async (index) => {
    const { allAudios } = this.state

    await allAudios[index].audio.pauseAsync()
  }

  async unload() {
    const { allAudios } = this.state

    allAudios.map(audioObj => audioObj.audio && audioObj.audio.unloadAsync())
  }

  filterDailyCards = (cards, startOfWeek, endOfWeek) => {
    return cards.filter(
      card => card.isDaily && card.createdAt >= startOfWeek && card.createdAt <= endOfWeek,
    )
  }

  complete = async () => {
    const { card } = this.state
    await this.submit()
    await this.props.getRandomQuote()

    if (card.isDaily) {
      this.props.navigation.navigate('Feedback', {
        card,
        index: 0,
      })
    } else {
      this.props.navigation.navigate('Feedback', { card })
    }
  }

  submit = async () => {
    try {
      this.setState({ saveLoading: true })

      const { 
        pendingRecordings = [], 
        updatePendingRecordings, 
        updateCurrentSkill,
      } = this.props

      const { 
        card, 
        textFeedback, 
        meaningfulValue, 
      } = this.state

      await addCompletedCardId(card)
      if (card.skill) {
        const updatedSkill = await addSkill(card.skill)
        updateCurrentSkill(updatedSkill)
      }

      const newCard = { ...card }
      newCard.audios.forEach((a, index) => {
        newCard.audios[index].downloadedUri = a.recording
      })

      const uniqueId = generateId()
      newCard.firestoreKey = uniqueId
      newCard.textFeedback = textFeedback
      newCard.meaningfulValue = meaningfulValue

      pendingRecordings.push(newCard)
      updatePendingRecordings(pendingRecordings)

      const firestoreKey = await postRecordings(newCard)
      
      const recordings = pendingRecordings.filter(
        recording => recording.firestoreKey !== firestoreKey,
      )

      await updatePendingRecordings(recordings)
      this.setState({ saveLoading: false })
    } catch (e) {
      this.setState({ saveLoading: false })
      processErr(e, "We're unable to save your audios at this moment")
    }
  }

  renderPlayer({ loadingAudio, percent, isPlaying }, index) {
    return (
      <TouchableOpacity
        onPress={isPlaying ? this.pause.bind(this, index) : this.play.bind(this, index)}
      >
        <ProgressCircle
          percent={percent}
          radius={20}
          borderWidth={1}
          color="#ef9a9f"
          shadowColor="#b073b1"
          bgColor="#a9529c"
        >
          {!loadingAudio ? (
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {isPlaying ? (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/pause-button.png')}
                />
              ) : (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/play-button.png')}
                />
              )}
            </View>
          ) : (
            <ActivityIndicator size="large" />
          )}
        </ProgressCircle>
      </TouchableOpacity>
    )
  }

  renderEpisodeAudios() {
    const { allAudios } = this.state
    return allAudios.length
      ? allAudios.map((audio, index) => (audio ? (
        <View
          key={index} 
          style={{
            flexDirection: 'column', 
            justifyContent: 'space-between', 
            alignItems: 'center', 
            padding: '7%', 
            paddingTop: '5%',
          }}
        >
          <Text style={[Styles.cardTitle, {
            fontSize: 14, color: 'white', fontFamily: 'gt-walsheim-bold', letterSpacing: 3.33, marginBottom: 0,
          }, audio.isPlaying && { color: '#ef9a9f' }]}
          >
            {`PART ${index + 1}`}
          </Text>
          {this.renderPlayer(audio, index)}
          <Text style={[{ color: 'white', alignSelf: 'center', fontFamily: 'gt-walsheim-bold' }, audio.isPlaying && { color: '#ef9a9f' }]}>{audio.percent > 0 ? audio.currentPosition : audio.totalDuration}</Text>
        </View>
      ) : <ActivityIndicator size="large" />)) : <ActivityIndicator size="large" />
  }

  renderAudios() {
    const { allAudios } = this.state
    return allAudios.length
      ? allAudios.map((audio, index) => (audio ? (
        <View
          key={index} 
          style={{
            width: '100%', 
            flexDirection: 'row', 
            justifyContent: 'space-between', 
            alignItems: 'center',
          }}
        >
          <Text style={[Styles.cardTitle, {
            fontSize: 14, color: 'white', fontFamily: 'gt-walsheim-medium', letterSpacing: 3.33, marginBottom: 0,
          }, audio.isPlaying && { color: '#ef9a9f' }]}
          >
YOUR THOUGHTS
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[{
              color: 'white', alignSelf: 'center', opacity: 0.7, fontFamily: 'gt-walsheim-medium', right: 8,
            }, audio.isPlaying && { opacity: 1.0, fontFamily: 'gt-walsheim-bold', color: '#ef9a9f' }]}
            >
              {audio.percent > 0 ? audio.currentPosition : audio.totalDuration}
            </Text>
            {this.renderPlayer(audio, index)}
          </View>
        </View>
      ) : <ActivityIndicator size="large" />)) : <ActivityIndicator size="large" />
  }

  getHeightMargin() {
    if (windowHeight >= 800) return -windowHeight * 0.15
    if (windowHeight < 600) return -windowHeight * 0.45
    return -windowHeight * 0.2
  }

  renderSaveButton() {
    const { saveLoading } = this.state
    return !saveLoading ? (
      <TouchableOpacity style={{ padding: 10 }} onPress={this.complete}>
        <Text
          style={{
            fontFamily: 'gt-walsheim-medium',
            color: 'white',
            fontSize: 16,
            marginBottom: 0,
          }}
        >
          Next
        </Text>
      </TouchableOpacity>
    ) : (
      <ActivityIndicator size="large" />
    )
  }

  render() {
    const { card, dontSave, starProgress } = this.state
    const defaultColor = ['#040029', '#1D0E57', '#371D88', '#4F1C82', '#691A7C']

    return (
      <View
        style={{
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20,
          paddingBottom: 20,
          justifyContent: 'center',
          backgroundColor: 'rgba(0,0,0,0.85)',
        }}
      >
        <LinearGradient
          style={{
            position: 'absolute',
            width: windowWidth,
            height: windowHeight * 1.1,
            top: -iPhoneXPadding * 1.1,
          }}
          colors={defaultColor}
        />
        <StarBackground />
        <View
          style={{
            width: '100%',
            zIndex: 1000,
            top: 35,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}
        >
          {!dontSave && this.renderSaveButton()}
        </View>
        <View
          style={{
            flex: 9 + isiPhoneSE * 3,
            top: -15 * isiPhoneSE,
            alignItems: 'center',
            justifyContent: 'space-around',
          }}
        >
          <View
            style={{
              marginTop: 50,
              alignItems: 'center',
              justifyContent: 'flex-end',
              paddingBottom: iPhoneXPadding,
            }}
          >
            <Text
              style={[
                {
                  fontFamily: 'gt-walsheim-regular',
                  textAlign: 'center',
                  fontSize: 20,
                  lineHeight: 26,
                  color: 'white',
                },
              ]}
            >
              {`Congratulations on \n completing ${card.title}`}
            </Text>
          </View>
          {card.isDaily && <StarProgress isDaily progress={starProgress} />}
          <View
            style={{
              width: windowWidth * 0.9,
              top: 15,
              padding: 12,
              justifyContent: 'center',
              borderWidth: 0.5,
              borderColor: 'white',
              backgroundColor: 'rgba(216, 216, 216, 0.15)',
              borderRadius: 14,
            }}
          >
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {this.state.isEpisode ? this.renderEpisodeAudios() : this.renderAudios()}
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    cards: state.checkIn.cards,
    myRecordings: state.checkIn.myRecordings,
    pendingRecordings: state.checkIn.pendingRecordings,
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  updateMyRecording,
  updatePendingRecordings,
  updateCurrentSkill,
  getRandomQuote,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Recap)
