import React from 'react'
import {
  Dimensions,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  ScrollView,
  ActivityIndicator,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  InteractionManager,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { LinearGradient } from 'expo'
import { Analytics, PageHit } from 'expo-analytics'
import sortBy from 'lodash/sortBy'
import moment from '../../configs/Moment'
import Styles from './Styles'
import { listenUserStats } from '../../redux/checkIn/actions'
import AudioFooter from '../AudioFooter/AudioFooter'
import StarProgress from '../../components/UI/StarProgress'
import StarBackground from '../../components/UI/StarBackground'
import SkillCircle from '../../components/UI/SkillCircle'
import { segmentAnalyzePage, segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'

import Loader from '../../components/UI/Loader'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const analytics = new Analytics('UA-118205605-4')

const isiPhoneSE = windowHeight < 600

class Dailies extends React.Component {
  static navigationOptions({ navigation }) {
    return {
      header: () => (
        <View
          style={{
            flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'white',
          }}
        />
      ),
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
      swipeEnabled: false,
    }
  }

  static transitionConfig() {
    return {
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
      },
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      cards: [],
      stats: [],
      starProgress: [],
      goBackWeeks: 0,
      progress: [],
      outOfRange: false,
      didFinishInitialAnimation: false,
    }
    this._navigate = this.navigate.bind(this)
    this.filterDailyCards = this.filterDailyCards.bind(this)
    this.changeDailiesWeek = this.changeDailiesWeek.bind(this)
  }

  componentDidMount() {
    segmentAnalyzePage(this.props.user, 'Dailies Log')
    // Timer just in case InteractionManager stalls

    InteractionManager.runAfterInteractions(() => {
      // 2: Component is done animating
      // 3: Start fetching the team
      this.props.listenUserStats(this.props.user.id)
      this.getCards(this.props.myRecordings, this.props.pendingRecordings)
      this.getStats(this.props.user_stats)
      this.setState({
        didFinishInitialAnimation: true,
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      if (this.props.myRecordings && nextProps.myRecordings) {
        if (this.props.myRecordings.length !== nextProps.myRecordings.length) {
          this.getCards(nextProps.myRecordings)
          this.setState({
            didFinishInitialAnimation: true,
          })
        }
      }
      if (this.props.user_stats && nextProps.user_stats) {
        if (this.props.user_stats.length !== nextProps.user_stats.length) {
          this.getStats(nextProps.user_stats)
          this.setState({
            didFinishInitialAnimation: true,
          })
        }
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.goBackWeeks !== prevState.goBackWeeks) {
      this.getCards(this.props.myRecordings, this.props.pendingRecordings)
      this.setState({
        didFinishInitialAnimation: true,
      })
    }
  }

  getStarProgress = (cards) => {
    const starProgress = []
    const completedWeekdays = []
    cards.map((card) => {
      completedWeekdays.push(moment(card.createdAt).isoWeekday())
    })
    for (i = 0; i < 7; i++) {
      if (completedWeekdays.includes(i)) {
        starProgress.push(true)
      } else {
        starProgress.push(false)
      }
    }
    return starProgress
  };

  getCards(myRecordings = [], pendingRecordings = []) {
    // Problem Line
    // pendingRecordings.forEach(recording => cards.push(recording));
    const sortedCards = sortBy(myRecordings, 'createdAt').reverse()
    const firstCardDate = myRecordings.length == 0 ? moment() : sortedCards[sortedCards.length - 1].createdAt
    const daysToSubstract = this.state.goBackWeeks * 7
    const startOfWeek = moment()
      .startOf('isoWeek')
      .subtract(daysToSubstract, 'days')
      .toDate()
    const endOfWeek = moment()
      .endOf('isoWeek')
      .subtract(daysToSubstract, 'days')
      .toDate()
    const dailyCards = this.filterDailyCards(sortedCards, startOfWeek, endOfWeek)
    const starProgress = this.getStarProgress(dailyCards)
    const outOfRange = firstCardDate > endOfWeek
    // const starProgress = this.setStarProgress(dailyCards)
    this.setState({ starProgress })
    this.setState({ cards: dailyCards })
    this.setState({ pendingRecordings })
    this.setState({ outOfRange })
  }

  getStats = (user_stats = []) => {
    // Problem Line
    // pendingRecordings.forEach(recording => cards.push(recording));
    this.setState({ stats: user_stats, didFinishInitialAnimation: true })
  };

  navigate(card) {
    this.props.navigation.navigate('AllAudios', { card, dontSave: true })
  }

  privacyDialog() {
    Alert.alert(
      'All your data is private 🔒',
      'In this age of data collection, Atlas is deeply committed to your privacy. Atlas does not listen to, sell, or market the recordings you make.',
    )
  }

  filterDailyCards(cards, startOfWeek, endOfWeek) {
    return cards.filter(
      card => card.isDaily && card.createdAt >= startOfWeek && card.createdAt <= endOfWeek,
    )
  }

  changeDailiesWeek(moveTo) {
    const { goBackWeeks } = this.state
    if (goBackWeeks + moveTo >= 0) this.setState({ goBackWeeks: goBackWeeks + moveTo })
  }

  renderSkillStats = () => {
    const { stats } = this.state
    return (
      <FlatList
        data={stats}
        horizontal
        contentContainerStyle={{ flex: 1, justifyContent: 'space-around' }}
        style={{ top: 25, padding: 20 }}
        renderItem={({ item: skill }) => (
          <View style={{ alignItems: 'center' }}>
            <SkillCircle skill={skill.name} points={skill.level} size={70} />
            <Text style={{
              top: 15, color: 'white', fontSize: 15, textTransform: 'capitalize', fontFamily: 'gt-walsheim-regular',
            }}
            >
              {' '}
              {skill.name}
              {' '}
            </Text>
          </View>
        )}
      />
    )
  };

  renderDateRange = () => {
    const daysToSubstract = this.state.goBackWeeks * 7
    const startOfWeek = moment()
      .startOf('isoWeek')
      .subtract(daysToSubstract, 'days')
      .format('MMM D')
    const endOfWeek = moment()
      .endOf('isoWeek')
      .subtract(daysToSubstract, 'days')
      .format('MMM D, YYYY')
    return (
      <Text
        style={{
          fontFamily: 'gt-walsheim-medium',
          fontSize: 16,
          color: 'white',
          textAlign: 'center',
        }}
      >
        {startOfWeek}
        {' '}
-
        {endOfWeek}
      </Text>
    )
  };

  render() {
    const {
      cards, disabled, outOfRange, pendingRecordings, stats, goBackWeeks,
    } = this.state
    const defaultColor = ['#040029', '#1D0E57', '#371D88', '#4F1C82', '#691A7C']
    return (
      <View style={Styles.container}>
        <LinearGradient
          colors={defaultColor}
          style={{
            flex: 1,
            width: windowWidth,
            height: windowHeight - isiPhoneSE * windowHeight * 0.1,
          }}
        >
          <ScrollView style={Styles.homeList} onScroll={this._onScroll}>
            <StarBackground />
            <Text
              style={{
                fontFamily: 'gt-walsheim-medium',
                letterSpacing: 7.8,
                fontSize: 16,
                color: 'white',
                textAlign: 'center',
              }}
            >
              DAILY RECORDINGS
            </Text>
            {!this.state.didFinishInitialAnimation ? (
              <Loader />
            ) : (
              <View>
                <View
                  style={{
                    flex: 1,
                    paddingRight: 35,
                    top: 20,
                    zIndex: 1000,
                    paddingLeft: 35,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                  }}
                >
                  <TouchableOpacity disabled={outOfRange} onPress={() => this.changeDailiesWeek(1)}>
                    {!outOfRange ? (
                      <Image
                        source={require('../../assets/images/backArrow.png')}
                        style={{ right: 20, width: 24, height: 15 }}
                      />
                    ) : (
                      <View style={{ width: 20 }} />
                    )}
                  </TouchableOpacity>
                  {this.renderDateRange()}
                  <TouchableOpacity onPress={() => this.changeDailiesWeek(-1)}>
                    {!(goBackWeeks == 0) ? (
                      <Image
                        source={require('../../assets/images/forwardArrow.png')}
                        style={{ left: 20, width: 24, height: 15 }}
                      />
                    ) : (
                      <View style={{ width: 20 }} />
                    )}
                  </TouchableOpacity>
                </View>
                <View style={{ top: 20 }}>
                  <StarProgress index={this.state.goBackWeeks} progress={this.state.starProgress} />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: 20,
                    paddingLeft: 25,
                    paddingRight: 25,
                    flex: 1,
                  }}
                >
                  {this.renderSkillStats()}
                </View>
                {cards.length > 0 ? (
                  <FlatList
                    data={cards}
                    style={{
                      top: 25, flex: 1, paddingLeft: 20, paddingRight: 20,
                    }}
                    renderItem={({ item: card }) => (
                      <View style={{
                        paddingLeft: 20, paddingRight: 20, flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: 80, alignItems: 'center', borderColor: 'white', borderWidth: 0.5, borderRadius: 11, backgroundColor: 'rgba(216,216,216,0.1)', marginBottom: 15,
                      }}
                      >
                        <View style={{ flex: 3 }}>
                          <Text style={{
                            fontFamily: 'gt-walsheim-regular', textTransform: 'uppercase', fontSize: 12, letterSpacing: 3.4, opacity: 0.7, color: 'white',
                          }}
                          >
                            {moment(card.createdAt).format('MMM D, YYYY h:mma')}
                          </Text>
                          <Text style={{ fontFamily: 'gt-walsheim-medium', fontSize: 17, color: 'white' }}>{card.audioTitle}</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ListenAllAudios', { transition: 'modal', card, dontSave: true })}>
                            <View style={{
                              width: 70, height: 30, borderRadius: 5, backgroundColor: '#B8399C', alignItems: 'center', justifyContent: 'center',
                            }}
                            >
                              <Text style={{ fontFamily: 'gt-walsheim-regular', color: 'white' }}>Listen</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    )}
                  />
                ) : (
                  <Text style={Styles.emptyState}>No Dailies Completed This Week</Text>
                )}
                <TouchableOpacity onPress={() => this.privacyDialog()} style={{ marginTop: 30 }}>
                  <Text
                    style={{
                      fontFamily: 'gt-walsheim-medium',
                      fontSize: 14,
                      textAlign: 'center',
                      color: '#64B1EB',
                    }}
                  >
                    Your Recordings Are Private
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </ScrollView>
        </LinearGradient>
        <AudioFooter navigation={this.props.navigation} isDailies="true" />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user_stats: state.checkIn.user_stats,
  user: state.auth.user,
  cards: state.checkIn.cards,
  myRecordings: state.checkIn.myRecordings,
  pendingRecordings: state.checkIn.pendingRecordings,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenUserStats,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dailies)
