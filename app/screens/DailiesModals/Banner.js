import React from 'react'
import {
  Text,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'
import Gradient from 'react-native-css-gradient'

const { height } = Dimensions.get('window')

const labels = {
  morning: {
    gradient: `linear-gradient(
      178.01deg, 
      #62119F 1.45%, 
      #84219E 46.56%, 
      #CD739E 83.52%, 
      #F39F9E 123.45%, 
      rgba(243, 159, 158, 0.0001) 160.35%
    )`,
    logo: require('../../assets/images/sun.png'),
    completedLogo: require('../../assets/images/sun-completed.png'),
    boxShadow: {
      shadowColor: 'rgba(255, 154, 0, 0.5)',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.72,
      shadowRadius: 10,
      elevation: 5,
    },
  },
  night: {
    gradient: `linear-gradient(
      154.51deg, 
      #494E94 -30.11%, 
      #696DC4 4.43%, 
      #8385EB 31.93%, 
      #91FAFE 91.07%
    )`,
    logo: require('../../assets/images/moon.png'),
    completedLogo: require('../../assets/images/moon-completed.png'),
    boxShadow: {
      shadowColor: 'rgba(255, 255, 255, 0.227519)',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.72,
      shadowRadius: 5,
      elevation: 5,
    },
  },
}

const Banner = (props) => {
  const {
    title,
    disabled,
    type,
    completed,
    subTitle,
    touchable,
    onPress,
  } = props

  const {
    completedLogo,
    logo,
    gradient,
    boxShadow,
  } = labels[type]

  const Container = touchable ? TouchableOpacity : View

  return (
    <Container
      style={[styles.wrapper, completed && boxShadow]}
      onPress={onPress}
    >
      <Gradient
        gradient={gradient}
        style={styles.container}
      >
        {completed && (
        <Image
          source={require('../../assets/images/checkMark.png')}
          style={styles.completedLogo}
        />
        )}

        <Image
          source={completed ? completedLogo : logo}
          style={{ width: 65, height: 48 }}
        />
        <Text style={styles.title}>
          {title}
        </Text>

        <Text style={styles.subTitle}>
          {subTitle}
        </Text>
      </Gradient>

      {disabled && <View style={styles.background} />}
    </Container>
  )
}

Banner.defaultProps = {
  disabled: false,
  completed: false,
  touchable: false,
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'rgb(255, 0, 0)',
    flex: 1,
    height: height / 3.1,
    marginLeft: 9,
    marginRight: 9,
    borderRadius: 8,
  },

  completedLogo: {
    width: 28,
    height: 28,
    position: 'absolute',
    right: 7,
    top: 9,
  },

  background: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 8,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },

  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 8,
  },

  title: {
    color: 'white',
    textAlign: 'center',
    width: '75%',
    fontFamily: 'gt-walsheim-medium',
    fontSize: 16,
    lineHeight: 20,
    marginTop: 20,
  },

  subTitle: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 13,
    lineHeight: 18,
    color: 'white',
    opacity: 0.53,
  },
})

export default Banner
