import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
} from 'react-native'

import BannerList from './BannerList'
import NextButton from './NextButton'
import {
  updateShowInstruction,
} from '../../redux/dailies/actions'

class Night extends React.Component {
  cancelModal = () => {
    this.props.updateShowInstruction(false)
  }

  render() {
    const {
      imageURL,
    } = this.props

    const uri = imageURL || require('../../assets/images/modalBG.png')
    return (
      <View>
        <ImageBackground
          source={uri}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={styles.background} />

          <BannerList disableLeft />

          <View style={styles.textContainer}>
            <Text style={styles.title}>
              NIGHT
            </Text>
            <Text style={styles.text}>
              Night Atlas helps you reflect about the day / your identity
            </Text>
          </View>

          <View style={styles.buttonContainer}>
            <NextButton
              text="DONE"
              onPress={this.cancelModal}
            />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    updateShowInstruction,
  },
  dispatch,
)

export default connect(
  null,
  mapDispatchToProps,
)(Night)


const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 100,
  },

  title: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
    letterSpacing: 12.22,
    lineHeight: 26,
    marginTop: 14,
  },

  text: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    lineHeight: 28,
    marginTop: 25,
    width: '80%',
  },

  buttonContainer: {
    flex: 1,
    marginBottom: 32,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  background: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    position: 'absolute',
  },
})
