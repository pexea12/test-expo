import React from 'react'
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
} from 'react-native'

import BannerList from './BannerList'
import NextButton from './NextButton'


export default class Introduction extends React.Component {
  handleOnPress = () => {
    const { parentNavigation } = this.props
    parentNavigation.navigate('login')
  }

  render() {
    const {
      imageURL,
      scrollNext,
    } = this.props

    const uri = imageURL || require('../../assets/images/modalBG.png')
    return (
      <View>
        <ImageBackground
          source={uri}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={styles.background} />

          <BannerList />

          <View style={styles.textContainer}>
            <Text style={styles.introducing}>
              INTRODUCING
            </Text>
            <Text style={styles.title}>
              ATLAS DAILY
            </Text>
            <Text style={styles.text}>
              Here&apos;s some text about the Atlas Dailies etc
            </Text>
          </View>

          <View style={styles.buttonContainer}>
            <NextButton onPress={scrollNext} />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 100,
  },

  introducing: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
    lineHeight: 18,
    letterSpacing: 6,
  },

  title: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
    letterSpacing: 12.22,
    lineHeight: 26,
    marginTop: 14,
  },

  text: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    lineHeight: 19,
    marginTop: 35,
  },

  buttonContainer: {
    flex: 1,
    marginBottom: 32,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  background: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    position: 'absolute',
  },
})
