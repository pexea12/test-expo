import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'

import Banner from './Banner'


const BannerList = (props) => {
  const {
    disableLeft,
    disableRight,
  } = props

  return (
    <View style={styles.container}>
      <Banner
        title="Do One Good Thing"
        type="morning"
        disabled={disableLeft}
      />
      <Banner
        title="What Do You Really Want?"
        type="night"
        disabled={disableRight}
      />
    </View>
  )
}

BannerList.defaultProps = {
  disableLeft: false,
  disableRight: false,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginLeft: 15,
    marginRight: 15,
    marginTop: 90,
  },
})

export default BannerList
