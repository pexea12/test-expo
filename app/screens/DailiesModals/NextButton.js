import React from 'react'
import {
  Text,
  TouchableHighlight,
  StyleSheet,
} from 'react-native'


class NextButton extends React.Component {
  handlePress = () => {
    const { onPress } = this.props
    onPress()
  }

  render() {
    const { text } = this.props

    return (
      <TouchableHighlight
        style={styles.touchableWrapper}
        onPress={this.handlePress}
      >
        <Text style={styles.touchableHighlight}>
          {text}
        </Text>
      </TouchableHighlight>
    )
  }
}

NextButton.defaultProps = {
  text: 'NEXT',
}

const styles = StyleSheet.create({
  touchableHighlight: {
    color: '#5659A0',
    fontFamily: 'gt-walsheim-bold',
    textAlign: 'center',
    letterSpacing: 3.55,
    fontSize: 16,
    lineHeight: 19,
  },
  touchableWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D8D8D8',
    width: 261,
    height: 58,
    borderRadius: 27,
  },
})

export default NextButton
