import React from 'react'
import {
  NetInfo,
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'

import posed from 'react-native-pose'
import { LinearGradient, FileSystem, Notifications } from 'expo'
import Communications from 'react-native-communications'
import { Analytics, PageHit, Event } from 'expo-analytics'
import Styles from './Styles'
import { updateProfile, firebase, postRecordings } from '../../configs/Firebase'


import {
  listenAudioCards,
  listenMyRecordings,
  updateMyRecording,
  updatePendingRecordings,
  listenCompletedCardIds,
} from '../../redux/checkIn/actions'
import AudioFooter from '../AudioFooter/AudioFooter'
import SingleCardViewAudio from '../SingleCardView/SingleCardViewAudio'
import { onUserChanged } from '../../redux/auth/actions'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'


const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height


const analytics = new Analytics('UA-131293770-1')
const isiPhoneSE = windowHeight < 600
const isiPhoneX = windowHeight > 800

const tween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 2000,
})
const slowerTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1000,
  delay: 500,
})

const starConfig = {
  enter: {
    opacity: 1,
    scale: 1.02,
    transition: tween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.6,
    transition: tween,
  },
}
const starConfig2 = {
  enter: {
    opacity: 1,
    scale: 1.02,
    transition: slowerTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.6,
    transition: slowerTween,
  },
}

const StarAnimate = posed.View(starConfig)
const StarAnimate2 = posed.View(starConfig2)

class Episodes extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: () => (
      <View
        style={{
          flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'white',
        }}
      />
    ),
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  });

  static transitionConfig = {
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      cards: [],
      myRecordings: [],
      yOffset: 0,
      completedCardIds: [],
      starAnimate: false,
    }

    this.uploadPendings = this.uploadPendings.bind(this)
  }

  authUser() {
    return new Promise(((resolve, reject) => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          onUserChanged(user)
          resolve(user)
        } else {
          reject('User not logged in')
        }
      })
    }))
  }


  componentDidMount() {
    if (firebase.auth().currentUser) {
      this.props.listenAudioCards()
      this.props.listenMyRecordings(this.props.user.id)
      this.props.listenCompletedCardIds(this.props.user.id)
      this.getCards(this.props.cards)
      this.getRecordings(this.props.myRecordings)
      this.getCompletedCardIds(this.props.completedCardIds)
      NetInfo.addEventListener(
        'connectionChange',
        this.uploadPendings,
      )
      this.isDownloadedAudios = false
    } else {
      this.authUser().then(
        (user) => {
          this.props.listenAudioCards()
          this.props.listenMyRecordings(user.id)
          this.props.listenCompletedCardIds(this.props.user.id)
          this.getCards(this.props.cards)
          this.getRecordings(this.props.myRecordings)
          this.getCompletedCardIds(this.props.completedCardIds)
          NetInfo.addEventListener(
            'connectionChange',
            this.uploadPendings,
          )
          this.isDownloadedAudios = false
        }, (error) => {
          alert('Something went wrong!')
        },
      )
    }
    segmentAnalyzePage(this.props.user, 'Episodes Library')
    setInterval(() => {
      this.setState({
        starAnimate: !this.state.starAnimate,
      })
    }, 3000)
  }

  uploadPendings(connectionInfo) {
    let { pendingRecordings, updatePendingRecordings } = this.props

    if (connectionInfo.type !== 'none' && pendingRecordings) {
      pendingRecordings.forEach(async (recording) => {
        try {
          const response = await postRecordings(recording)

          pendingRecordings = pendingRecordings.filter(recording => recording.firestoreKey !== response)
          updatePendingRecordings(pendingRecordings)
        } catch (e) {
          console.log('e =>', e)
        }
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      if (this.props.cards && nextProps.cards) {
        if (this.props.cards.length != nextProps.cards.length) {
          this.getCards(nextProps.cards)
        }
      }
      if (this.props.myRecordings && nextProps.myRecordings) {
        if (this.props.myRecordings.length != nextProps.myRecordings.length) {
          this.getRecordings(nextProps.myRecordings)
        }
      }
      if (this.props.completedCardIds && nextProps.completedCardIds) {
        if (this.props.completedCardIds.length != nextProps.completedCardIds.length) {
          this.getCompletedCardIds(nextProps.completedCardIds)
        }
      }
    }

    if (nextProps.myRecordings && !this.isDownloadedAudios) {
      this.isDownloadedAudios = true
      this.prepareDownloadAudiosLocally(nextProps.myRecordings)
    }
  }

  getCards(allCards = []) {
    const cards = sortBy(allCards, 'createdAt')
    this.setState({ cards })
    this.prepareDownloadLibraryAudiosLocally(cards)
  }

  // Download Library Audios

  async prepareDownloadLibraryAudiosLocally(cards) {
    const CardAudioPromises = []
    cards && cards.forEach(card => CardAudioPromises.push(this.downloadLibraryAudiosLocally(card)))
    try {
      const responses = await Promise.all(CardAudioPromises)
      this.setState({ cards: responses })
    } catch (e) {
      console.log(e)
    }
  }

  async downloadLibraryAudiosLocally(card) {
    const promises = []

    card && card.audios && card.audios.forEach((audio) => {
      promises.push(this.downloadLibraryAudioLocally(audio))
    })

    try {
      const responses = await Promise.all(promises)
      card.audios.forEach((c, index) => {
        card.audios[index].libraryDownloadedUri = responses[index].uri
      })

      return card
    } catch (e) {
      console.log(e)
    }
  }

  downloadLibraryAudioLocally(audio) {
    const urlToDownload = audio.url
    const name = urlToDownload.slice(urlToDownload.length - 30, urlToDownload.length)

    return new Promise(async (resolve) => {
      try {
        if (audio.libraryDownloadedUri) {
          resolve({ uri: audio.libraryDownloadedUri })
        } else {
          const uri = await FileSystem.downloadAsync(
            urlToDownload,
            `${FileSystem.documentDirectory + name}.wav`,
          )
          resolve(uri)
        }
      } catch (e) {
        console.log(e)
      }
    })
  }

  // Download Recordings

  async prepareDownloadAudiosLocally(myRecordings) {
    const { updateMyRecording } = this.props
    const AudioPromises = []

    myRecordings && myRecordings.forEach(recording => AudioPromises.push(this.downloadCardAudiosLocally(recording)))

    try {
      const responses = await Promise.all(AudioPromises)
      updateMyRecording(responses)
    } catch (e) {
      console.log(e)
    }
  }

  async downloadCardAudiosLocally(card) {
    const promises = []

    card && card.audios && card.audios.forEach((audio) => {
      promises.push(this.downloadAudioLocally(audio))
    })

    try {
      const responses = await Promise.all(promises)
      card.audios.forEach((c, index) => {
        card.audios[index].downloadedUri = responses[index].uri
      })

      return card
    } catch (e) {
      console.log(e)
    }
  }

  downloadAudioLocally(audio) {
    const urlToDownload = audio.recording

    const name = urlToDownload.slice(urlToDownload.length - 30, urlToDownload.length)

    return new Promise(async (resolve) => {
      try {
        if (audio.downloadedUri) {
          resolve({ uri: audio.downloadedUri })
        } else {
          const uri = await FileSystem.downloadAsync(
            urlToDownload,
            `${FileSystem.documentDirectory + name}.m4a`,
          )
          resolve(uri)
        }
      } catch (e) {
        console.log(e)
      }
    })
  }

  getRecordings(myRecordingsData = []) {
    const myRecordings = sortBy(myRecordingsData, 'createdAt').reverse()
    this.setState({ myRecordings })
  }

  getCompletedCardIds(completedCardIds = []) {
    this.setState({ completedCardIds })
  }

  goToEpisodes = () => {
    if (this.state.yOffset < 0.5 * windowHeight) {
      this.scroll.scrollTo({ x: 0, y: 0.9 * windowHeight, animated: true })
    } else {
      this.scroll.scrollTo({ x: 0, y: 0, animated: true })
    }
  };

  // TODO
  getFeaturedIndex = () => {
    const { completedCardIds, myRecordings } = this.state

    //  TODO: Update User State to viewed onboarding when begin is clicked, else render 1
    if (!this.state.completedCardIds || this.state.completedCardIds.length == 0) {
      return 0
    } if ((!this.state.completedCardIds || this.state.completedCardIds.length == 0) && this.props.user.viewedEducationCarousel) {
      return 1
    }
    const completedIds = map(completedCardIds, 'featuredId')
    const filtered = map(completedIds, n => n || 0)
    const max = filtered.reduce((a, b) => Math.max(a, b))
    return max
  }


  navigateFeaturedCard = (featuredCard) => {
    this.props.navigation.navigate('AudioCard', { card: featuredCard })
  };

  getLockedStatus = (card) => {
    // If hasnt viewed into, lock all
    const featuredIndex = this.getFeaturedIndex()
    if (card.featuredId > featuredIndex + 1) {
      return true
    }
    return false
  };

  // TODO: swap out with real card
  getFeaturedCard = () => {
    const { cards } = this.state
    let index = this.getFeaturedIndex()
    if (index > 3) {
      index = 3
    }
    return cards[index]
  }

  isCardCompleted = (card) => {
    const { myRecordings } = this.state
    const completedIds = map(myRecordings, 'featuredId')
    if (completedIds && completedIds.includes(card.featuredId)) {
      return true
    }
    return false
  };

  render() {
    const { cards, yOffset } = this.state
    const { user } = this.props
    const cardWidth = (windowWidth - 60) / 3
    const featuredCard = this.getFeaturedCard()
    const up = yOffset > 0.5 * windowHeight

    const defaultColor = ['#62119F', '#A12E9D', '#B6369C', '#F39F9E', '#ffffff00']


    return (
      <View style={[Styles.container]}>
        <ScrollView
          style={[Styles.homeList]}
          ref={(c) => {
            this.scroll = c
          }}
          onScroll={(event) => {
            this.setState({ yOffset: event.nativeEvent.contentOffset.y })
          }}
        >
          <View style={[Styles.headerHome]}>
            <LinearGradient
              colors={
                featuredCard && featuredCard.colors ? featuredCard.colors : defaultColor
              }
              style={[
                Styles.topBox,
                { flex: 1, height: windowHeight - isiPhoneSE * windowHeight * 0.1 },
              ]}
            >
              <StarAnimate pose={this.state.starAnimate ? 'enter' : 'exit'}>
                <Image
                  resizeMode="contain"
                  source={require('../../assets/images/starBackground.png')}
                  style={{
                    position: 'absolute',
                    top: -windowHeight * 0.08,
                    opacity: 0.9,
                    width: windowWidth * 1.08,
                  }}
                />
              </StarAnimate>
              <StarAnimate2 pose={this.state.starAnimate ? 'enter' : 'exit'}>
                <Image
                  resizeMode="contain"
                  source={require('../../assets/images/starBackground.png')}
                  style={{
                    position: 'absolute',
                    top: -windowHeight * 0.09,
                    left: -windowHeight * 0.02,
                    opacity: 0.95,
                    width: windowWidth * 1.08,
                  }}
                />
              </StarAnimate2>
              <StarAnimate2 pose={!this.state.starAnimate ? 'enter' : 'exit'}>
                <Image
                  resizeMode="contain"
                  source={require('../../assets/images/starBackground.png')}
                  style={{
                    position: 'absolute',
                    top: -windowHeight * 0.2,
                    left: -windowHeight * 0.08,
                    opacity: 0.95,
                    width: windowWidth * 1.08,
                  }}
                />
              </StarAnimate2>
              <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 40 }}>
                <Text
                  style={[
                    Styles.typeComment,
                    {
                      color: 'white',
                      opacity: 0.8,
                      letterSpacing: 3.11,
                      fontSize: 10,
                      fontFamily: 'gt-walsheim-medium',
                      alignSelf: 'center',
                    },
                  ]}
                >
                  EPISODE
                </Text>
                <Text
                  style={[
                    Styles.typeComment,
                    {
                      color: 'white',
                      textAlign: 'left',
                      fontFamily: 'gt-walsheim-bold',
                      fontSize: 15,
                      alignSelf: 'center',
                    },
                  ]}
                >
                  {featuredCard && featuredCard.featuredId}
                  <Text
                    style={{
                      fontFamily: 'gt-walsheim-regular',
                      textAlign: 'center',
                      opacity: 0.8,
                      letterSpacing: 3.1,
                    }}
                  >
                    {' '}
                    OF
                    {' '}
                  </Text>
                  4
                </Text>
              </View>
              <View>
                <Text
                  style={[
                    {
                      marginTop: '30%',
                      alignSelf: 'center',
                      fontSize: 18 + 3 * isiPhoneX,
                      letterSpacing: 3,
                      fontFamily: 'gt-walsheim-medium',
                      color: 'white',
                    },
                  ]}
                >
                  {featuredCard && featuredCard.title.toUpperCase()}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    opacity: 0.7,
                    marginTop: '9%',
                    justifyContent: 'center',
                  }}
                >
                  <Image
                    style={{ width: 20, height: 20, opacity: 0.75 }}
                    source={require('../../assets/images/clock.png')}
                  />
                  <Text
                    style={[
                      Styles.typeComment,
                      {
                        marginLeft: '2%',
                        color: 'white',
                        alignSelf: 'center',
                        fontSize: 15 + 2 * isiPhoneX,
                        letterSpacing: 0,
                      },
                    ]}
                  >
                    Est
                    {' '}
                    {featuredCard && featuredCard.completionTime}
                  </Text>
                </View>
                <Text
                  style={[
                    Styles.typeComment,
                    {
                      textAlign: 'center',
                      marginTop: '9%',
                      color: 'white',
                      alignSelf: 'center',
                      fontSize: 15 + 2 * isiPhoneX,
                      letterSpacing: 0,
                      fontFamily: 'gt-walsheim-regular',
                    },
                  ]}
                >
                  {featuredCard && featuredCard.blurb}
                </Text>
                <TouchableOpacity
                  onPress={() => this.navigateFeaturedCard(featuredCard)}
                  style={[Styles.glowButton, { marginTop: '12%' }]}
                >
                  <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Image
                      style={{
                        width: 12, height: 15, marginLeft: 18, top: 4,
                      }}
                      source={require('../../assets/images/play_purple.png')}
                    />
                    <Text
                      style={{
                        flex: 1,
                        marginLeft: '12%',
                        fontSize: 18,
                        alignSelf: 'center',
                        fontFamily: 'gt-walsheim-bold',
                        color: '#BE449C',
                      }}
                    >
                      Begin
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={this.goToEpisodes}
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  bottom: isiPhoneSE ? windowHeight * 0.06 : 100,
                }}
              >
                {up ? (
                  <Image
                    style={{ width: 24, height: 12 }}
                    source={require('../../assets/images/upArrow.png')}
                  />
                ) : (
                  <Image
                    style={{ width: 24, height: 12 }}
                    source={require('../../assets/images/downArrow.png')}
                  />
                )}
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <View
            style={[Styles.contentContainer, { flex: 1, background: 'white', maxWidth: '100%' }]}
          >
            <View style={{ marginBottom: 30, width: '100%', flex: 1 }}>
              {/* Intro To Atlas Card */}
              <View style={{ marginRight: 20 }}>
                <Text
                  style={[
                    Styles.cardsCompleted,
                    {
                      fontSize: 18,
                      textAlign: 'center',
                      letterSpacing: 2.5,
                      alignSelf: 'center',
                      fontFamily: 'gt-walsheim-bold',
                      color: '#404040',
                      textTransform: 'uppercase',
                    },
                  ]}
                >
                  Season One
                </Text>
                <Text
                  style={[
                    Styles.typeComment,
                    {
                      marginTop: '2%',
                      marginBottom: '5%',
                      color: '#404040',
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontFamily: 'gt-walsheim-regular',
                    },
                  ]}
                >
                  Explore the dimensions of your identity
                </Text>
              </View>

              {/* <TouchableOpacity style={{marginRight: 20}} onPress={() => this.props.navigation.navigate('IntroAudioCard')}>
                                <LinearGradient colors={['#64B1EB', '#A653E7']} start={[0, 1]} end={[1, 0]} style={[Styles.contentShadowBox, {flex: 1}]}>

                                    <View style={{flexDirection: 'row', width: '100%', flex: 1, maxWidth: '100%', alignItems: 'flex-end'}}>
                                        <View style={{justifyContent: 'flex-start', flex: 1, maxWidth: '100%'}}>
                                            <Text style={[Styles.articleTitle, {fontSize: 18, opacity: 0.7, fontFamily: 'gt-walsheim-regular', textTransform: 'uppercase', letterSpacing: 1.8}]}>YOUR PLACE IN THE WORLD</Text>
                                            <Text style={[Styles.articleTitle]}>Introduction</Text>
                                        </View>
                                    </View>
                                 </LinearGradient>
                            </TouchableOpacity>  */}

              {/* Episodes */}

              {cards.length == 0 && (
              <View style={Styles.emptyStateWrapper}>
                <Text style={Styles.emptyState}>No cards yet!</Text>
              </View>
              )}
              {cards && (
              <FlatList
                data={cards}
                style={{ flex: 1, width: '100%' }}
                renderItem={({ item: card }) => (
                  <View style={{ marginRight: 20 }}>
                    {<SingleCardViewAudio
                      navigation={this.props.navigation}
                      card={card}
                      style={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                      isLocked={this.getLockedStatus(card)}
                      isCardCompleted={this.isCardCompleted(card)}
                    />}
                  </View>
                )}
              />
              )}
              <TouchableOpacity
                onPress={() => Communications.text(
                  '',
                  'Check out Atlas at https://itunes.apple.com/us/app/atlas-inc/id1423178843?mt=8!',
                )
                }
              >
                <View style={Styles.holder}>
                  <Text
                    style={[
                      Styles.fontGTBold,
                      Styles.opacity,
                      Styles.description,
                      {
                        alignSelf: 'center',
                        color: '#5F62B4',
                        opacity: 1,
                        fontSize: 16,
                        marginTop: 20,
                      },
                    ]}
                  >
                    Share Atlas with A Friend ⛰
                  </Text>
                </View>
              </TouchableOpacity>
              {
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('CreateNotification')}
                >
                  <Text
                    style={[
                      Styles.cardsCompleted,
                      {
                        color: 'red',
                        fontFamily: 'gt-walsheim-bold',
                        alignSelf: 'center',
                        left: -8,
                      },
                    ]}
                  >
                    Admin: Create Custom Notification
                  </Text>
                </TouchableOpacity>
              }
            </View>
          </View>
        </ScrollView>
        <AudioFooter navigation={this.props.navigation} isEpisodes="true" />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  cards: state.checkIn.cards,
  myRecordings: state.checkIn.myRecordings,
  pendingRecordings: state.checkIn.pendingRecordings,
  completedCardIds: state.checkIn.completedCardIds,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenAudioCards,
  listenMyRecordings,
  onUserChanged,
  updateMyRecording,
  updatePendingRecordings,
  listenCompletedCardIds,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Episodes)
