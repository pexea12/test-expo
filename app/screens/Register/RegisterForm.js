import React from 'react'
import {
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Keyboard,
  Alert,
  ImageBackground,
  Dimensions,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Styles from './Styles'
import GradientButton from '../../components/Buttons/GradientButton'
import { register } from '../../configs/Firebase'
import { onUserChanged } from '../../redux/auth/actions'
import { segmentAnalyzePage, segmentSignup } from '../../configs/SegmentAnalytics'
import Loader from '../../components/UI/Loader'
import { emailRegex } from './utils'


class RegisterForm extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      firstName: '',
      lastName: '',
      schoolEmail: '',
      password: '',
      shortHeight: 0,
      keyboard: false,
      loading: false,
      isFocused: -1,
      privacyOpen: false,
      tcOpen: false,
    }
  }

  componentDidMount() {
    // this.updateApp()
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    segmentAnalyzePage('anon', 'Onboarding Register')
  }

  componentDidUpdate(prevProps) {
    if (this.props.user && prevProps.user == null) {
      this.props.navigation.navigate('AudioHome')
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  onFocus(index) {
    this.setState({ isFocused: index })
  }

  setLoading(bool) {
    this.setState({ loading: bool })
  }

  keyboardDidHide = () => {
    this.setState({ keyboard: false })
  }

  keyboardDidShow = (e) => {
    const shortHeight = Dimensions.get('window').height - e.endCoordinates.height
    this.setState({ keyboard: true, shortHeight })
  }


  navigateToLogin = () => {
    this.props.navigation.navigate('Login')
  }

  signup = async () => {
    Keyboard.dismiss()
    const {
      firstName,
      lastName,
      schoolEmail,
      password,
    } = this.state
    if (this.validate()) {
      this.setLoading(true)
      try {
        const user = await register({
          firstName,
          lastName,
          schoolEmail,
          password,
        })
        // Alert.alert('Thank You!', 'A confirmation message has been sent to ' + schoolEmail)
        segmentSignup(user)
        this.props.onUserChanged(user.id)
        this.props.navigation.navigate('AudioHome', {
          transition: 'fade',
        })
      } catch (e) {
        Alert.alert('Error', e.error)
      } finally {
        this.setLoading(false)
      }
    }
  }

  togglePrivacy() {
    this.setState(({ privacyOpen }) => ({ privacyOpen: !privacyOpen }))
  }

  toggleTermsAndConditions() {
    this.setState(({ tcOpen }) => ({ tcOpen: !tcOpen }))
  }

  validate() {
    const { schoolEmail, password } = this.state

    if (!emailRegex.test(schoolEmail.toLowerCase())) {
      alert('Enter valid email')
      return false
    }
    if (password.length < 8) {
      alert('Minimum length of password field is 8 characters')
      return false
    }
    if (password.length > 16) {
      alert('Maximum length of password field is 16 characters')
      return false
    }
    return true
  }

  render() {
    const {
      firstName,
      lastName,
      schoolEmail,
      password,
      keyboard,
      shortHeight,
      loading,
    } = this.state
    const disabled = !firstName || !lastName || !schoolEmail || !password

    return (
      <View style={Styles.container}>
        <ImageBackground
          source={require('../../assets/images/bg5.png')}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={
            keyboard
              ? [Styles.container, Styles.containerKeyboard]
              : Styles.container
          }
          >
            {loading && <Loader />}
            <View
              style={[
                Styles.modalView,
                keyboard && shortHeight
                  ? { height: shortHeight }
                  : Styles.modalHeight,
              ]}
            >
              {!keyboard && (
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontSize: 48,
                      fontFamily: 'gt-walsheim-medium',
                      color: 'white',
                    }}
                  >
                    Atlas
                  </Text>
                </View>
              )}

              <View style={Styles.marginTop}>
                <View>
                  <TextInput
                    underlineColorAndroid="transparent"
                    placeholder="First Name"
                    placeholderTextColor="#D8D8D8"
                    maxlength={200}
                    style={
                      this.state.isFocused === 0
                        ? [Styles.textInput, Styles.textInputFocus]
                        : [Styles.textInput, Styles.textInputGray]
                    }
                    onFocus={() => this.onFocus(0)}
                    onChangeText={name => this.setState({ firstName: name })}
                    value={firstName}
                  />
                  <TextInput
                    underlineColorAndroid="transparent"
                    placeholder="Last Name"
                    placeholderTextColor="#D8D8D8"
                    maxlength={200}
                    style={
                      this.state.isFocused === 1
                        ? [Styles.textInput, Styles.textInputFocus]
                        : [Styles.textInput, Styles.textInputGray]
                    }
                    onFocus={() => this.onFocus(1)}
                    onChangeText={name => this.setState({ lastName: name })}
                    value={lastName}
                  />
                  <TextInput
                    underlineColorAndroid="transparent"
                    keyboardType="email-address"
                    placeholder="Email"
                    placeholderTextColor="#D8D8D8"
                    maxlength={200}
                    autoCapitalize="none"
                    style={
                      this.state.isFocused === 2
                        ? [Styles.textInput, Styles.textInputFocus]
                        : [Styles.textInput, Styles.textInputGray]
                    }
                    onFocus={() => this.onFocus(2)}
                    onChangeText={email => this.setState({ schoolEmail: email })}
                    value={schoolEmail}
                  />
                  <TextInput
                    underlineColorAndroid="transparent"
                    placeholder="Password (8+ characters)"
                    placeholderTextColor="#D8D8D8"
                    secureTextEntry
                    maxlength={16}
                    style={
                      this.state.isFocused === 3
                        ? [Styles.textInput, Styles.textInputFocus]
                        : [Styles.textInput, Styles.textInputGray]
                    }
                    onFocus={() => this.onFocus(3)}
                    onChangeText={passwordData => this.setState({ password: passwordData })}
                    value={password}
                  />
                </View>
                <View style={[Styles.form, Styles.textWrap]}>
                  <Text style={[Styles.text, Styles.colorGray]}>
                    By signing up, you agree to our
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('TermsAndConditions')
                    }}
                  >
                    <Text style={[Styles.text, Styles.colorPink]}>
                      Terms of Service
                    </Text>
                  </TouchableOpacity>
                  <Text style={[Styles.text, Styles.colorGray]}> &</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('Privacy')
                    }}
                  >
                    <Text style={[Styles.text, Styles.colorPink]}>
                      Privacy Policy.
                    </Text>
                  </TouchableOpacity>
                </View>
                <View>
                  {!loading && (
                    <TouchableOpacity
                      disabled={disabled}
                      onPress={this.signup}
                    >
                      <GradientButton
                        text="Sign up"
                        disabled={disabled}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>

            {!keyboard && (
              <View style={Styles.footer}>
                <Text style={[Styles.center, Styles.colorWhite, Styles.fontRegular]}>
                  Already have an account?
                </Text>
                <TouchableOpacity onPress={this.navigateToLogin}>
                  <Text style={[Styles.colorBrand, Styles.fontRegular]}>
                    Sign In
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    onUserChanged,
  },
  dispatch,
)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterForm)
