import { StyleSheet } from 'react-native'


const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerKeyboard: {
    justifyContent: 'flex-start',
    paddingTop: 15,
  },
  modalView: {
    maxWidth: 400,
    width: '90%',
    justifyContent: 'flex-start',
    borderRadius: 7,
    marginTop: '14%',
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  modalHeight: {
    height: '90%',
  },
  fontRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 14,
  },
  colorBrand: {
    color: '#ED4D96',
  },
  textWrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '8%',
    marginTop: '8%',
  },
  form: {
    width: '100%',
    justifyContent: 'center',
  },
  colorGray: {
    color: '#9B9B9B',
  },
  colorPink: {
    color: '#F6529D',
  },
  text: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
  },
  textInput: {
    height: 40,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 16,
    borderBottomWidth: 1,
    marginTop: '5%',
  },
  textInputGray: {
    color: '#9B9B9B',
    borderBottomColor: '#D8D8D8',
  },
  textInputFocus: {
    color: '#F6529D',
    borderBottomColor: '#F6529D',
    fontFamily: 'gt-walsheim-medium',
  },
  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 20,
  },
  colorWhite: {
    color: '#FFFFFF',
  },
  center: {
    textAlign: 'center',
  },
})

export default Styles
