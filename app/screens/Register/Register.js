import React, { Component } from 'react'
import {
  Text,
  TouchableHighlight,
  StyleSheet,
} from 'react-native'
import { connect } from 'react-redux'
import { Wizard } from '../../components/Steps'
import {
  Generic,
  MicrophoneBtn,
  GridSelect,
} from '../OnboardingSteps'
import { segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'
import { emailRegex } from './utils'


const GenericNextBtn = ({ children, onPress }) => (
  <TouchableHighlight
    onPress={onPress}
    style={styles.touchableHighlight}
  >
    <Text style={styles.nextText}>
      {children}
    </Text>
  </TouchableHighlight>
)

const MicrophoneBtnWrapper = ({ onPress }) => (
  <MicrophoneBtn onPressCallBack={onPress} />
)

const GridSelectWrapperManage = ({ onPress }) => (
  <GridSelect
    skillValue="stress anxiety"
    skillText="Manage stress and anxiety"
    onPressCallBack={onPress}
  />
)

const GridSelectWrapperBuild = ({ onPress }) => (
  <GridSelect
    skillValue="optimism self-esteem"
    skillText="Build optimism and self esteem"
    onPressCallBack={onPress}
  />
)

const GridSelectWrapperFind = ({ onPress }) => (
  <GridSelect
    skillValue="focus clarity"
    skillText="Find focus and clarity"
    onPressCallBack={onPress}
  />
)

const GridSelectWrapperChecking = ({ onPress }) => (
  <GridSelect
    skillValue="none"
    skillText="Just checking it out"
    onPressCallBack={onPress}
  />
)

class Register extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {
      micPermission: false,
      selectedSkill: null,
      schoolEmail: '',
      password: '',
    }
  }

  handleSubmitWizard = () => {
    this.props.navigation.navigate('RegisterForm', {
      formSelect: this.state.selectedSkill,
    })
  };

  handleMicClick = (status) => {
    this.setState({ micPermission: status !== 'granted' })
  };

  handleSkillClick = (selectedSkill) => {
    segmentAnalyzeEvent('Onboarding Form Select', { selectedSkill })
    this.setState({ selectedSkill }, this.handleSubmitWizard)
  };

  onChangeCallback = (formState) => {
    this.setState(formState)
  };

  validate() {
    const { schoolEmail, password } = this.state

    if (!emailRegex.test(schoolEmail.toLowerCase())) {
      alert('Enter valid email')
      return false
    }
    if (password.length < 8) {
      alert('Minimum length of password field is 8 characters')
      return false
    }
    if (password.length > 16) {
      alert('Maximum length of password field is 16 characters')
      return false
    }

    return true
  }

  render() {
    const steps = [
      {
        component: (
          <Generic
            index="1"
            parentNavigation={this.props.navigation}
            imageURL={require('../../assets/images/bg2.png')}
          />
        ),
        routeName: 'SayIt',
        blockNext: false,
        nextBtns: [
          {
            wrapper: GenericNextBtn,
          },
        ],
      },
      {
        component: (
          <Generic
            index="2"
            parentNavigation={this.props.navigation}
            imageURL={require('../../assets/images/bg3.png')}
          />
        ),
        routeName: 'Micro',
        blockNext: this.state.micPermission,
        nextBtns: [
          {
            wrapper: MicrophoneBtnWrapper,
            callback: this.handleMicClick,
          },
        ],
      },
      {
        component: (
          <Generic
            index="3"
            parentNavigation={this.props.navigation}
            imageURL={require('../../assets/images/bg4.png')}
          />
        ),
        routeName: 'Space',
        blockNext: false,
        nextBtns: [
          {
            wrapper: GenericNextBtn,
          },
        ],
      },
      {
        component: (
          <Generic
            index="4"
            parentNavigation={this.props.navigation}
            imageURL={require('../../assets/images/bg6.png')}
          />
        ),
        routeName: 'Skills',
        noGlow: true,
        nextBtns: [
          {
            wrapper: GridSelectWrapperManage,
            callback: this.handleSkillClick,
          },
          {
            wrapper: GridSelectWrapperFind,
            callback: this.handleSkillClick,
          },
          {
            wrapper: GridSelectWrapperBuild,
            callback: this.handleSkillClick,
          },
          {
            wrapper: GridSelectWrapperChecking,
            callback: this.handleSkillClick,
          },
        ],
      },
    ]

    return (
      <Wizard
        handleSubmitWizard={this.handleSubmitWizard}
        steps={steps}
        navigation={this.props.navigation}
      />
    )
  }
}

const styles = StyleSheet.create({
  touchableHighlight: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowRadius: 20,
    shadowColor: 'white',
    shadowOpacity: 0.75,
    width: 200,
    margin: 15,
    borderRadius: 25,
    padding: 15,
  },
  nextText: {
    color: '#55599B',
    fontFamily: 'gt-walsheim-bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: 2.0,
    fontSize: 15,
  },
})

const mapStateToProps = state => ({
  user: state.auth.user,
})

export default connect(
  mapStateToProps,
)(Register)
