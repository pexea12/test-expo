import React from 'react'
import {
  Modal,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ImageBackground,
  Alert,
} from 'react-native'
import { LinearGradient } from 'expo'
import Styles from './Styles'
import GradientButton from '../../components/Buttons/GradientButton'

export default class Terms extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Modal animationType="slide" transparent visible onRequestClose={() => {}}>
        <TouchableHighlight style={Styles.modal}>
          <View style={Styles.modalView}>
            <View>
              <Text style={Styles.heading}>Community Guidelines</Text>
            </View>
            <View>
              <Text style={Styles.subtitle}>tldr; just be a good person</Text>
              <Text style={Styles.paragraph}>
                Atlas isn't just any regular old app. It's a space to check in with those around you
                and share you're feeling -- you can't carry the weight of the world on your own.
              </Text>
            </View>
            <View style={Styles.buttonView}>
              <TouchableOpacity onPress={this.props.onAccept}>
                <GradientButton text="I Accept" />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    )
  }
}
