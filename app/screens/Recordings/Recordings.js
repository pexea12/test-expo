import React from 'react'
import {
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import sortBy from 'lodash/sortBy'
import { Notifications, LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import moment from '../../configs/Moment'
import Styles from './Styles'
import BaseStyles from '../BaseStyles/Styles'
import { listenAudioCards } from '../../redux/checkIn/actions'
import AudioFooter from '../AudioFooter/AudioFooter'
import SingleCardViewAudio from '../SingleCardView/SingleCardViewAudio'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const analytics = new Analytics('UA-118205605-4')

const iPhoneXPadding = (windowHeight > 800) * 20

class Recordings extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: () => (
      <View
        style={{
          flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'white',
        }}
      />
    ),
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  });

  static transitionConfig = {
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      cards: [],
    }
    this._navigate = this.navigate.bind(this)
  }

  componentDidMount() {
    this.getCards(this.props.myRecordings, this.props.pendingRecordings)
    segmentAnalyzePage(this.props.user, 'Episodes Log')
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      if (this.props.myRecordings && nextProps.myRecordings) {
        if (this.props.myRecordings.length != nextProps.myRecordings.length) {
          this.getCards(nextProps.myRecordings)
        }
      }
    }
  }

  getCards(myRecordings = [], pendingRecordings = []) {
    const sortedCards = sortBy(myRecordings, 'createdAt').reverse()
    const episodeCards = sortedCards.filter(card => !card.isDaily)
    this.setState({ cards: episodeCards })
    this.setState({ pendingRecordings })
  }

  navigate(card) {
    this.props.navigation.navigate('AllAudios', { card, dontSave: true })
  }

  privacyDialog() {
    Alert.alert(
      'All your data is private 🔒',
      'In this age of data collection, Atlas is deeply committed to your privacy. Atlas does not listen to, sell, or market the recordings you make.',
    )
  }

  render() {
    const { cards, pendingRecordings } = this.state

    return (
      <View style={Styles.container}>
        <ScrollView style={Styles.homeList} onScroll={this._onScroll}>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 20 + iPhoneXPadding / 2,
              justifyContent: 'space-between',
              alignItems: 'center',
              height: 75 + iPhoneXPadding,
              backgroundColor: 'white',
            }}
          >
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('FeedbackForm', { transition: 'modal' })
                }
              >
                <Text
                  style={{
                    width: 70,
                    right: -15,
                    fontFamily: 'gt-walsheim-medium',
                    fontSize: 13,
                    textAlign: 'left',
                    color: '#64B1EB',
                  }}
                >
                  Send Feedback
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ left: -12, flex: 1, alignItems: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Account', { transition: 'modal' })
                }}
              >
                <Image
                  style={{ width: 25, height: 25 }}
                  source={require('../../assets/images/settings.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <Text
            style={[
              Styles.mainTitle,
              {
                fontSize: 16, flex: 1, letterSpacing: 7, textAlign: 'center',
              },
            ]}
          >
            EPISODE RECORDINGS
          </Text>
          {pendingRecordings && pendingRecordings.length > 0 && (
          <View
            style={{
              paddingLeft: 20,
              paddingRight: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
              height: 80,
              alignItems: 'center',
              borderBottomWidth: 1,
              borderBottomColor: '#D8D8D8',
            }}
          >
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <ActivityIndicator />
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'gt-walsheim-regular',
                  marginTop: '5%',
                  opacity: 0.7,
                }}
              >
                  Your new recording is saving...
              </Text>
            </View>
          </View>
          )}
          {cards.length > 0
            ? (
              <FlatList
                data={cards}
                style={{ flex: 1, width: '100%' }}
                renderItem={({ item: card }) => (
                  <View style={{
                    paddingLeft: 20,
                    paddingRight: 20,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    height: 80,
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderBottomColor: '#D8D8D8',
                  }}
                  >
                    <View style={{ flex: 3 }}>
                      <Text style={[Styles.mainTitle, { fontSize: 20 }]}>
                        {card.title}
                      </Text>
                      <Text style={[Styles.mainTitle, { fontSize: 14, color: '#D8D8D8' }]}>
                        {moment(card.createdAt).format('MMM D, YYYY h:mma')}
                      </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('ListenAllAudios', { transition: 'modal', card, dontSave: true })}>
                        <View style={{
                          width: 70,
                          height: 30,
                          borderRadius: 5,
                          backgroundColor: '#B8399C',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        >
                          <Text style={{
                            fontFamily: 'gt-walsheim-regular',
                            color: 'white',
                          }}
                          >
                          Listen
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              />
            )
            : (
              <Text style={Styles.emptyState}>
                No episodes completed yet!
              </Text>
            )
          }
          <TouchableOpacity onPress={() => this.privacyDialog()} style={{ marginTop: 12 }}>
            <Text
              style={{
                fontFamily: 'gt-walsheim-medium',
                fontSize: 14,
                textAlign: 'center',
                color: '#64B1EB',
              }}
            >
              Your Recordings Are Private
            </Text>
          </TouchableOpacity>
        </ScrollView>
        <AudioFooter navigation={this.props.navigation} isRecordings="true" />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  cards: state.checkIn.cards,
  myRecordings: state.checkIn.myRecordings,
  pendingRecordings: state.checkIn.pendingRecordings,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenAudioCards,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Recordings)
