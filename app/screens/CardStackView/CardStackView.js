import React from 'react'
import {
  Linking, ScrollView, Modal, FlatList, Text, Dimensions, View, Slider, processColor, TouchableOpacity, Animated, TouchableWithoutFeedback, Alert, TextInput, Keyboard, ActivityIndicator, Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import Carousel from 'react-native-snap-carousel'
import posed from 'react-native-pose'
import find from 'lodash/find'
import Communications from 'react-native-communications'
import { listenCardsFromStack, stopListenStack } from '../../redux/checkIn/actions'
import moment from '../../configs/Moment'
import Header from '../../components/Header/Header'
import { postComment, getMembers, postCard } from '../../configs/Firebase'
import Styles from './Styles'
import CardPreviewModal from '../CardPreviewModal/CardPreviewModal'


const analytics = new Analytics('UA-118205605-4')

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const categories = ['Basics', 'Stress', 'Asking for Help', 'Thoughts']

const fastTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 500,
})

const delayTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 500,
  delay: 200,
})


const config = {
  state0: {
    opacity: 1.0,
    transition: delayTween,
  },
  state1: {
    opacity: 0.0,
    transition: delayTween,
  },
}

const completionConfig2 = {
  state0: {
    y: windowHeight * 0.05,
    opacity: 1.0,
    transition: fastTween,
  },
  state1: {
    y: windowHeight,
    opacity: 0.0,
    transition: fastTween,
  },
}
const TextinputWrapper = posed.View(completionConfig2)
const FadeWrapper = posed.View(config)

class CardStackView extends React.Component {
    static navigationOptions = {
      header: null,
    };

    constructor(props) {
      super(props)
      this.state = {
        stack_cards: [],
        loading: false,
        animateBox: false,
        displayText: false,
        currentIndex: 0,
        modalVisible: false,
        longText: false,
        response: '',
        members: [],
      }
      this._submit = this.submit.bind(this)
    }

    componentDidMount() {
      const { stackId } = this.props.navigation.state.params
      analytics.event(new Event('PageView', 'CardStackView', stackId))
        .then(() => console.log('success'))
        .catch(e => console.log(e.message))
    }


    componentWillMount() {
      const { stackId } = this.props.navigation.state.params
      this.props.listenCardsFromStack(stackId)
      this.getCards(this.props.stack_cards)
      this.getMembers()
    }

    setModalVisible = (visible) => {
      this.setState({ modalVisible: visible })
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.user) {
        if (this.props.stack_cards || nextProps.stack_cards) {
          if (this.props.stack_cards && this.props.stack_cards.length != nextProps.stack_cards.length) {
            this.getCards(nextProps.stack_cards)
          } else {
            this.getCards(this.props.stack_cards)
          }
        }
      }
    }

    async getMembers() {
      try {
        const members = await getMembers(this.props.user)
        this.setState({ members })
      } catch (e) {
        Alert.alert(e.error)
      }
    }

    componentWillUnmount() {
      const { stackId } = this.props.navigation.state.params
      this.props.stopListenStack(stackId)
    }

    async submit(card) {
      const { response } = this.state
      if (card && card.type != null && card.type == 'form') {
        const cardComplete = {
          response, content: card.content, type: card.type, createdAt: Date.now(), userId: this.props.user.id, stack: card.stack,
        }
        this.submitHelper(cardComplete)
      } else if (card && card.type != null && card.type == 'song') {
        const cardComplete = {
          response, content: card.content, caption: card.caption, type: 'song', createdAt: Date.now(), userId: this.props.user.id, stack: card.stack,
        }
        this.submitHelper(cardComplete)
      } else {
        const cardComplete = {
          caption: card.caption, content: card.content, type: 'quote', createdAt: Date.now(), userId: this.props.user.id, stack: card.stack,
        }
        this.submitHelper(cardComplete)
      }
    }


    async getCards(stack_cards = []) {
      try {
        this.setState({ stack_cards })
      } catch (e) {
        Alert.alert('Error', e.message || '')
      }
    }

    async submitHelper(card) {
      try {
        await postCard(this.props.user.id, card)
      } catch (e) {
        Alert.alert('Error', e.message)
      }
    }

    renderCard = ({ item, index }) => {
      const { stack } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      if (item && item.type == 'form') {
        return (
          <TouchableOpacity
            onPress={() => { this.setState({ displayText: !this.state.displayText, response: '' }) }}
            style={{
              shadowOffset: { width: 4, height: 4 },
              shadowColor: 'rgba(0, 0, 0, 0.25)',
              shadowOpacity: 1.0,
              backgroundColor: 'white',
              height: '80%',
              left: windowWidth * 0.035,
              paddingTop: 10,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              width: '95%',
              borderRadius: 8,
              padding: 20,
            }}
          >
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {item && item.content != null && (
              <Text style={{
                color: '#6D16A0', fontFamily: 'gt-walsheim-regular', fontSize: 24, lineHeight: 32, textAlign: 'center',
              }}
              >
                {item.content}
              </Text>
              )}
              <Text style={{
                color: 'black', opacity: 0.5, top: 15, letterSpacing: 1.5, fontFamily: 'gt-walsheim-bold', fontSize: 18, textAlign: 'center',
              }}
              >
TAP TO ANSWER
              </Text>
            </View>
          </TouchableOpacity>
        )
      } if (item && item.type == 'song') {
        return (
          <View
            style={{
              shadowOffset: { width: 4, height: 4 },
              shadowColor: 'rgba(0, 0, 0, 0.25)',
              shadowOpacity: 1.0,
              backgroundColor: 'white',
              height: '80%',
              left: windowWidth * 0.035,
              paddingTop: 10,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              width: '95%',
              borderRadius: 8,
              padding: 20,
            }}
          >
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {item && item.content != null && (
              <Text style={{
                color: '#6D16A0', fontFamily: 'gt-walsheim-regular', fontSize: 24, lineHeight: 32, textAlign: 'center',
              }}
              >
                {item.content}
              </Text>
              )}
              {item && item.caption && (
              <Text style={{
                color: 'black', top: 15, opacity: 0.5, letterSpacing: 3.1, fontFamily: 'gt-walsheim-bold', fontSize: 18, textAlign: 'center',
              }}
              >
                {item.caption.toUpperCase()}
              </Text>
              )}

              <TouchableOpacity style={{ alignItems: 'center', marginTop: 32 }} onPress={() => { Linking.openURL(item.song_link) }}>
                <Image source={require('../../assets/images/listen_on_spotify.png')} style={{ width: 242, height: 100 }} />
              </TouchableOpacity>
            </View>
          </View>
        )
      }
      return (
        <View style={{
          shadowOffset: { width: 4, height: 4 },
          shadowColor: 'rgba(0, 0, 0, 0.25)',
          shadowOpacity: 1.0,
          backgroundColor: 'white',
          height: '80%',
          left: windowWidth * 0.035,
          paddingTop: 10,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '95%',
          borderRadius: 8,
          padding: 20,
        }}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
            {item && item.content && (
            <Text style={{
              color: '#6D16A0', fontFamily: 'gt-walsheim-regular', fontSize: 24, lineHeight: 32, textAlign: 'center',
            }}
            >
              {item.content}
            </Text>
            )}
            {item && item.caption && (
            <Text style={{
              color: 'black', top: 15, opacity: 0.5, letterSpacing: 0.4, fontFamily: 'gt-walsheim-bold', fontSize: 18, textAlign: 'center',
            }}
            >
              {item.caption.toUpperCase()}
            </Text>
            )}
          </View>
        </View>
      )
    }

    shuffleStack = (a) => {
      let j; let x; let
        i
      for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1))
        x = a[i]
        a[i] = a[j]
        a[j] = x
      }
      return a
    }

    changeIndex = (currentIndex) => {
      this.setState({ currentIndex })
    }

    renderTextInput = () => {
      const {
        stack_cards, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack } = this.props.navigation.state.params
      return (
        <TextinputWrapper
          pose={!displayText ? 'state1' : 'state0'}
          style={{
            backgroundColor: '#FAFAFA',
            position: 'absolute',
            zIndex: 1000,
            width: '103%',
            height: '95%',
            borderRadius: 22,
            opacity: 0.98,
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <ScrollView>
            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity
                style={{
                  justifyContent: 'center', backgroundColor: 'transparent', width: 70, height: 40, borderRadius: 8, marginTop: 20,
                }}
                onPress={() => { Keyboard.dismiss(); this.setState({ displayText: !this.state.displayText, isVisible: !this.state.isVisible }) }}
              >
                <Text style={{
                  fontFamily: 'gt-walsheim-bold', color: 'white', textAlign: 'center', fontSize: 16, color: '#494E94',
                }}
                >
Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  justifyContent: 'center', backgroundColor: '#494E94', width: 70, height: 40, borderRadius: 8, marginTop: 20,
                }}
                onPress={() => {
                  Keyboard.dismiss()
                  this._submit(stack_cards[currentIndex])
                  this.setState({
                    modalVisible: true,
                    displayText: !this.state.displayText,
                    isVisible: !this.state.isVisible,
                  })
                }}
              >
                <Text style={{
                  fontFamily: 'gt-walsheim-bold', color: 'white', textAlign: 'center', fontSize: 16,
                }}
                >
Save
                </Text>
              </TouchableOpacity>
            </View>
            {stack_cards && stack_cards[currentIndex] && <Text style={Styles.questionForm}>{stack_cards[currentIndex].content}</Text>}
            <TextInput
              ref="responseForm"
              blurOnSubmit
              underlineColorAndroid="transparent"
              placeholder="Your Thoughts Here"
              multiline
              returnKeyType="none"
              onFocus={() => { this.setState({ longText: !this.state.longText }) }}
              onEndEditing={() => { this.setState({ response: this.refs.responseForm._lastNativeText, longText: !this.state.longText }) }}
              style={longText ? {
                height: windowHeight * 0.20, fontFamily: 'gt-walsheim-regular', fontSize: 20, lineHeight: 28, top: 20,
              } : {
                fontFamily: 'gt-walsheim-regular', fontSize: 20, lineHeight: 28, top: 20,
              }}
              value={response}
            />
          </ScrollView>
        </TextinputWrapper>
      )
    }

    sendCard() {
      const { stack_cards, currentIndex } = this.state
      analytics.event(new Event('Interaction', 'Send Card', stack_cards[currentIndex]))
        .then(() => console.log('success'))
        .catch(e => console.log(e.message))
      Communications.text('', "Thought you'd love this card: https://itunes.apple.com/us/app/atlas-edu/id1423178843?mt=8")
    }

    renderSaveModal = () => {
      const {
        stack_cards, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      const color2 = (stack && stack.color2) ? stack.color2 : '#F6B05C'
      return (
        <Modal
          style={{ flex: 1 }}
          animationType="fade"
          visible={this.state.modalVisible}
        >
          <FadeWrapper
            pose={!this.state.modalVisible ? 'state1' : 'state0'}
            style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}
          >
            <View style={{ padding: 30, justifyContent: 'center' }}>
              <Text style={{
                color: 'white', textAlign: 'center', fontFamily: 'gt-walsheim-regular', fontSize: 28,
              }}
              >
Card Saved!
              </Text>
              {this.renderCardModal(stack_cards[currentIndex])}
              <View style={{
                top: 25, flexDirection: 'row', width: '100%', justifyContent: 'center',
              }}
              >
                <TouchableOpacity style={[Styles.doneButton, { backgroundColor: 'white' }]} onPress={() => this.sendCard()}>
                  <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Send</Text>
                </TouchableOpacity>
              </View>
              <View style={{
                top: 35, flexDirection: 'row', width: '100%', justifyContent: 'center',
              }}
              >
                <TouchableOpacity
                  style={[Styles.doneButton, { backgroundColor: color1 }]}
                  onPress={() => {
                    this._carousel.snapToNext()
                    this.setState({ modalVisible: false })
                    this.setModalVisible(!this.state.modalVisible)
                  }}
                >
                  <Text style={[Styles.buttonText, { color: 'white', letterSpacing: 0 }]}>Done</Text>
                </TouchableOpacity>
              </View>
            </View>
          </FadeWrapper>
        </Modal>
      )
    }

    prepareNotifications(token) {
      return { to: token, body: 'Anonymous suggested a card for you! Try it out?' }
    }

    submitComment = () => {
      // this is the comment
      const { members, stack_cards, currentIndex } = this.state
      const currentCard = stack_cards[currentIndex]
      const { post } = this.props.navigation.state.params
      const checkInUser = find(members, { id: post.userId })
      const notification = checkInUser ? this.prepareNotifications(checkInUser.token) : false
      try {
        postComment(post.id, { cardId: currentCard.id, createdAt: Date.now(), userId: this.props.user.id }, notification)
        this.props.navigation.goBack()
      } catch (e) {
        Alert.alert('Error', e.message)
      }
    }

    renderCommentButton = () => {
      const {
        stack_cards, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      return (
        <TouchableOpacity style={[Styles.doneButton]} onPress={() => this.submitComment()}>
          <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Comment</Text>
        </TouchableOpacity>
      )
    }

    renderSaveButton = () => {
      const {
        stack_cards, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      return (
        <TouchableOpacity
          style={Styles.doneButton}
          onPress={() => {
            this._submit(stack_cards[currentIndex])
            this.setState({ modalVisible: true })
          }}
        >
          <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Save</Text>
        </TouchableOpacity>
      )
    }

    renderAnswerButton = () => {
      const {
        stack_cards, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      return (
        <TouchableOpacity
          style={Styles.doneButton}
          onPress={() => { this.setState({ displayText: !this.state.displayText, response: '' }) }}
        >
          <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Answer</Text>
        </TouchableOpacity>
      )
    }

    renderButton = (card) => {
      const { stack, isComment, cardId } = this.props.navigation.state.params
      if (isComment) {
        return this.renderCommentButton()
      } if (card && card.type != null && card.type == 'form') {
        return this.renderAnswerButton()
      }
      return this.renderSaveButton()
    }

    renderModal = () => {
      const {
        stack_cards, modalVisible, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack, isComment, cardId } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      const color2 = (stack && stack.color2) ? stack.color2 : '#F6B05C'
      let index = 0
      if (cardId && stack_cards) {
        index = stack_cards.findIndex(x => x && x.id == cardId)
      }
      return (
        <Modal
          style={{ flex: 1 }}
          animationType="fade"
          visible={modalVisible}
        >
          <View
            style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}
          >
            <View style={{ width: '100%', padding: 20, justifyContent: 'center' }}>
              <Text style={{
                color: 'white', textAlign: 'center', fontFamily: 'gt-walsheim-regular', fontSize: 24, bottom: 25,
              }}
              >
Card Saved!
              </Text>
              <View style={{ justifyContent: 'center' }}>
                {<CardPreviewModal navigation={this.props.navigation} response={response} card={stack_cards[currentIndex]} stack={stack} />}
              </View>
              <View style={{
                top: 25, flexDirection: 'row', width: '100%', justifyContent: 'center',
              }}
              >
                <TouchableOpacity style={[Styles.doneButton, { backgroundColor: 'white' }]} onPress={() => Communications.text('', "Thought you'd love this card: https://itunes.apple.com/us/app/atlas-edu/id1423178843?mt=8")}>
                  <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Send</Text>
                </TouchableOpacity>
              </View>
              <View style={{
                top: 35, flexDirection: 'row', width: '100%', justifyContent: 'center',
              }}
              >
                <TouchableOpacity
                  style={[Styles.doneButton, { backgroundColor: color1 }]}
                  onPress={() => {
                    this._carousel.snapToNext()
                    this.setState({ modalVisible: false })
                    this.setModalVisible(!this.state.modalVisible)
                  }}
                >
                  <Text style={[Styles.buttonText, { color: 'white', letterSpacing: 0 }]}>Done</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      )
    }

    render() {
      const {
        stack_cards, modalVisible, currentIndex, longText, response, displayText, currentCard,
      } = this.state
      const { stack, isComment, cardId } = this.props.navigation.state.params
      const color1 = (stack && stack.color1) ? stack.color1 : '#F6B05C'
      const color2 = (stack && stack.color2) ? stack.color2 : '#F6B05C'
      let index = 0
      if (cardId && stack_cards) {
        index = stack_cards.findIndex(x => x && x.id == cardId)
      }
      return (
        <View style={Styles.container}>
          {this.props.loading && <ActivityIndicator size="large" />}
          <LinearGradient
            colors={[color1, color2]}
            locations={[0, 1]}
            start={[0.55, 0.3]}
            end={[0.7, 1]}
            style={{
              position: 'absolute',
              zIndex: -1000,
              width: '100%',
              height: '100%',
            }}
          />
          {this.renderModal()}
          {this.renderTextInput()}
          <View style={{
            flexDirection: 'row', justifyContent: 'flex-start', marginBottom: windowHeight * 0.07, marginTop: '15%', width: '100%', paddingLeft: 32,
          }}
          >
            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
              <Image source={require('../../assets/images/arrow-back-white.png')} style={{ width: 31, height: 21 }} />
            </TouchableOpacity>
          </View>
          <Carousel
            ref={(c) => {
              this._carousel = c
            }}
            onSnapToItem={this.changeIndex}
            inactiveSlideOpacity={0.3}
            layout="tinder"
            loop
            firstItem={index}
            data={stack_cards}
            removeClippedSubviews={false}
            renderItem={this.renderCard}
            sliderWidth={windowWidth}
            itemWidth={windowWidth * 0.92}
          />
          <View style={{
            top: -windowHeight * 0.06, padding: 22, flexDirection: 'row', justifyContent: 'space-between', width: '100%',
          }}
          >
            <TouchableOpacity
              style={Styles.doneButton}
              onPress={() => { this._carousel.snapToNext() }}
            >
              <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Next Card</Text>
            </TouchableOpacity>
            {this.renderButton(stack_cards[currentIndex])}
          </View>
        </View>
      )
    }
}


const mapStateToProps = state => ({
  user: state.auth.user,
  stack_cards: state.checkIn.stack_cards,
  loading: state.checkIn.loading,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenCardsFromStack,
  stopListenStack,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardStackView)
