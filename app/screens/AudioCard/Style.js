const Styles = {
  cardTitle: {
    fontFamily: 'gt-walsheim-bold',
    color: 'white',
    fontSize: 20,
    textAlign: 'left',
    letterSpacing: 0.3,
    marginBottom: 12,
  },

  question: {
    fontFamily: 'gt-walsheim-medium',
    top: 20,
    textAlign: 'center',
    lineHeight: 27,
    fontSize: 18,
    color: 'white',
  },
}

export default Styles
