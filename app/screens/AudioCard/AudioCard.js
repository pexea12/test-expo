import React from 'react'
import {
  Text,
  Dimensions,
  View,
  Slider,
  TouchableOpacity,
  Animated,
  Alert,
  ActivityIndicator,
  Image,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  FileSystem,
  LinearGradient,
  Audio,
  Permissions,
  KeepAwake,
} from 'expo'
import { Analytics, Event } from 'expo-analytics'
import posed from 'react-native-pose'
import Pulse from 'react-native-pulse'
import RecordingConfig from '../../configs/Recording'
import Feedback from '../Feedback/Feedback'
import Styles from './Style'
import {
  listenAudioCards,
  listenMyRecordings,
  updateMyRecording,
  updatePendingRecordings,
} from '../../redux/checkIn/actions'
import StarBackground from '../../components/UI/StarBackground'
import { segmentAnalyzePage, segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const analytics = new Analytics('UA-131293770-1')

const quickTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 300,
})

const tween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 600,
  delay: 250,
})

const slowerTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 600,
  delay: 800,
})

const slowerTween2 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 600,
  delay: 400,
})

const config = {
  enter: {
    opacity: 1,
    transition: tween,
  },
  exit: {
    opacity: 0.2,
    transition: tween,
  },
}

const nextConfig = {
  enter: {
    opacity: 1,
    scale: 1.15,
    transition: tween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.0,
    transition: tween,
  },
}

const delayedConfig = {
  enter: {
    opacity: 1,
    scale: 1.15,
    transition: slowerTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.0,
    transition: slowerTween,
  },
}

const delayedConfig2 = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: tween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.2,
    transition: tween,
  },
}

const delayedConfig3 = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: slowerTween2,
  },
  exit: {
    scale: 1.0,
    opacity: 0.6,
    transition: slowerTween2,
  },
}

const quickConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: quickTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.3,
    transition: quickTween,
  },
}

const recordConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: tween,
  },
  exit: {
    scale: 1.0,
    opacity: 0.0,
    transition: tween,
  },
}

const FadeOut = posed.View(config)
const NextFadeOut = posed.View(nextConfig)
const QuickFadeOut = posed.View(quickConfig)
const RecordFadeOut = posed.View(recordConfig)
const DelayedFadeOut = posed.View(delayedConfig)

class CardPreview extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.isSeeking = false
    this.shouldPlayAtEndOfSeek = false
    this.state = {
      loadingAudio: true,
      index: 0,
      latestIndex: 0,
      playing: false,
      atEnd: false,
      currentPage: 'player',
      mode: 'playing',
      card: props.navigation.state.params.card,
      percent: 0,
      visited: [],
      shouldPlay: false,
      recordTransition: false,
      enableNext: false,
    }
  }

  componentDidMount = async () => {
    const { card } = this.props.navigation.state.params
    await this.resetCard()
    await this.loadAudio()
    analytics
      .event(new Event('AudioView', this.state.card ? this.state.card.title : null))
      .then(() => console.log('success'))
      .catch(e => console.log(e.message))
  };

  componentWillUnmount() {
    this.unloadPlayer()
    this.unloadRecorder()
  }

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000)
    const seconds = ((millis % 60000) / 1000).toFixed(0)
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  resetCard = async () => {
    const { card } = this.props.navigation.state.params
    const newCard = card
    for (let i = 0; i < card.audios.length; i++) {
      if (card.audios[i].recording) {
        delete newCard.audios[i].recording
      }
    }
    this.setState({ card: newCard })
  }

  loadAudio = async () => {
    const { card } = this.props.navigation.state.params
    const { index, mode, enableNext } = this.state
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })

    const audio = new Audio.Sound()
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)

    this.setState({ loadingAudio: true }, async () => {
      if (card.isDaily) {
        segmentAnalyzePage(this.props.user, 'Daily Listen', { card: card.title })
      } else {
        segmentAnalyzePage(this.props.user, `Ep Listen ${index + 1}`, { card: card.title })
      }

      if (status !== 'granted') {
        alert('Hey! You have not enabled selected permissions')
        this.props.navigation.goBack()
      }

      try {
        // //if downloaded
        await audio.loadAsync({ uri: card.audios[index].libraryDownloadedUri || card.audios[index].url })
        audio.setOnPlaybackStatusUpdate((status) => {
          const totalDuration = this.millisToMinutesAndSeconds(status.durationMillis)
          const currentPosition = this.millisToMinutesAndSeconds(
            status.durationMillis - status.positionMillis,
          )
          const soundDuration = status.durationMillis
          const percent = Math.round((status.positionMillis / status.durationMillis) * 100)
          if (!enableNext && percent == 100) {
            this.setState({ enableNext: true })
          }
          this.setState({
            ...status, audio, soundDuration, loadingAudio: !status.durationMillis, currentPosition, totalDuration, percent,
          })
        })
        audio.setProgressUpdateIntervalAsync(1000)
      } catch (error) {
        // An error occurred!
        console.log(error)
        alert("We're unable to load this audio right now. Check your internet connection.")
      }
    })
  }

  play = async () => {
    const { audio, percent } = this.state
    if (percent == 100) {
      this.onSeekSliderSlidingComplete(0)
    }
    audio.playAsync()
  }

  pause = async () => {
    const { audio } = this.state
    audio.pauseAsync()
  }

  async unloadPlayer() {
    const { audio } = this.state
    await audio.unloadAsync()
  }

  unloadRecorder = async () => {
    const { recording } = this.state
    await recording.stopAndUnloadAsync()
  };

  getNextPage = () => {
    const { index, currentPage } = this.state
    if (currentPage == 'recorder') {
      return (`player${index + 1}`)
    }
    return (`recorder${index}`)
  }

  indexUpdate = (pageArg) => {
    const { index } = this.state
    const newVisited = []
    if (pageArg === 'recorder') {
      newVisited.push(`player${index}`)
    } else {
      newVisited.push(`recorder${index}`)
    }
    this.setState({ visited: this.state.visited.concat(newVisited) })
  };

  prevCompleted = (navIndex, navPage) => {
    const { visited } = this.state
    let multi = 1
    for (let i = 0; i < navIndex; i++) {
      multi *= visited.indexOf(`player${i}`) > -1
      multi *= visited.indexOf(`recorder${i}`) > -1
    }
    if (navPage == 'recorder') {
      multi *= visited.indexOf(`recorder${navIndex + 1}`) > -1
    }
    return multi
  };

  // Navigate from Record To Audio
  recordToAudio = async (navIndex) => {
    const { audio } = this.state
    const { card } = this.props.navigation.state.params
    const newIndex = navIndex || this.state.index + 1

    if (card.audios[newIndex]) {
      await audio.unloadAsync()
      this.indexUpdate('player')
      this.setState(
        {
          index: newIndex, currentPage: 'player', loadingAudio: true, mode: 'playing',
        },
        this.loadAudio,
      )
    } else {
      if (card.isDaily) {
        this.props.navigation.navigate('Recap', {
          card,
          textFeedback: '',
          meaningfulValue: 0,
        })
      } else {
        this.props.navigation.navigate('Recap', {
          card,
          textFeedback: '',
          meaningfulValue: 0,
          isEpisode: true,
          index: 0,
        })
      }
    }
  };

  // Navigating from the menu to Audio
  navToAudio = async (navIndex) => {
    const { card } = this.props.navigation.state.params
    const { audio, visited, currentPage } = this.state
    const newCard = { ...card }

    // If you've visited it already, or if you've completed all previous pages
    if (visited.indexOf(`player${navIndex}`) > -1 || this.prevCompleted(navIndex, 'player')) {
      if (currentPage == 'player') {
        await this.unloadPlayer()
      }
      await audio.unloadAsync()
      this.setState(
        {
          card: newCard,
          recordTransition: false,
          currentPage: 'player',
          loadingAudio: true,
          index: navIndex,
          mode: 'playing',
          percent: 0,
          enableNext: true,
        },
        this.loadAudio,
      )
    } else {
      Alert.alert("You'll need to complete previous parts first")
    }
  };

  // Navigating from the menu to Recorder
  navToRecord = (navIndex) => {
    const { currentPage, visited, enableNext } = this.state
    if (
      visited.indexOf(`recorder${navIndex}`) > -1
      || this.prevCompleted(navIndex, 'recorder')
      || (currentPage == 'player' && enableNext)
    ) {
      // If currently at a recorder, then we need to unload current audio first.
      // if (currentPage == "recorder") {
      this.setState({ index: navIndex, percent: 100, mode: 'recording' }, async () => {
        this.goToRecord()
      })
    } else {
      Alert.alert("You'll need to complete previous parts first")
    }
  };

  backNavigation = () => {
    const {
      index, currentPage, isRecording, isDoneRecording,
    } = this.state
    if (index == 0 && currentPage == 'player') {
      Alert.alert(
        'Exit Episode?',
        'Current recordings will not be saved if you exit before completing',
        [
          { text: 'Cancel', onPress: () => console.warn('No Pressed'), style: 'cancel' },
          { text: 'Confirm', onPress: () => this.props.navigation.goBack() },
        ],
      )
    } else if (currentPage == 'player') {
      // go back to recording
      this.navToRecord(index - 1)
    } else {
      this.setState({ percent: 0, loadingAudio: true, enableNext: true }, async () => {
        this.unloadRecorder()
        this.navToAudio(index)
      })
    }
  };

  forwardNavigation = (enabled) => {
    const {
      isDoneRecording,
      currentPage,
      isRecording,
      card,
      enableNext,
      visited,
      index,
    } = this.state

    if (enabled) {
      if (currentPage == 'player') {
        this.navToRecord(index)
      } else {
        this.unloadRecorder()
        this.navToAudio(index + 1)
      }
    } else {
      Alert.alert("You'll need to complete this part and previous parts before moving on")
    }
  };

  goToRecord = async () => {
    const { card, index } = this.state
    // ESSENTIALLY, THERE'S A STATE CHANGE WE NEED TO PERFOM FIRST BEFORE WE DO THE REST.
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })
    this.indexUpdate('recorder')
    const recording = new Audio.Recording()

    // console.log("NEW INDEX: ", newIndex)

    this.setState({
      currentPage: 'recorder',
      mode: 'recording',
    })
    this.unloadPlayer()

    if (card.isDaily) {
      segmentAnalyzePage(this.props.user, 'Daily Record', { card: card.title })
    } else {
      segmentAnalyzePage(this.props.user, `Ep Record ${index + 1}`, { card: card.title })
    }

    await recording.prepareToRecordAsync(RecordingConfig)
    recording.setOnRecordingStatusUpdate((status) => {
      this.setState({ ...status, recordTransition: true, recording })
    })
    recording.setProgressUpdateInterval(5000)
    this.setState({ enableNext: false, loadingAudio: false, recording })
  }

  recordHelper = async () => {
    const { recording, card, index } = this.state
    this.setState({ displayRecording: !this.state.displayRecording })
    recording.startAsync()
  }

  record = async () => {
    const { recording, card, index } = this.state
    if (card.audios[index].recording) {
      Alert.alert('New Recording? 🎙', 'Your new recording will replace your previous one', [
        { text: 'Cancel', onPress: () => console.warn('No Pressed'), style: 'cancel' },
        { text: 'Confirm', onPress: () => this.recordHelper() },
      ])
    } else {
      this.recordHelper()
    }
  }

  stopRecording = async (newIndex) => {
    const { recording, index } = this.state
    const { card } = this.props.navigation.state.params
    const newCard = { ...card }

    this.setState({
      recordTransition: false,
      displayRecording: !this.state.displayRecording,
    })

    await recording.stopAndUnloadAsync()
    await recording.createNewLoadedSoundAsync()
    newCard.audios[index].recording = recording.getURI()

    this.setState({
      card: newCard, isDoneRecording: false, Audio: true, totalDuration: '',
    }, () => {
      this.recordToAudio()
    })
  };

  onSeekSliderValueChange = (value) => {
    const { audio } = this.state
    if (audio != null && !this.isSeeking) {
      this.isSeeking = true
      this.shouldPlayAtEndOfSeek = this.state.shouldPlay
      audio.pauseAsync()
    }
  };

  onSeekSliderSlidingComplete = async (value) => {
    const { audio } = this.state
    if (audio != null) {
      this.isSeeking = false
      const seekPosition = (value / 100) * this.state.soundDuration
      if (this.shouldPlayAtEndOfSeek) {
        audio.playFromPositionAsync(seekPosition)
      } else {
        audio.setPositionAsync(seekPosition)
      }
    }
  };

  renderNextButtonHelper = (forwardEnabled) => {
    const {
      percent, enableNext, navIndex, index, latestIndex,
    } = this.state

    let heightMargin = this.getHeightMargin()

    if (windowHeight >= 800) {
      heightMargin *= 0.5
    } else if (windowHeight < 600) {
      heightMargin *= 0.35
    } else {
      heightMargin *= 0.5
    }

    return (
      <NextFadeOut
        pose={percent == 100 || enableNext || forwardEnabled ? 'enter' : 'exit'}
        style={{ top: heightMargin * 0.83, flexDirection: 'row', justifyContent: 'center' }}
      >
        {this.renderNextButton()}
      </NextFadeOut>
    )
  };

  renderPlayer = (forwardEnabled) => {
    const {
      loadingAudio, card, percent, soundDuration, index,
    } = this.state
    const cardTitle = card.isDaily ? card.title : `PART ${index + 1}`
    const currentAudioPosition = this.millisToMinutesAndSeconds((percent / 100) * soundDuration)

    return (
      <View
        style={{
          flex: 1,
          bottom: -55,
          justifyContent: 'center',
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
          {loadingAudio && <ActivityIndicator size="large" />}
          <FadeOut
            pose={
              loadingAudio || percent == 100 || currentAudioPosition !== currentAudioPosition
                ? 'exit'
                : 'enter'
            }
            style={{ flex: 8, width: '100%', justifyContent: 'space-between' }}
          >
            <View
              style={{
                top: -20,
                flex: 1,
                justifyContent: 'center',
                marginLeft: 20,
                marginRight: 20,
              }}
            >
              <Text
                style={[
                  Styles.cardTitle,
                  {
                    textAlign: 'center',
                    fontSize: 17,
                    textTransform: 'uppercase',
                    letterSpacing: 3.33,
                  },
                ]}
              >
                {cardTitle}
              </Text>

              {card.audioSubheaders && (
                <Text
                  style={[
                    Styles.cardTitle,
                    {
                      textAlign: 'center',
                      opacity: 0.75,
                      fontFamily: 'gt-walsheim-regular',
                      fontSize: 15,
                    },
                  ]}
                >
                  {card.audioSubheaders[index]}
                </Text>
              )}
            </View>
            <View style={{ flex: 0.3, alignItems: 'center' }}>
              <Slider
                style={{ borderRadius: 5, height: 15, width: windowWidth * 0.75 }}
                minimumTrackTintColor="white"
                trackStyle={{ height: 10, backgroundColor: 'white' }}
                thumbStyle={{ width: 14, height: 14, borderRadius: 14 }}
                minimumValue={0}
                maximumValue={100}
                value={percent}
                thumbImage={require('../../assets/images/sliderThumbTrack.png')}
                onValueChange={this.onSeekSliderValueChange}
                onSlidingComplete={this.onSeekSliderSlidingComplete}
                disabled={this.state.loadingAudio}
              />
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  top: 5,
                  height: 50,
                  width: windowWidth * 0.75,
                }}
              >
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                  <Text
                    style={{
                      fontFamily: 'gt-walsheim-regular',
                      color: 'white',
                      fontSize: 13,
                      textAlign: 'left',
                    }}
                  >
                    {!loadingAudio
                      ? this.millisToMinutesAndSeconds((percent / 100) * soundDuration)
                      : ''}
                  </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                  <Text
                    style={{
                      fontFamily: 'gt-walsheim-regular',
                      color: 'white',
                      fontSize: 13,
                      textAlign: 'right',
                    }}
                  >
                    -
                    {!loadingAudio
                      ? this.millisToMinutesAndSeconds(((100 - percent) / 100) * soundDuration)
                      : ''}
                  </Text>
                </View>
              </View>
            </View>
          </FadeOut>
          <View style={{ justifyContent: 'center', flex: 3 }}>
            <View style={{ justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
              {this.renderBottomButtons()}
            </View>
          </View>
          {!loadingAudio && this.renderNextButtonHelper(forwardEnabled)}
        </View>
      </View>
    )
  };

  renderBottomButtons = () => {
    const {
      loadingAudio,
      percent,
      isPlaying,
    } = this.state

    const opacityValue = 0.15 + 0.8 * (percent < 100)

    if (isPlaying) {
      return (
        <FadeOut pose={(loadingAudio || (percent == 100)) ? 'exit' : 'enter'}>
          <TouchableOpacity
            style={{ opacity: opacityValue }}
            disabled={percent == 100 || loadingAudio}
            onPress={isPlaying ? this.pause : this.play}
          >
            <View style={{
              width: 80,
              height: 80,
              borderRadius: 25,
              backgroundColor: 'rgba(54, 54, 54, 0.5)',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: '#d8d8d8',
            }}
            >
              <Image
                style={{ width: 35, height: 35 }}
                source={require('../../assets/images/pause-button.png')}
              />
            </View>
          </TouchableOpacity>
        </FadeOut>
      )
    } if (percent === 100) {
      return (
        <TouchableOpacity
          style={{ opacity: 1.0 }}
          onPress={isPlaying ? this.pause : this.play}
        >
          <View style={{
            width: 80,
            height: 80,
            borderRadius: 25,
            backgroundColor: 'rgba(54, 54, 54, 0.5)',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#d8d8d8',
          }}
          >
            <Image
              style={{ width: 35, height: 35 }}
              source={require('../../assets/images/replayButton.png')}
            />
          </View>
        </TouchableOpacity>
      )
    }

    return (
      <FadeOut pose={(loadingAudio || (percent == 100)) ? 'exit' : 'enter'}>
        <TouchableOpacity
          style={{ opacity: opacityValue }}
          disabled={percent == 100 || loadingAudio}
          onPress={isPlaying ? this.pause : this.play}
        >
          <View style={{
            width: 80,
            height: 80,
            borderRadius: 25,
            backgroundColor: 'rgba(54, 54, 54, 0.5)',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#d8d8d8',
          }}
          >
            <Image
              style={{ width: 35, height: 35, left: 4 }}
              source={require('../../assets/images/play-button.png')}
            />
          </View>
        </TouchableOpacity>
      </FadeOut>
    )
  }

  loadRecordingAudio = async () => {
    this.loadAudio
  };

  renderExistingRecording = () => {
    const {
      card,
      percent,
      isPlaying,
      index,
      loadingAudio,
      currentPage,
      isRecording,
      isDoneRecording,
      recordTransition,
      displayRecording,
    } = this.state

    this.loadRecordingAudio()

    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Text
          style={{
            color: 'white',
            fontFamily: 'gt-walsheim-medium',
            fontSize: 16,
            textAlign: 'center',
            opacity: 0.75,
          }}
        >
          You've answered this question
        </Text>
      </View>
    )
  };

  renderRecorder = () => {
    const {
      card,
      index,
      loadingAudio,
      percent,
      currentPage,
      isRecording,
      isDoneRecording,
      recordTransition,
      displayRecording,
    } = this.state

    return (
      <QuickFadeOut pose={recordTransition ? 'enter' : 'exit'}>
        <LinearGradient
          style={{
            height: windowHeight * 1.08,
            width: windowWidth,
            padding: 25,
            top: -windowHeight * 0.03,
          }}
          colors={['rgba(0,0,0,0)', 'black']}
        >
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={Styles.question}>
                {card.questions[index]}
              </Text>
              {card.audios[index].recording && this.renderExistingRecording()}
            </View>
            <View
              style={{
                top: -30,
                flex: 0.8,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              {displayRecording && (
                <View style={{ bottom: -windowWidth * 0.16 }}>
                  <Pulse
                    color="rgba(182, 233, 255, 0.13)"
                    numPulses={5}
                    diameter={400}
                    speed={15}
                    duration={800}
                  />
                </View>
              )}
              {this.renderRecordButtons()}
            </View>
          </View>
        </LinearGradient>
      </QuickFadeOut>
    )
  };

  returnStep = (currIndex) => {
    const { index, currentPage, visited } = this.state
    if (index == currIndex && currentPage == 'player') {
      return (
        <View>
          <TouchableOpacity
            style={{
              justifyContent: 'space-between',
              flexDirection: 'column',
            }}
          >
            <View
              style={{
                color: 'rgba{74, 74, 74, 0.56)',
                borderColor: 'white',
                borderWidth: 1,
                width: 24,
                height: 24,
                borderRadius: 24,
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color: 'white',
                  fontFamily: 'gt-walsheim-bold',
                  textAlign: 'center',
                }}
              >
                {currIndex + 1}
              </Text>
            </View>
          </TouchableOpacity>
          {visited.indexOf(`player${currIndex}`) > -1 && (
            <Image
              style={{
                width: 11.24,
                height: 8,
                top: 8,
                right: -5,
              }}
              source={require('../../assets/images/checkMark.png')}
            />
          )}
        </View>
      )
    }
    return (
      <TouchableOpacity
        onPress={() => {
          this.navToAudio(currIndex)
        }}
      >
        <View
          style={{
            backgroundColor: 'rgba{216, 216, 216, 0.3)',
            borderColor: 'white',
            borderWidth: 1,
            opacity: 0.4,
            width: 24,
            height: 24,
            borderRadius: 24,
            justifyContent: 'center',
          }}
        >
          <Text
            style={{
              fontSize: 14,
              color: 'white',
              fontFamily: 'gt-walsheim-bold',
              textAlign: 'center',
            }}
          >
            {currIndex + 1}
          </Text>
        </View>
        {visited.indexOf(`player${currIndex}`) > -1 && (
        <Image
          style={{
            width: 11.24,
            height: 8,
            opacity: 0.4,
            top: 8,
            right: -5,
          }}
          source={require('../../assets/images/checkMark.png')}
        />
        )}
      </TouchableOpacity>
    )
  };

  returnVoiceStep = (currIndex) => {
    const { index, currentPage, visited } = this.state
    if (index == currIndex && currentPage == 'recorder') {
      return (
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity>
              <View
                style={{
                  color: 'rgba{74, 74, 74, 0.56)',
                  shadowColor: 'white',
                  shadowOpacity: 0.65,
                  shadowRadius: 1,
                  borderColor: 'white',
                  borderWidth: 1,
                  bottom: -5,
                  width: 15,
                  height: 15,
                  borderRadius: 15,
                  justifyContent: 'center',
                }}
              />
            </TouchableOpacity>
          </View>
          {visited.indexOf(`recorder${currIndex}`) > -1 && (
            <View
              style={{
                top: 17,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <Image
                style={{ width: 11.24, height: 8, opacity: 1.0 }}
                source={require('../../assets/images/checkMark.png')}
              />
            </View>
          )}
        </View>
      )
    }
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => {
              this.navToRecord(currIndex)
            }}
          >
            <View
              style={{
                borderColor: 'white',
                borderWidth: 1,
                opacity: 0.4,
                bottom: -5,
                width: 15,
                height: 15,
                borderRadius: 15,
                justifyContent: 'center',
              }}
            />
          </TouchableOpacity>
        </View>
        {visited.indexOf(`recorder${currIndex}`) > -1 && (
        <View
          style={{
            top: 17,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Image
            style={{ width: 11.24, height: 8, opacity: 0.4 }}
            source={require('../../assets/images/checkMark.png')}
          />
        </View>
        )}
      </View>
    )
  };

  renderSteps() {
    return (
      <View
        style={{
          flexDirection: 'row',
          flex: 0.78,
          height: 25,
          right: -10,
          justifyContent: 'space-between',
        }}
      >
        {this.returnStep(0)}
        {this.returnVoiceStep(0)}
        {this.returnStep(1)}
        {this.returnVoiceStep(1)}
        {this.returnStep(2)}
        {this.returnVoiceStep(2)}
      </View>
    )
  }

  renderRecordButtonText = (isRecording) => {
    if (isRecording) {
      return (
        <Text
          style={[
            Styles.cardTitle,
            {
              width: 100,
              opacity: 0.7,
              fontFamily: 'gt-walsheim-medium',
              top: 10,
              textTransform: 'uppercase',
              letterSpacing: 5,
              fontSize: 14,
              textAlign: 'center',
            },
          ]}
        >
          Finish
        </Text>
      )
    }
    return (
      <Text
        style={[
          Styles.cardTitle,
          {
            width: 100,
            opacity: 0.7,
            fontFamily: 'gt-walsheim-medium',
            top: 10,
            textTransform: 'uppercase',
            letterSpacing: 5,
            fontSize: 14,
            textAlign: 'center',
          },
        ]}
      >
        Record
      </Text>
    )
  };

  renderRecordButtons = () => {
    const { isRecording, isDoneRecording, isLoading } = this.state
    return (
      <TouchableOpacity onPress={isRecording ? this.stopRecording : this.record}>
        <View
          style={{
            top: -20,
            width: 150,
            height: 100,
            alignItems: 'center',
            justifyContent: 'space-around',
          }}
        >
          {!isLoading ? (
            isRecording && !isDoneRecording ? (
              <View style={{ alignItems: 'center', justifyContent: 'space-around' }}>
                {this.renderRecordButtonText(isRecording)}
                <Image
                  style={{ top: 20, width: 80, height: 80 }}
                  source={require('../../assets/images/stopPulse.png')}
                />
              </View>
            ) : (
              <View style={{ alignItems: 'center', justifyContent: 'space-around' }}>
                {this.renderRecordButtonText(isRecording)}
                <Image
                  style={{ top: 20, width: 80, height: 80 }}
                  source={require('../../assets/images/recordPulse.png')}
                />
              </View>
            )
          ) : (
            <ActivityIndicator size="large" />
          )}
        </View>
      </TouchableOpacity>
    )
  };

  renderNextButton = () => {
    const { enableNext } = this.state
    return (
      <View>
        <TouchableOpacity disabled={!enableNext} onPress={() => this.forwardNavigation(true)}>
          <View
            style={{
              width: 125,
              height: 40,
              borderRadius: 20,
              shadowColor: 'white',
              shadowOpacity: 0.4,
              shadowRadius: 5,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={[
                Styles.cardTitle,
                {
                  color: '#a660b4',
                  fontSize: 13,
                  marginBottom: 0,
                },
              ]}
            >
              Next
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  };

  getNextPageString = () => {
    const { currentPage, index } = this.state
    if (currentPage == 'recorder') {
      return (`player${index + 1}`)
    }
    return (`recorder${index}`)
  }

  getHeightMargin = () => {
    if (windowHeight >= 800) return -windowHeight
    if (windowHeight < 600) return -windowHeight * 1.6
    return -windowHeight * 1.2
  }

  render() {
    const { card } = this.props.navigation.state.params
    const {
      index,
      mode,
      currentPage,
      enableNext,
      visited,
      isRecording,
      isDoneRecording,
    } = this.state
    const nextPage = this.getNextPageString()

    let forwardEnabled = enableNext
      || (currentPage == 'recorder' && this.prevCompleted(index + 1, 'player'))
      || (currentPage == 'player' && this.prevCompleted(index, 'recorder'))
      || visited.indexOf(nextPage) > -1
      || visited.indexOf(currentPage + index) > -1
    if (currentPage == 'recorder' && !card.audios[index].recording) {
      forwardEnabled = false
    }
    const backwardEnabled = !(isRecording && !isDoneRecording)
    let forwardNavOpacity
    let backNavOpacity = 1.0

    if (!forwardEnabled) {
      forwardNavOpacity = 0.3
    }
    if (!backwardEnabled) {
      backNavOpacity = 0.3
    }

    const heightMargin = this.getHeightMargin()

    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <KeepAwake />
        <LinearGradient
          style={{
            height: windowHeight * 1.08,
            padding: 25,
            top: -windowHeight * 0.03,
          }}
          colors={['#494E94', '#696DC4', '#8385EB', '#8DD8F8']}
        >
          <Image
            resizeMode="contain"
            source={require('../../assets/images/audioBG2.png')}
            style={{
              position: 'absolute',
              top: heightMargin * 0.5,
              width: windowWidth * 1.08,
              opacity: 0.32,
            }}
          />
          <QuickFadeOut
            pose={
              !(currentPage == 'recorder' && (isRecording && !isDoneRecording)) ? 'enter' : 'exit'
            }
            style={{ flex: 0.20, justifyContent: 'center', top: 0.04 * windowHeight }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                bottom: 10,
                zIndex: 1000,
              }}
            >
              <TouchableOpacity
                disabled={!backwardEnabled}
                style={{ opacity: backNavOpacity }}
                onPress={() => {
                  this.backNavigation()
                }}
              >
                <Image
                  source={require('../../assets/images/back-angle.png')}
                  style={{ left: -4, width: 21, height: 21 }}
                />
              </TouchableOpacity>
              {card.audios.length > 1 && this.renderSteps()}
              <TouchableOpacity
                disabled={!forwardEnabled}
                style={{ opacity: forwardNavOpacity }}
                onPress={() => {
                  this.forwardNavigation(forwardEnabled)
                }}
              >
                <Image
                  source={require('../../assets/images/forward-angle.png')}
                  style={{ width: 21, height: 21 }}
                />
              </TouchableOpacity>
            </View>
          </QuickFadeOut>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            {mode === 'playing' ? this.renderPlayer(forwardEnabled) : this.renderRecorder()}
          </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

export default connect(
  mapStateToProps,
)(CardPreview)
