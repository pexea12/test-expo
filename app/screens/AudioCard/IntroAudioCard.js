import React from 'react'
import {
  ScrollView,
  FlatList,
  Text,
  Dimensions,
  View,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
  LinearGradient, Audio, Permissions, KeepAwake,
} from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import posed from 'react-native-pose'
import Styles from './Style'
import Header from '../../components/Header/Header'
import moment from '../../configs/Moment'
import { listenMembers } from '../../redux/checkIn/actions'
import { postComment, getMembers } from '../../configs/Firebase'
import CardPreviewModal from '../CardPreviewModal/CardPreviewModal'
import RecordingConfig from '../../configs/Recording'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const analytics = new Analytics('UA-131293770-1')


class CardPreview extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)
    this.state = {
      loadingAudio: false,
      index: 0,
      playing: false,
      mode: 'playing',
      card:
        'https://firebasestorage.googleapis.com/v0/b/atlasaudio-2e675.appspot.com/o/scripts%2F0.%20Intro%2FIntro%20V2%20Fully%20Mastered.mp3?alt=media&token=5fb3f1db-4282-4300-b7dd-90fd74b9f3c2',
      percent: 0,
    }

    this.loadAudio = this.loadAudio.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this.next = this.next.bind(this)
    this.goToRecord = this.goToRecord.bind(this)
    this.record = this.record.bind(this)
    this.stopRecording = this.stopRecording.bind(this)
  }

  async componentDidMount() {
    this.loadAudio()
    analytics.hit(new PageHit('IntroAudio'))
      .then(() => console.log('success'))
      .catch(e => console.log(e.message))
  }

  componentWillUnmount() {
    this.unload()
  }

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000)
    const seconds = ((millis % 60000) / 1000).toFixed(0)
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  async loadAudio() {
    const {
      card, index, mode, enableNext,
    } = this.state
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })
    const audio = new Audio.Sound()
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)

    this.setState({ loadingAudio: true }, async () => {
      if (status !== 'granted') {
        alert('Hey! You have not enabled selected permissions')
        this.props.navigation.goBack()
      }
      try {
        await audio.loadAsync({ uri: card })
        audio.setOnPlaybackStatusUpdate((status) => {
          const totalDuration = this.millisToMinutesAndSeconds(status.durationMillis)
          const currentPosition = this.millisToMinutesAndSeconds(status.durationMillis - status.positionMillis)
          const percent = status.positionMillis / status.durationMillis * 100
          if (!enableNext && percent == 100) {
            this.setState({ enableNext: true })
          }
          this.setState({
            ...status,
            audio,
            loadingAudio: !status.durationMillis,
            currentPosition,
            totalDuration,
            percent,
          })
        })
        audio.setProgressUpdateIntervalAsync(1000)
      } catch (error) {
        // An error occurred!
        alert(`error${JSON.stringify(error)}`)
      }
    })
  }

  async play() {
    const { audio, percent } = this.state

    if (percent == 100) {
      audio.replayAsync()
    } else {
      audio.playAsync()
    }
  }

  async pause() {
    const { audio } = this.state
    audio.pauseAsync()
  }

  async unload() {
    const { audio } = this.state
    await audio.unloadAsync()
  }

  async next() {
    this.props.navigation.navigate('AudioHome')
  }

  async goToRecord() {
    const recording = new Audio.Recording()

    this.setState({
      mode: 'recording',
      loadingAudio: true,
    })
    this.unload()
    await recording.prepareToRecordAsync(RecordingConfig)
    recording.setOnRecordingStatusUpdate((status) => {
      this.setState({ ...status, recording })
    })
    recording.setProgressUpdateInterval(5000)

    this.setState({ loadingAudio: false, recording })
  }

  async record() {
    const { recording } = this.state

    recording.startAsync()
  }

  async stopRecording() {
    const { recording, index } = this.state
    const { card } = this.props.navigation.state.params
    const newCard = { ...card }

    await recording.stopAndUnloadAsync()
    await recording.createNewLoadedSoundAsync()
    newCard.audios[index].recording = recording.getURI()

    this.setState({
      card: newCard, isDoneRecording: false, loadingAudio: true, totalDuration: '',
    }, () => {
      this.next()
    })
  }

  renderPlayer() {
    const {
      loadingAudio, percent, isPlaying, currentPosition, totalDuration,
    } = this.state
    return <View style={{ flex: 1, bottom: -35, justifyContent: 'center' }} />
  }

  renderSteps() {
    const { index } = this.state

    switch (index) {
      case 0:
        return (
          <Image
            style={{ width: 243, height: 19, resizeMode: 'contain' }}
            source={require('../../assets/images/step1.png')}
          />
        )
      case 1:
        return (
          <Image
            style={{ width: 243, height: 19, resizeMode: 'contain' }}
            source={require('../../assets/images/step2.png')}
          />
        )
      case 2:
        return (
          <Image
            style={{ width: 243, height: 19, resizeMode: 'contain' }}
            source={require('../../assets/images/step3.png')}
          />
        )
    }
  }

  renderRecordButtons() {
    const { isRecording, isDoneRecording, isLoading } = this.state
    return (
      <TouchableOpacity onPress={isRecording ? this.stopRecording : this.record}>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: '#d8d8d8',
            borderRadius: 80,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {!isLoading ? (
            isRecording && !isDoneRecording ? (
              <Image
                style={{ width: 25, height: 25 }}
                source={require('../../assets/images/stop-recording.png')}
              />
            ) : (
              <Image
                style={{ width: 25, height: 25 }}
                source={require('../../assets/images/start-recording.png')}
              />
            )
          ) : (
            <ActivityIndicator size="large" />
          )}
        </View>
        {!isRecording && (
        <Text style={[Styles.cardTitle, { top: 10, fontSize: 13, textAlign: 'center' }]}>
            Record
        </Text>
        )}
      </TouchableOpacity>
    )
  }

  renderNextButton() {
    const { enableNext } = this.state

    return enableNext && (
    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
      <View
        style={[
          {
            width: 80,
            height: 40,
            borderRadius: 10,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
          },
        ]}
      >
        <Text style={[Styles.cardTitle, { color: '#a660b4', fontSize: 13, marginBottom: 0 }]}>
              Start
        </Text>
      </View>
    </TouchableOpacity>
    )
  }

  render() {
    const {
      card, index, loadingAudio, mode, isRecording, isDoneRecording,
    } = this.state
    const { user } = this.props

    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}>
        <KeepAwake />
        <LinearGradient style={{ flex: 1 }} colors={['#6f1a99', '#862298', '#a93097', '#ec939d']}>
          <View style={{ justifyContent: 'space-between', padding: 25 }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                paddingTop: 25,
                width: '100%',
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('AudioHome')
                }}
              >
                <Image
                  source={require('../../assets/images/back-angle.png')}
                  style={{
                    left: -4, top: -4, width: 21, height: 21,
                  }}
                />
              </TouchableOpacity>
            </View>

            <View>
              <Text style={[Styles.heading, { marginTop: 8, color: 'white' }]}>{card.title}</Text>
              {mode == 'playing' && (
              <View style={{ justifyContent: 'flex-start' }}>
                <Text
                  style={[
                    Styles.instructions,
                    {
                      lineHeight: 40,
                      opacity: 1.0,
                      fontFamily: 'gt-walsheim-medium',
                      textAlign: 'left',
                      fontSize: 32,
                    },
                  ]}
                >
                    Hey
                  {' '}
                  {user.first_name}
!
                </Text>
                <Text
                  style={[
                    Styles.instructions,
                    {
                      lineHeight: 40,
                      opacity: 1.0,
                      fontFamily: 'gt-walsheim-medium',
                      textAlign: 'left',
                      fontSize: 32,
                    },
                  ]}
                >
                    Welcome to Atlas.
                </Text>
                <View style={{ marginTop: 15, width: 270 }}>
                  <Text
                    style={[
                      Styles.instructions,
                      { fontSize: 16, opacity: 0.9, textAlign: 'left' },
                    ]}
                  >
                      Click play to hear our welcome message!
                  </Text>
                </View>
              </View>
              )}
            </View>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
            <View
              style={{
                flex: 2.3,
                top: -10,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              {mode == 'playing' ? (
                this.renderPlayer()
              ) : (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around' }}>
                  <View style={{ paddingLeft: 20, paddingRight: 20, justifyContent: 'flex-start' }}>
                    <Text style={Styles.question}>{card.questions[index]}</Text>
                  </View>
                  <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                    <Text style={Styles.instructions}>
                      Take your time. Once you start talking, you’ll find that your words start to
                      flow.
                    </Text>
                  </View>
                </View>
              )}
            </View>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              {mode == 'playing' ? this.renderNextButton() : this.renderRecordButtons()}
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardPreview)
