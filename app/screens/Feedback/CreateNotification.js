import React from 'react'
import {
  Alert,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import map from 'lodash/map'
import compact from 'lodash/compact'
import Styles from './Styles'
import Header from '../../components/Header/Header'
import Emoji from '../../components/Emoji/Emoji'
import { sendCustomNotification, getMembers } from '../../configs/Firebase'


class CreateNotfication extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      members: [],
      text: '',
      mood: '',
      description: '',
      onText: false,
      emoji: {},
      emojis: [],
      searched: [],
      loading: false,
      isDailyCheckIn: false,
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._searching = this.searching.bind(this)
    this._selectEmoji = this.selectEmoji.bind(this)
    this._fullView = this.fullView.bind(this)
  }

  componentWillMount() {
    this.getMembers()
  }

  isDailyCheckIn() {
    this.setState({
      isDailyCheckIn:
        !!this.props.navigation.state.params && this.props.navigation.state.params.showDailyCheckIn,
    })
  }

  async getMembers() {
    try {
      const members = await getMembers(this.props.user)
      this.setState({ members })
    } catch (e) {
      Alert.alert(e.error)
    }
  }

  getEmojis() {
    this.setState({
      emojis: this.props.emojis.map((emoji) => {
        emoji.checked = false
        return emoji
      }),
    })
  }

  navigate() {
    this.props.navigation.goBack()
  }

  selectEmoji(emoji) {
    this.setState({ emoji, text: emoji.mood, onText: false }, () => {
      this.searching(emoji.mood)
    })
    this.refs.text_input.blur()
  }

  searching(text) {
    const { emojis, emoji } = this.state
    if (text.toLowerCase() != emoji.mood) {
      this.setState({ emoji: {} })
    }
    const searched = emojis.filter((emoji) => {
      emoji.checked = emoji.mood == text.toLowerCase()
      return emoji.mood.substring(0, text.length) == text.toLowerCase()
    })
    this.setState({ text, searched })
  }

  fullView() {
    Keyboard.dismiss()
  }

  submitDialog() {
    Alert.alert('Are you sure you want to send this notification?', null, [
      { text: 'Yes!', onPress: this._submit },
      { text: 'Cancel' },
    ])
  }

  prepareNotifications(tokens, notificationText) {
    return tokens.map(token => ({ to: token, body: notificationText }))
  }

  async submit() {
    const { description, members } = this.state
    const notificationText = { description, createdAt: Date.now(), userId: this.props.user.id }
    const tokens = compact(map(members, 'token'))
    const notifications = this.prepareNotifications(tokens, description)
    const customNotification = { notificationText, createdAt: Date.now(), userId: this.props.user.id }

    try {
      await sendCustomNotification(customNotification, notifications)
      Alert.alert(
        'Thank you!',
        'Your custom notification was sent to everyone',
        [
          { text: '✨' },
        ],
        { cancelable: false },
      )
      this.props.navigation.pop(1)
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  render() {
    const {
      text,
      description,
      onText,
      emoji,
      emojis,
      searched,
      loading,
      isDailyCheckIn,
    } = this.state

    return (
      <View style={Styles.topContainer}>
        <Header
          onCancel={this._navigate}
          onSave={() => this.submitDialog()}
          rightText="Send"
          disabledRight={!description}
          title="New"
        />
        <View style={Styles.container}>
          <View style={Styles.modalView}>
            <View style={[Styles.flexView, Styles.marginTop]}>
              <View style={Styles.marginForm}>
                <View style={Styles.feeling}>
                  <Text style={Styles.mainTitle}>What's the notification you want to send?</Text>
                </View>
              </View>
              <View style={Styles.marginForm}>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Emojis encouraged"
                  multiline
                  style={[Styles.textInput, Styles.marginTopSmall]}
                  onChangeText={description => this.setState({ description })}
                  value={description}
                  returnKeyType="done"
                  returnKeyLabel="Done"
                  onSubmitEditing={Keyboard.dismiss}
                />
              </View>
            </View>
          </View>
          {!!onText && (
          <View style={Styles.flexView2}>
            {loading ? (
              <ActivityIndicator size="large" />
            ) : (
              <Emoji
                emojis={text.length ? searched : emojis}
                isSearching={!!text.length}
                onPress={this._selectEmoji}
                fullView={this._fullView}
              />
            )}
          </View>
          )}
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  emojis: state.checkIn.emojis,
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateNotfication)
