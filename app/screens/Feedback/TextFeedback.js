import React from 'react'
import Slider from 'react-native-slider'
import {
  Text,
  View,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { LinearGradient } from 'expo'
import Styles from './SliderStyles'
import Header from '../../components/Header/Header'
import Emoji from '../../components/Emoji/Emoji'


const windowHeight = Dimensions.get('window').height

class TextFeedback extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)

    this.state = {
      text: '',
      mood: '',
      description: '',
      onText: false,
      emoji: {},
      emojis: [],
      searched: [],
      loading: false,
      card: props.navigation.state.params.card,
      textFeedback: '',
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._fullView = this.fullView.bind(this)
  }

  navigate() {
    this.props.navigation.goBack()
  }

  componentWillReceiveProps(nextProps) {}

  fullView() {
    Keyboard.dismiss()
  }

  async submit() {
    this.props.navigation.navigate('Recap')
  }

  renderSlider = () => {
    const { results } = this.props.navigation.state.params

    return (
      <View style={Styles.slider}>
        <Animated.Text
          style={[
            Styles.moodValue,
            {
              fontWeight: 'bold',
              fontSize: 90,
              fontFamily: 'gt-walsheim-regular',
              color: new Animated.Value(this.state.value).interpolate({
                inputRange: [0, 10],
                outputRange: ['#A653E7', '#64B1EB'],
                extrapolate: 'clamp',
              }),
            },
          ]}
        >
          {this.state.value}
        </Animated.Text>
        <Slider
          maximumValue={10}
          thumbTintColor={new Animated.Value(this.state.value).interpolate({
            inputRange: [0, 10],
            outputRange: ['#A653E7', '#64B1EB'],
            extrapolate: 'clamp',
          })}
          animateTransitions
          trackStyle={{ height: 2, backgroundColor: '#A5A5A5' }}
          thumbStyle={{ width: 30, height: 30, borderRadius: 30 }}
          minimumValue={0}
          value={this.state.value}
          step={1}
          minimumTrackTintColor={new Animated.Value(this.state.value).interpolate({
            inputRange: [0, 10],
            outputRange: ['#A653E7', '#64B1EB'],
            extrapolate: 'clamp',
          })}
          onValueChange={value => this.setState({ value, onText: true })}
        />
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
          <View>
            <Text
              style={{
                fontFamily: 'gt-walsheim-bold',
                fontSize: 14,
                width: 75,
                flexWrap: 'wrap',
                color: '#3F3F3F',
                textAlign: 'left',
              }}
            >
              0
            </Text>
            <Text
              style={{
                fontFamily: 'gt-walsheim-regular',
                fontSize: 14,
                width: 75,
                flexWrap: 'wrap',
                color: 'black',
                textAlign: 'left',
              }}
            >
              Not Meaningful
            </Text>
          </View>
          <View>
            <Text
              style={{
                fontFamily: 'gt-walsheim-bold',
                fontSize: 14,
                width: 75,
                flexWrap: 'wrap',
                color: '#3F3F3F',
                textAlign: 'right',
              }}
            >
              10
            </Text>
            <Text
              style={{
                fontFamily: 'gt-walsheim-regular',
                fontSize: 14,
                width: 75,
                flexWrap: 'wrap',
                color: 'black',
                textAlign: 'right',
              }}
            >
              Extremely Meaningful
            </Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const {
      text,
      description,
      onText,
      emoji,
      emojis,
      searched,
      loading,
      textFeedback,
    } = this.state
    const { index, results } = this.props.navigation.state.params
    const backIndex = index - 1

    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}>
        <LinearGradient style={{ flex: 1 }} colors={['#6f1a99', '#862298', '#a93097', '#ec939d']}>
          <View style={Styles.topContainer}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingLeft: 30,
                  paddingRight: 30,
                  width: '100%',
                  top: '12%',
                  zIndex: 5,
                  marginBottom: 30,
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack()
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'gt-walsheim-regular',
                        color: 'white',
                        textAlign: 'center',
                        opacity: 0.8,
                      }}
                    >
                      Back
                    </Text>
                  </View>
                </TouchableOpacity>

                <Text style={[Styles.feelingLabel, { left: 18 }]}>Feedback</Text>

                <TouchableOpacity
                  style={[Styles.nextButton]}
                  onPress={() => {
                    this.submit()
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'gt-walsheim-bold',
                        color: 'white',
                        textAlign: 'center',
                      }}
                    >
                      {'Done'}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.modalView}>
                <View style={[
                  Styles.modalViewContainer, 
                  { 
                    left: -8,
                    alignItems: 'stretch', 
                  },
                ]}>
                  <View style={{ width: '100%' }}>
                    <View style={{ width: '100%' }}>
                      <TextInput
                        underlineColorAndroid="transparent"
                        placeholder="How could we improve this episode? Are there any questions you wished we asked?"
                        multiline
                        style={Styles.textInput}
                        onChangeText={textFeedback => this.setState({ textFeedback })}
                        value={textFeedback}
                        returnKeyType="done"
                        returnKeyLabel="Done"
                        onsubmitEditing={Keyboard.dismiss}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextFeedback)
