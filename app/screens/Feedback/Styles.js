const Styles = {
  topContainer: {
    height: '100%',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  flexView: {
    flex: 1,
    width: '100%',
  },
  flexView2: {
    paddingTop: 3,
    justifyContent: 'flex-start',
    width: '100%',
  },
  flexRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
  },
  grayBackground: {
    backgroundColor: '#A8A8A8',
  },
  blackBackground: {
    backgroundColor: 'black',
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  width100: {
    flexDirection: 'row',
  },
  marginForm: {
    width: '100%',
    marginBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  name: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 18,
    color: '#575757',
  },
  marginTop: {
    marginTop: '12%',
  },
  marginTopSmall: {
    marginTop: '2%',
  },
  center: {
    textAlign: 'center',
  },
  fontGTBold: {
    fontFamily: 'gt-walsheim-bold',
    color: '#343434',
  },
  fontGTRegular: {
    fontFamily: 'gt-walsheim-regular',
    color: '#575757',
    fontSize: 16,
  },
  opacity: {
    opacity: 0.6,
  },
  colorPink: {
    color: '#F36F7A',
  },
  fontSizeBigger: {
    fontSize: 18,
  },
  fontSizeSmaller: {
    fontSize: 14,
  },
  actions: {
    height: 70,
    paddingTop: 30,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  textInput: {
    height: 250,
    paddingBottom: 10,
    fontSize: 17,
    letterSpacing: -0.16,
    fontFamily: 'gt-walsheim-regular',
  },
  textInputEmoji: {
    width: 'auto',
    fontFamily: 'gt-walsheim-regular',
    minWidth: 50,
    height: 40,
    lineHeight: 32,
    fontSize: 30,
  },
  textInputSearch: {
    width: '100%',
    height: 35,
    paddingLeft: 5,
    fontSize: 14,
  },
  checkImage: {
    width: 30,
    height: 30,
  },
  link: {
    color: '#ED5198',
  },
  selected: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  search: {
    backgroundColor: '#F3F3F3',
    paddingLeft: 8,
    paddingTop: 12,
    paddingRight: 8,
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 6,
    marginTop: '5%',
    marginBottom: '3%',
  },
  chip: {
    flexDirection: 'row',
    borderRadius: 16,
    height: 30,
    paddingTop: 5,
    paddingLeft: 8,
    paddingRight: 8,
    marginBottom: 5,
    backgroundColor: '#FC636B',
  },
  chipText: {
    color: 'white',
    fontFamily: 'gt-walsheim-medium',
    fontSize: 12,
    marginTop: 2,
  },
  favorites: {
    paddingLeft: '1%',
    paddingRight: '1%',
  },
  paddingRow: {
    paddingRight: 5,
    paddingLeft: 5,
  },
  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 5,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
    padding: 10,
  },
  listName: {
    fontSize: 15,
    letterSpacing: 0.16,
    fontFamily: 'gt-walsheim-regular',
    color: '#4A4A4A',
    lineHeight: 28,
  },
  delete: {
    color: '#9B9B9B',
    fontSize: 15,
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
  },
  flag: {
    width: '100%',
    backgroundColor: '#dedede',
    padding: 13,
  },
  titleFlag: {
    color: '#858585',
    fontSize: 12,
    fontFamily: 'gt-walsheim-medium',
    letterSpacing: 1.5,
  },
  feeling: {
    marginBottom: 15,
  },
  emoji: {
    width: 30,
    height: 30,
    marginLeft: 5,
  },
  checked: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainTitle: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 20,
    color: '#575757',
  },
}

export default Styles
