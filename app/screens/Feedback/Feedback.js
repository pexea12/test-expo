import React from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo'
import Styles from './SliderStyles'
import StarBackground from '../../components/UI/StarBackground'
import SkillCircle from '../../components/UI/SkillCircle'
import { updateProfile } from '../../configs/Firebase'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const isiPhoneSE = windowHeight < 600

class Feedback extends React.Component {
  static navigationOptions() {
    return {
      header: null,
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
      swipeEnabled: false,
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      card: props.navigation.state.params.card,
      value: 5,
    }
  }

  componentDidMount() {
    const { card } = this.state
    segmentAnalyzePage(this.props.user, 'Skill Bump', {
      card: card.title,
      index: this.state.index,
      isDaily: card.isDaily,
    })
  }

  submit = async () => {
    const { card } = this.state
    if (card.isDaily) {
      updateProfile(this.props.user.id, { finishedDaily: true })
      this.props.navigation.navigate('AudioHome')
    } else {
      this.props.navigation.navigate('Recordings')
    }
  }

  render() {
    const { value } = this.state
    const { skill, quote } = this.props
    const defaultColor = ['#040029', '#1D0E57', '#371D88', '#4F1C82', '#691A7C']

    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <LinearGradient
          colors={defaultColor}
          style={{
            flex: 1,
            width: windowWidth,
            height: windowHeight - isiPhoneSE * windowHeight * 0.1,
          }}
        >
          <StarBackground />
          <View
            style={{
              zIndex: 1000,
              width: '100%',
              top: 30,
              justifyContent: 'flex-end',
              flexDirection: 'row',
            }}
          >
            <TouchableOpacity
              style={[Styles.nextButton]}
              onPress={() => {
                this.submit(value)
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'gt-walsheim-bold',
                  color: 'white',
                  textAlign: 'center',
                }}
              >
                Finish
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <View style={{ height: windowHeight * 0.6, justifyContent: 'flex-start' }}>
              <View style={{ width: '100%', paddingLeft: 30, paddingRight: 30 }}>
                <Text
                  style={[
                    Styles.feelingLabel,
                    {
                      textAlign: 'center', fontSize: 18, flexWrap: 'wrap', lineHeight: 23,
                    },
                  ]}
                >
                  {quote.quote}
                </Text>
                <View
                  style={{
                    top: 20,
                    width: '100%',
                    justifyContent: 'center',
                    flexDirection: 'row',
                  }}
                >
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'center',
                      opacity: 0.7,
                      letterSpacing: 2.5,
                      fontSize: 15,
                      textTransform: 'uppercase',
                    }}
                  >
                    {quote.author}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: '100%',
                  width: '100%',
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <View>
                  <SkillCircle skill={skill.name} points={skill.level || 0} animated size={190} />
                  <Text
                    style={{
                      top: 20,
                      opacity: 0.9,
                      color: 'white',
                      textAlign: 'center',
                      textTransform: 'capitalize',
                      fontFamily: 'gt-walsheim-regular',
                      fontSize: 21,
                    }}
                  >
                    {skill.name}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('FeedbackForm', {
                transition: 'modal',
                card: this.state.card,
              })
            }}
          >
            <Text
              style={{
                bottom: 30,
                fontSize: 16,
                fontFamily: 'gt-walsheim-bold',
                color: 'white',
                textAlign: 'center',
              }}
            >
              Give Us Feedback!
            </Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const { quote, skill } = state.dailies
  return {
    user: state.auth.user,
    quote,
    skill,
  }
}

export default connect(mapStateToProps)(Feedback)
