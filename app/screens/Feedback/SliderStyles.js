const Styles = {
  topContainer: {
    height: '100%',
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  nextButton: {
    width: 72,
    height: 30,
    borderRadius: 15,
    backgroundColor: 'transparent',
    justifyContent: 'center',
  },

  feelingLabel: {
    fontFamily: 'gt-walsheim-regular',
    textAlign: 'center',
    fontSize: 24,
    color: 'white',
  },

  modalView: {
    width: '90%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: '2%',
    paddingRight: '2%',
    borderRadius: 7,
    marginLeft: 20,
  },

  modalViewContainer: {
    flex: 1,
    width: '100%',
    marginTop: '5%',
    alignItems: 'center',
  },

  modalForm: {
    width: '100%',
    alignSelf: 'center',
  },

  textInput: {
    width: '100%',
    padding: 20,
    paddingTop: 30,
    lineHeight: 24,
    color: 'black',
    fontSize: 18,
    letterSpacing: -0.16,
    height: 500,
    borderRadius: 10,
    backgroundColor: 'white',
    fontFamily: 'gt-walsheim-regular',
    top: 50,
  },

  slider: {
    marginTop: '20%',
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
    zIndex: 10,
  },

  moodValue: {
    fontFamily: 'gt-walsheim-medium',
    color: '#F27468', 
    fontSize: 36,
    textAlign: 'center'
  },
}

export default Styles
