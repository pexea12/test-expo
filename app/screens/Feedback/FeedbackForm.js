import React from 'react'
import Slider from 'react-native-slider'
import {
  Text,
  View,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { LinearGradient } from 'expo'
import Styles from './SliderStyles'
import Header from '../../components/Header/Header'
import Emoji from '../../components/Emoji/Emoji'

import { sendFeedback } from '../../configs/Firebase'

import moment from '../../configs/Moment'
import { segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'

const windowHeight = Dimensions.get('window').height
const iPhoneXPadding = (windowHeight > 800) * 20

class FeedbackForm extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)

    this.state = {
      text: '',
      mood: '',
      description: '',
      onText: false,
      emoji: {},
      emojis: [],
      searched: [],
      loading: false,
      textFeedback: '',
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._fullView = this.fullView.bind(this)
  }

  navigate() {
    this.props.navigation.goBack()
  }

  componentWillReceiveProps(nextProps) {}

  fullView() {
    Keyboard.dismiss()
  }

  async submit() {
    const { textFeedback } = this.state
    const CurrentDate = moment().format()
    const feedback = {
      feedback: textFeedback, createdAt: Date.now(), firstName: this.props.user.first_name, lastName: this.props.user.last_name, email: this.props.user.email,
    }

    try {
      await sendFeedback(feedback)
      segmentAnalyzeEvent('Feedback Submitted', { feedback: feedback.feedback })
      this.props.navigation.goBack()
    } catch (e) {
      Alert.alert('Error', e.message)
    }
    Alert.alert(
      'Thank you!',
      "Thank you for your feedback on this early version of Atlas — we're excited to improve this experience for you!.",
    )
    this.props.navigation.goBack()
  }

  render() {
    const {
      text,
      description,
      onText,
      emoji,
      emojis,
      searched,
      loading,
      textFeedback,
    } = this.state

    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}>
        <LinearGradient style={{ flex: 1 }} colors={['#6f1a99', '#862298', '#a93097', '#ec939d']}>
          <View style={Styles.topContainer}>
            <View style={{ top: 25 + iPhoneXPadding }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingLeft: 26,
                  paddingRight: 20,
                  width: '100%',
                  zIndex: 1000,
                }}
              >
                <TouchableOpacity
                  style={{ zIndex: 1000 }}
                  onPress={() => {
                    this.props.navigation.goBack()
                  }}
                >
                  <Image
                    source={require('../../assets/images/xbutton.png')}
                    style={{ width: 21, height: 21 }}
                  />
                </TouchableOpacity>
                <Text style={[Styles.feelingLabel, { left: 30 }]}>Feedback</Text>
                <TouchableOpacity
                  style={[Styles.nextButton]}
                  onPress={() => {
                    this.submit()
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'gt-walsheim-bold',
                        color: 'white',
                        textAlign: 'center',
                      }}
                    >
                      {'Submit'}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.modalView}>
                <View style={Styles.modalViewContainer}>
                  <View style={Styles.modalForm}>
                    <TextInput
                      underlineColorAndroid="transparent"
                      placeholder="Tell us how we can make Atlas better!"
                      multiline
                      style={Styles.textInput}
                      onChangeText={textFeedback => this.setState({ textFeedback })}
                      value={textFeedback}
                      returnKeyType="done"
                      returnKeyLabel="Submit"
                      onSubmitEditing={Keyboard.dismiss}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedbackForm)
