import React from 'react'
import {
  Image,
  View,
  TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import BaseStyles from '../BaseStyles/Styles'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'


class Footer extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  }

  navigateHome = () => {
    const {
      user,
      navigation,
    } = this.props

    segmentAnalyzePage(user, 'Home')
    navigation.navigate('AudioHome', { transition: 'ios' })
  }

  render() {
    const {
      isHome,
      isDailies,
      isEpisodes,
      isRecordings,
      incomplete,
      navigation,
    } = this.props

  return (
    <View style={{ width: '100%' }}>
      <View style={[BaseStyles.footerHome]}>
        {!isHome ? (
          <TouchableOpacity
            onPress={() => {
              this.navigateHome()
            }}
          >
            <Image
              style={[BaseStyles.iconFooter]}
              source={require('../../assets/images/home.png')}
            />
          </TouchableOpacity>
        ) : (
          <Image
            style={[BaseStyles.iconFooter]}
            source={require('../../assets/images/home-color.png')}
          />
        )}
        {!isEpisodes ? (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Episodes', { transition: 'none' })
            }}
          >
            <Image
              style={[BaseStyles.iconFooter]}
              source={require('../../assets/images/footer/episodes.png')}
            />
          </TouchableOpacity>
        ) : (
          <Image
            style={[BaseStyles.iconFooter]}
            source={require('../../assets/images/footer/episodes-selected.png')}
          />
        )}
        {!isRecordings ? (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Recordings', { transition: 'none' })
            }}
          >
            <Image
              style={[BaseStyles.iconFooterExplore]}
              source={require('../../assets/images/footer/recordings.png')}
            />
          </TouchableOpacity>
        ) : (
          <Image
            style={[BaseStyles.iconFooterExplore]}
            source={require('../../assets/images/footer/recordings-selected.png')}
          />
        )}
        {!isDailies ? (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Dailies', { transition: 'none' })
            }}
          >
            <Image
              style={[BaseStyles.iconFooterExplore]}
              source={require('../../assets/images/footer/dailies.png')}
            />
          </TouchableOpacity>
        ) : (
          <Image
            style={[BaseStyles.iconFooterExplore]}
            source={require('../../assets/images/footer/dailies-selected.png')}
          />
        )}
      </View>
    </View>
  )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Footer)
