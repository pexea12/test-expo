const Styles = {
  modal: {
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: '#FFFFFF',
    height: '40%',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingLeft: '5%',
    paddingRight: '5%',
    borderRadius: 5,
    borderColor: '#979797',
  },
  flexView: {
    flex: 1,
    justifyContent: 'center',
  },
  flexView2: {
    flex: 2,
    justifyContent: 'center',
    width: '100%',
  },
  buttonView: {
    width: '100%',
  },
  center: {
    textAlign: 'center',
  },
  heading: {
    marginTop: '5%',
    fontFamily: 'gt-walsheim-medium',
    fontSize: 25,
    color: '#343434',
  },
  subtitle: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 17,
    letterSpacing: 0.2,
    color: '#9B9B9B',
    marginTop: '8%',
  },
  paragraph: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 17,
    letterSpacing: 0.2,
    marginLeft: '5%',
    marginRight: '5%',
    marginTop: '5%',
    marginBottom: '5%',
    color: '#2A2A2A',
  },
}

export default Styles
