import React from 'react'
import {
  Modal,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ImageBackground,
  Alert,
} from 'react-native'
import { LinearGradient } from 'expo'
import Styles from './CheckInReportStyles'
import GradientButton from '../../components/Buttons/GradientButton'

export default class FirstCheckIn extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Modal animationType="fade" transparent visible onRequestClose={() => {}}>
        <TouchableHighlight style={Styles.modal}>
          <View style={Styles.modalView}>
            <View style={Styles.flexView}>
              <Text style={[Styles.center, Styles.heading]}>Post Reported</Text>
            </View>
            <View style={Styles.flexView}>
              <Text style={[Styles.center, Styles.paragraph]}>Thanks for reporting this post.</Text>
            </View>
            <View style={[Styles.flexView, Styles.buttonView]}>
              <TouchableOpacity onPress={this.props.onClose}>
                <GradientButton text="Okay" />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    )
  }
}
