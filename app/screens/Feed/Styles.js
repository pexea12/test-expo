const Styles = {
  container: {
    flex: 1,
    backgroundColor: '#DFDFDF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  profileIcon: {
    width: 32,
    height: 32,
    overflow: 'hidden',
    borderRadius: 35,
    backgroundColor: '#D8D8D8',
  },
  profileImage: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginBottom: '5%',
    marginRight: '2%',
  },
  flexRow: {
    flexDirection: 'row',
    width: '100%',
    flex: 1,
  },
  flexVertical: {
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: '84%',
    flex: 1,
    justifyContent: 'center',
    top: -6,
    height: 57,
  },
  flexView: {
    justifyContent: 'flex-start',
    width: '100%',
  },
  flexRow2: {
    flexDirection: 'row',
  },
  inline: {
    flexDirection: 'row',
  },
  selected: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    position: 'absolute',
    top: 65,
    paddingLeft: 20,
    zIndex: 1,
    backgroundColor: '#ffffff',
    paddingTop: 20,
  },
  chip: {
    flexDirection: 'row',
    borderRadius: 16,
    height: 27,
    paddingTop: 5,
    paddingLeft: 14,
    paddingRight: 7,
    marginBottom: 5,
    marginRight: 6,
    backgroundColor: '#FC636B',
  },
  chipText: {
    color: 'white',
    paddingRight: 8,
    fontFamily: 'gt-walsheim-medium',
    fontSize: 14,
    marginTop: 0,
  },
  emptyState: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 18,
    color: '#D0D0D0',
    marginTop: '50%',
    textAlign: 'center',
  },
  emptyStateWrapper: {
    width: '100%',
    justifyContent: 'center',
  },
  image: {
    width: 17,
    height: 17,
  },
  grayBackground: {
    backgroundColor: '#DFDFDF',
  },
  iconHeart: {
    width: 122,
    height: 28,
    paddingRight: 3,
  },
  iconComment: {
    width: 20,
    height: 16.5,
    marginTop: 4,
  },
  iconPeople: {
    width: 19,
    height: 20,
  },
  counter: {
    width: 25,
    textAlign: 'center',
    left: -26,
    bottom: -5,
  },
  counterPeople: {
    paddingLeft: 5,
  },
  postName: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 14,
  },
  textRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
  },
  textWidth: {
    maxWidth: '100%',
  },
  colorRed: {
    color: '#F1636E',
  },
  colorGreen: {
    color: '#2ECC71',
  },
  colorOrange: {
    color: '#F89406',
  },
  colorGray: {
    color: '#4A4A4A',
  },
  colorGrayIcons: {
    color: '#A4A4A4',
  },
  colorPink: {
    color: 'white',
    fontFamily: 'gt-walsheim-bold',
  },
  colorBlack: {
    color: '#000000',
  },
  center: {
    textAlign: 'center',
    lineHeight: 25,
  },
  notification: {
    position: 'absolute',
    top: 14,
    right: 4,
    zIndex: 1,
  },
  notificationMenu: {
    position: 'absolute',
    top: 30,
    right: 8,
    zIndex: 2,
  },
  menuOption: {
    width: 120,
    textAlign: 'center',
    lineHeight: 20,
    marginTop: 3,
    marginBottom: 3,
    position: 'relative',
    zIndex: 4,
  },
  downArrow: {
    width: 15,
    height: 9,
    margin: 10,
  },
  circleRed: {
    width: 10,
    height: 10,
    marginRight: 10,
    marginTop: 10,
  },
  triangle: {
    position: 'absolute',
    right: 0,
    width: 26,
    height: 7,
  },
  square: {
    marginTop: 6,
    borderWidth: 1,
    borderColor: '#b8b8b8',
    backgroundColor: '#ffffff',
    color: '#ffffff',
    paddingTop: 10,
    paddingBottom: 10,
    opacity: 1,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1.0,
  },
  emoji: {
    width: 15,
    height: 15,
    marginLeft: 5,
    marginTop: 5,
  },
  borderBottom: {
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 1,
    paddingBottom: 15,
    marginBottom: 13,
  },
  marginTop: {
    marginTop: 20,
  },
  padding: {
    paddingRight: 23,
    paddingLeft: 10,
  },
  description: {
    opacity: 0.8,
    zIndex: 0,
  },
  time: {
    opacity: 0.5,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 13,
    bottom: -1,
    right: -10,
  },
  headerFooter: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
  },
  headerOption: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 18,
    color: '#9B9B9B',
  },
  headerHome: {
    position: 'absolute',
    top: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 0.7,
    paddingTop: 36,
    paddingBottom: 12,
    paddingLeft: 15,
    zIndex: 2,
  },
  iconHeaderProfile: {
    height: 26,
    marginVertical: 12,
    paddingTop: 8,
  },
  iconSettings: {
    height: 28,
    width: 28,
    opacity: 0.5,
  },
  iconHeaderPrivate: {
    height: 20,
    width: 28,
    marginVertical: 13,
  },
  redNotification: {
    height: 8,
    width: 8,
    marginTop: 20,
    marginLeft: 2,
    position: 'absolute',
  },
  arrowBack: {
    padding: 10,
    left: -10,
  },
  iconFooterHome: {
    height: 24,
    width: 28,
    marginVertical: 10,
    marginHorizontal: 6,
  },
  iconFooterCheckIn: {
    height: 32,
    width: 32,
    marginVertical: 10,
    marginHorizontal: 3,
  },
  iconFooterPulse: {
    height: 27,
    width: 36,
    marginVertical: 10,
    marginHorizontal: 2,
  },
  mainTitle: {
    fontFamily: 'gt-walsheim-bold',
    fontSize: 22,
    opacity: 0.9,
    color: 'black',
    top: '2%',
  },
  dateToday: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 18,
    top: 4,
    opacity: 0.75,
    color: 'black',
  },
  homeTitle: {},
  privateTitle: {
    left: -5,
  },
  homeList: {
    marginBottom: 65,
    width: '100%',
  },
  inlineIcons: {
    flexDirection: 'row',
  },
  marginView: {
    marginTop: 78,
    marginBottom: 20,
  },
  textRow: {},
  dailyBox: {
    height: 130,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    border: 1,
    borderColor: 'gray',
    borderRadius: 6,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    padding: 10,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 0.3,
  },
  pollDescription: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 16,
  },
  poll: {
    height: 100,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'left',
    border: 0.3,
    borderColor: '#FC636B',
    borderWidth: 2,
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    padding: 10,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 0.3,
  },
  fullWidth: {
    width: '100%',
  },
  checkInText: {
    fontFamily: 'gt-walsheim-bold',
    fontSize: 17,
    color: 'white',
    textAlign: 'center',
  },
  timeText: {
    fontFamily: 'gt-walsheim-bold',
    color: 'white',
    textAlign: 'center',
    fontSize: 13,
    color: 'rgba(255, 255, 255, 0.40)',
    marginBottom: 5,
  },
  pollQuestion: {
    fontFamily: 'gt-walsheim-medium',
    color: '#824ADB',
    fontSize: 15,
  },
}

export default Styles
