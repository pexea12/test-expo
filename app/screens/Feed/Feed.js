import React from 'react'
import {
  Dimensions,
  Modal,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import keys from 'lodash/keys'
import find from 'lodash/find'
import sortBy from 'lodash/sortBy'
import map from 'lodash/map'
import { Notifications, LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import {
  listenPosts, listenStories, listenMembers, storeEmojis, listenCheckIns, listenJustMeCheckIns,
} from '../../redux/checkIn/actions'
import {
  reportFeedStory, deleteFeedStory, getPolls, getEmojis, getMembers, updateHearts, deletePost, reportPost, getStories, getSchools,
} from '../../configs/Firebase'
import moment from '../../configs/Moment'
import Styles from './Styles'
import BaseStyles from '../BaseStyles/Styles'
import CheckInReport from './CheckInReport'
import Footer from '../Footer/Footer.js'

const windowHeight = Dimensions.get('window').height


const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 30
  return layoutMeasurement.height + contentOffset.y
        >= contentSize.height - paddingToBottom
}

const analytics = new Analytics('UA-118205605-3')


class Feed extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  static transitionConfig = {
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      filter: null,
      posts: [],
      stories: [],
      unread: false,
      count: 7,
      appState: AppState.currentState,
      members: [],
      modalVisible: false,
    }

    this._navigate = this.navigate.bind(this)
    this._onScroll = this.onScroll.bind(this)
    this._search = this.search.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      if (this.props.posts || nextProps.posts) {
        this.setState({ members: nextProps.members })
        if (this.props.posts && this.props.posts.length != nextProps.posts.length) {
          this.getPosts(nextProps.posts)
        } else {
          const updatedPosts = this.updatePosts(this.props.posts, nextProps.posts)
          this.getPosts(updatedPosts)
        }
      }
    }
  }

  componentWillMount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
    this.getSchools()
  }

  async getSchools() {
    try {
      const schools = await getSchools()
      this.setState({ schools })
    } catch (e) {
      Alert.alert(e)
    }
  }

  componentDidMount() {
    if (this.props.user) {
      this.getMembers()
    }
    this._notificationSubscription = Notifications.addListener(this._handleNotification)
    AppState.addEventListener('change', this._handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      analytics
        .event(new Event('PageView', 'Home', this.props.user ? this.props.user.id : null))
        .then(() => console.log('success'))
        .catch(e => console.log(e.message))
    }
    this.setState({ appState: nextAppState })
  }

  _handleNotification = (notification) => {
    if (notification.origin == 'selected') {
      if (notification.data.isComment) {
        this.props.navigation.navigate('Comments', notification.data)
      }
    }
  };

  updatePosts(oldPosts, newPosts) {
    return oldPosts.map((post, index) => {
      post.hearts = newPosts[index].hearts
      post.commentsCount = newPosts[index].commentsCount
      post.hidden = newPosts[index].hidden
      return post
    })
  }

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  async getPosts(posts = []) {
    const { members } = this.state
    const memberIds = map(members, 'id')
    try {
      posts = posts.filter(post => post.hidden != true)
      posts = posts.map((post) => {
        const index = memberIds.indexOf(post.userId)
        if (index != -1) {
          post.name = `${members[index].first_name} ${members[index].last_name}`
          if (members[index].profile_picture != null) {
            post.profile_picture = `${members[index].profile_picture}`
          }
        } else if (post.userId == this.props.user.id) {
          post.name = `${this.props.user.first_name} ${this.props.user.last_name}`
          post.profile_picture = `${this.props.user.profile_picture}`
        }
        return post
      })
      this.setState({ posts })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  async getMembers() {
    try {
      this.props.listenMembers()
      this.props.listenPosts()
      this.getPosts(this.props.posts)
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  onScroll({ nativeEvent }) {
    if (isCloseToBottom(nativeEvent)) {
      this.setState({ count: this.state.count + 5 })
    }
  }

  search(text) {
    const { allMembers } = this.state
    const searchedMembers = allMembers.filter((elem) => {
      const inFirst = elem.first_name.toLowerCase().indexOf(text.toLowerCase()) > -1
      const inLast = elem.last_name.toLowerCase().indexOf(text.toLowerCase()) > -1
      const inEmail = elem.email.toLowerCase().indexOf(text.toLowerCase()) > -1
      return inFirst || inLast || inEmail
    })
    this.setState({ members: text.length ? searchedMembers : allMembers, search: text })
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  chip(item) {
    const close = item.mood != 'All Moods'
    return (
      <View style={[Styles.chip, !close && Styles.grayBackground]}>
        <TouchableOpacity
          style={[Styles.textRegular, Styles.chipText]}
          onPress={() => {
            this.filterAlert(item.mood)
          }}
        >
          <Text style={{ color: 'white' }}>{this.capitalize(item.mood)}</Text>
        </TouchableOpacity>
        {close && (
          <TouchableOpacity
            onPress={() => {
              this.removeFilter()
            }}
          >
            <Image style={Styles.image} source={require('../../assets/images/closeEmpty.png')} />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  toggleHearts = (post) => {
    const hearts = !!post.hearts && keys(post.hearts)
    const { members } = this.state
    const postUser = find(members, { id: post.userId })
    let myHeart = false
    if (!hearts) {
      post.hearts = {}
      post.hearts[this.props.user.id] = true
    } else {
      myHeart = hearts.includes(this.props.user.id)
      if (myHeart) {
        delete post.hearts[this.props.user.id]
      } else {
        post.hearts[this.props.user.id] = true
      }
    }
    updateHearts(post.id, post.hearts)
  };

  option(post) {
    this.setState({ menu: post })
  }

  delete(post) {
    deletePost(post.id)
  }

  async report(post) {
    if (!post.reportedBy) {
      post.reportedBy = {}
    }
    post.reportedBy[this.props.user.id] = true
    try {
      await reportPost(post.id, post.reportedBy)
      this.setState({ modal: true })
    } catch (e) {
      Alert.alert('Error', e.message)
    }

    this.setState({ menu: false })
  }

  renderMenu() {
    const { menu } = this.state
    return (
      <View>
        <Image source={require('../../assets/images/triangle.png')} style={[Styles.triangle]} />
        <View style={Styles.square}>
          {menu.userId != this.props.user.id && !(this.props.user && this.props.user.admin) ? (
            <TouchableOpacity onPress={() => this.report(menu)}>
              <Text style={[Styles.textRegular, Styles.menuOption]}>Report</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => this.delete(menu)}>
              <Text style={[Styles.textRegular, Styles.menuOption]}>Delete</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity onPress={() => this.setState({ menu: false })}>
            <Text style={[Styles.colorGrayIcons, Styles.textRegular, Styles.menuOption]}>
              Cancel
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderPosts() {
    const {
      posts, count, members, menu,
    } = this.state
    if (!posts.length) {
      return (
        <View style={Styles.emptyStateWrapper}>
          <Text style={Styles.emptyState}>Be the first to Post!</Text>
        </View>
      )
    }
    const postArray = [...posts]
    const obj = sortBy(postArray, 'createdAt').reverse()
    obj.length = count
    return obj.map((post) => {
      postTitleShortened = post.title
      if (post.title.length > 110) {
        postTitleShortened = post.title.substr(0, 110)
        postTitleShortened += '...'
      } else {
        postTitleShortened = post.title
      }
      const { modal, unread } = this.state
      const anonURI = require('../../assets/images/logo.png')

      const hearts = !!post.hearts && keys(post.hearts)
      const uri = hearts && hearts.includes(this.props.user.id) ? require('../../assets/images/sameBoat-colored.png')
        : require('../../assets/images/sameBoat.png')

      return (
        <View style={{
          shadowOffset: { width: 0, height: 2 },
          shadowColor: 'rgba(0, 0, 0, 0.25)',
          shadowOpacity: 0.3,
          backgroundColor: 'white',
          padding: 20,
          paddingBottom: 25,
          justifyContent: 'center',
          marginTop: 15,
        }}
        >
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('PostView', { post })
            }}
          >
            <View style={[Styles.flexRow, Styles.textWidth]}>
              <View style={Styles.profileImage}>
                <View style={Styles.profileIcon}>
                  {post.anon && <Image style={{ width: 32, height: 32 }} source={anonURI} />}
                  {!post.anon && (
                  <Image
                    style={{ width: 32, height: 32 }}
                    source={{ uri: post.profile_picture }}
                  />
                  )}
                </View>
              </View>
              <View
                style={{
                  bottom: -4,
                  flexDirection: 'column',
                  flex: 1,
                  justifyContent: 'center',
                  width: 300,
                }}
              >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 45 }}>
                  <Text style={[Styles.postName, Styles.colorBlack]}>
                    {post.anon ? 'Anonymous' : post.name}
                  </Text>
                  <Text style={Styles.time}>{moment(post.createdAt).fromNow(true)}</Text>
                </View>
              </View>
            </View>
            <View style={Styles.notification}>
              <TouchableOpacity onPress={e => this.option(post)}>
                <Image
                  source={require('../../assets/images/down-arrow.png')}
                  style={[Styles.downArrow]}
                />
              </TouchableOpacity>
            </View>
            <View style={Styles.notificationMenu}>
              {menu && menu.id == post.id && this.renderMenu()}
            </View>
            <View style={[Styles.flexRow, Styles.textWidth]}>
              <Text
                ellipsizeMode="tail"
                numberOfLines={3}
                style={{ fontFamily: 'gt-walsheim-medium', fontSize: 21 }}
              >
                {post.title}
              </Text>
            </View>
            <View style={[Styles.flexRow, Styles.textWidth]} />
            <View style={[Styles.flexRow, Styles.textWidth]}>
              <Text
                ellipsizeMode="tail"
                numberOfLines={3}
                style={{ top: 10, fontFamily: 'gt-walsheim-regular', fontSize: 15 }}
              >
                {post.description}
              </Text>
              {post.picture && (
              <View
                style={{
                  top: 10,
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    borderRadius: 8,
                    height: 350,
                    width: 400,
                    backgroundColor: '#F9F9F9',
                  }}
                >
                  <Image
                    style={{ width: 400, height: 350, resizeMode: 'contain' }}
                    source={{ uri: post.picture }}
                  />
                </View>
              </View>
              )}
            </View>
            <View style={{ top: 15, flexDirection: 'row' }}>
              <TouchableOpacity
                style={Styles.inline}
                onPress={() => {
                  this.toggleHearts(post)
                }}
              >
                <Image source={uri} style={Styles.iconHeart} />
                <Text
                  style={[
                    Styles.textRegular,
                    Styles.counter,
                    !!hearts && hearts.includes(this.props.user.id)
                      ? Styles.colorPink
                      : Styles.colorGrayIcons,
                  ]}
                >
                  {hearts.length || ''}
                </Text>
              </TouchableOpacity>
              <Text style={[Styles.textRegular, Styles.colorGrayIcons]}>
                {post.commentsCount || 0}
                {' '}
Cards
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    })
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible })
  }

  render() {
    const { modal, unread, schools } = this.state
    const id = this.props.user.school_id
    return (
      <View style={Styles.container}>
        <View style={Styles.modalView}>
          <Modal
            style={{ backgroundColor: 'rgba(0,0,0,0.8)', marginLeft: 20 }}
            animationType="slide"
            transparent
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.')
            }}
          >
            <View
              style={{
                backgroundColor: 'white',
                marginTop: windowHeight * 0.24,
                marginLeft: 30,
                marginRight: 30,
                borderRadius: 12,
                height: 150,
                bottom: 110,
                borderColor: '#D8D8D8',
                borderWidth: 2,
              }}
            >
              <View style={{ padding: 12 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false })
                    this.setModalVisible(!this.state.modalVisible)
                  }}
                >
                  <Text
                    style={{
                      textAlign: 'right',
                      fontSize: 16,
                      opacity: 0.7,
                      fontFamily: 'gt-walsheim-regular',
                    }}
                  >
                    Cancel
                  </Text>
                </TouchableOpacity>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingLeft: '20%',
                    paddingRight: '20%',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ modalVisible: false })
                      this.setModalVisible(!this.state.modalVisible)
                      this.props.navigation.navigate('CreatePost', { type: 'Image Post' })
                    }}
                  >
                      style=
                    {{
                      textAlign: 'right',
                      fontSize: 18,
                      opacity: 0.7,
                      fontFamily: 'gt-walsheim-medium',
                      alignSelf: 'center',
                      paddingBottom: 8,
                    }}
>
                    <Text>
                      Image
                    </Text>
                    <Image
                      source={require('../../assets/images/image_create_icon.png')}
                      style={{ height: 50, width: 50 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ modalVisible: false })
                      this.setModalVisible(!this.state.modalVisible)
                      this.props.navigation.navigate('CreatePost', { type: 'Text Post' })
                    }}
                  >
                    <Text
                      style={{
                        textAlign: 'right',
                        fontSize: 18,
                        opacity: 0.7,
                        fontFamily: 'gt-walsheim-medium',
                        alignSelf: 'center',
                        paddingBottom: 8,
                      }}
                    >
                      Text
                    </Text>

                    <Image
                      source={require('../../assets/images/text_create_icon.png')}
                      style={{ height: 52, width: 52 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <View style={[Styles.headerHome]}>
            <View style={Styles.homeTitle}>
              <Text style={Styles.dateToday}>{moment().format('dddd, MMM Do')}</Text>
            </View>
            <TouchableOpacity
              style={[BaseStyles.menuButtonPadding, { marginRight: 20 }]}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible)
              }}
            >
              <Image
                source={require('../../assets/images/footer-checkin.png')}
                style={BaseStyles.iconFooterCheckIn}
              />
            </TouchableOpacity>
          </View>
          <ScrollView style={Styles.homeList} onScroll={this._onScroll}>
            {this.props.loading && <ActivityIndicator size="large" />}
            <View style={Styles.marginView}>{this.renderPosts()}</View>
            {modal && (
              <CheckInReport
                onClose={() => {
                  this.setState({ modal: false })
                }}
              />
            )}
          </ScrollView>
          <Footer navigation={this.props.navigation} isHome="true" />
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  loading: state.checkIn.loading,
  posts: state.checkIn.posts,
  members: state.checkIn.members,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenMembers,
  listenPosts,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Feed)
