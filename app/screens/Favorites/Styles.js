const Styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flex: 0.5,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  containerKeyboard: {
    justifyContent: 'flex-start',
    paddingTop: 15,
  },
  modalView: {
    width: '90%',
    height: '95%',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingLeft: '2.5%',
    paddingRight: '2.5%',
  },
  width: {
    width: '100%',
  },
  flexView2: {
    flex: 5,
    paddingTop: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '100%',
  },
  flexRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  favoritesHeader: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    marginBottom: '4%',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  paddingRow: {
    paddingRight: 5,
    paddingLeft: 5,
  },
  checked: {
    marginTop: 9,
  },
  cancel: {
    marginTop: 8,
  },
  center: {
    textAlign: 'center',
  },
  heading: {
    fontSize: 32,
    fontFamily: 'gt-walsheim-bold',
    color: '#343434',
  },
  textInput: {
    height: 25,
    width: '100%',
    paddingLeft: 8,
    paddingRight: 8,
    borderBottomWidth: 0,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
    color: '#858585',
  },
  search: {
    backgroundColor: '#F3F3F3',
    paddingLeft: 8,
    paddingTop: 12,
    paddingBottom: 8,
    paddingRight: 8,
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 6,
    marginTop: 5,
    marginBottom: 8,
  },
  selected: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: '3%',
  },
  chip: {
    flexDirection: 'row',
    borderRadius: 12,
    height: 34,
    paddingTop: 5,
    paddingLeft: 10,
    paddingRight: 5,
    marginBottom: 6,
    marginRight: 6,
    backgroundColor: '#FC636B',
  },
  chipText: {
    color: 'white',
    fontSize: 15,
    paddingTop: 2,
    paddingRight: 8,
  },
  cancelChip: {
    height: 20,
    width: 20,
    margin: 2,
  },
  circle: {
    height: 25,
    width: 25,
  },
  footer: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 5,
  },
  fontGTRegular: {
    fontFamily: 'gt-walsheim-regular',
  },
  fontGTMedium: {
    fontFamily: 'gt-walsheim-medium',
  },
  fontGTBold: {
    fontFamily: 'gt-walsheim-bold',
  },
  colorBlack: {
    color: '#343434',
  },
  gray: {
    color: '#A8A8A8',
    fontSize: 16,
  },
  add: {
    color: '#F1636E',
    fontSize: 16,
  },
  addOpacity: {
    opacity: 0.6,
    color: '#F1636E',
    fontFamily: 'gt-walsheim-bold',
    color: '#343434',
    fontSize: 14,
    marginTop: 4,
  },
  name: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 17,
    color: '#575757',
  },
  email: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 12,
    color: '#9B9B9B',
  },
  title: {
    fontSize: 18,
    color: '#343434',
  },
  description: {
    color: '#9B9B9B',
    letterSpacing: 0.18,
  },
}

export default Styles
