import React from 'react'
import {
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Dimensions,
  Alert,
  FlatList,
  Image,
  ActivityIndicator,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { LinearGradient } from 'expo'
import { NavigationActions, StackActions } from 'react-navigation'
import filter from 'lodash/filter'
import keys from 'lodash/keys'
import findIndex from 'lodash/findIndex'

import Styles from './Styles'
import GradientButton from '../../components/Buttons/GradientButton'
import { getMembers, updateProfile } from '../../configs/Firebase'

class Favorites extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      members: [],
      allMembers: [],
      loading: false,
      search: '',
    }

    this._renderList = this.renderList.bind(this)
    this._search = this.search.bind(this)
    this._updateFavorites = this.updateFavorites.bind(this)
    this._skip = this.skip.bind(this)
  }

  componentWillMount() {
    this.getMembers()
  }

  search(text) {
    const { allMembers } = this.state
    const searchedMembers = allMembers.filter((elem) => {
      const inFirst = elem.first_name.toLowerCase().indexOf(text.toLowerCase()) > -1
      const inLast = elem.last_name.toLowerCase().indexOf(text.toLowerCase()) > -1
      const inEmail = elem.email.toLowerCase().indexOf(text.toLowerCase()) > -1
      return inFirst || inLast || inEmail
    })
    this.setState({ members: text.length ? searchedMembers : allMembers, search: text })
  }

  async getMembers() {
    this.setLoading(true)
    try {
      let members = await getMembers(this.props.user)
      const favorites = keys(this.props.user.favorites)
      if (this.props.isChild) {
        members = members.filter(member => favorites.includes(member.id))
      } else {
        members = members.filter((member) => {
          if (favorites.includes(member.id)) {
            member.checked = true
          }
          return member.id != this.props.user.id
        })
      }
      this.setState({ members, allMembers: members })
    } catch (e) {
      Alert.alert(e.error)
    } finally {
      this.setLoading(false)
    }
  }

  setLoading(bool) {
    this.setState({ loading: bool })
  }

  check(item) {
    const { members } = this.state
    const index = findIndex(members, { email: item.email })
    members[index].checked = !members[index].checked
    this.setState({ members })
  }

  async removeFavorite(item) {
    try {
      const { favorites } = this.props.user
      delete favorites[item.id]
      const res = await updateProfile(this.props.user.id, { favorites })
      this.getMembers()
      console.log('res from removeFavorite', res)
    } catch (e) {
      console.log('e from removeFavorite', e)
    }
  }

  async updateFavorites(favorites) {
    const favoriteUsers = {}
    favorites.map((elem) => {
      favoriteUsers[elem.id] = true
    })
    try {
      await updateProfile(this.props.user.id, { favorites: favoriteUsers })
      if (this.props.user.viewedOnboarding) {
        this.props.navigation.state.params.onGoBack()
        this.props.navigation.goBack()
      } else {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })],
        })
        this.props.navigation.dispatch(resetAction)
      }
    } catch (e) {
      Alert.alert('Error', e.message)
    } finally {
      updateProfile(this.props.user.id, { isLogged: true })
    }
  }

  skip() {
    updateProfile(this.props.user.id, { isLogged: true })
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Home' })],
    })
    this.props.navigation.dispatch(resetAction)
  }

  renderFavorites(favorites) {
    return (
      <View style={Styles.selected}>
        {favorites.map(member => (
          <View style={Styles.chip}>
            <Text style={[Styles.fontGTRegular, Styles.chipText]}>
              {member.first_name}
              {' '}
              {member.last_name}
            </Text>
            <TouchableOpacity onPress={() => this.check(member)}>
              <Image source={require('../../assets/images/close.png')} style={Styles.cancelChip} />
            </TouchableOpacity>
          </View>
        ))}
      </View>
    )
  }

  renderList({ item }) {
    return (
      <View style={Styles.borderBottom}>
        <TouchableOpacity
          style={[Styles.flexRow, Styles.paddingRow]}
          onPress={() => (this.props.isChild ? this.removeFavorite(item) : this.check(item))}
        >
          <View>
            <Text style={[Styles.name]}>
              {item.first_name}
              {' '}
              {item.last_name}
            </Text>
            <Text style={[Styles.email]}>{item.email}</Text>
          </View>
          <View style={Styles.checked}>
            {this.props.isChild ? (
              <Image source={require('../../assets/images/cancel.png')} style={Styles.cancel} />
            ) : item.checked ? (
              <Image source={require('../../assets/images/checked.png')} style={Styles.circle} />
            ) : (
              <Image source={require('../../assets/images/empty.png')} style={Styles.circle} />
            )}
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  renderHeader(favorites) {
    return (
      <View style={Styles.header}>
        <View>
          {this.props.user.viewedOnboarding && (
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Text style={[Styles.gray, Styles.fontGTMedium]}>Back</Text>
            </TouchableOpacity>
          )}
        </View>
        <View style={{ flexDirection: 'row' }}>
          {!this.props.user.viewedOnboarding && (
            <TouchableOpacity onPress={this._skip}>
              <Text style={[Styles.gray, Styles.fontGTMedium]}>Skip </Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            disabled={!favorites.length}
            onPress={() => this._updateFavorites(favorites)}
          >
            <Text style={[Styles.add, Styles.fontGTBold]}>
              {' '}
Add (
              {favorites.length}
)
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const {
      allMembers, members, loading, search,
    } = this.state
    const favorites = filter(allMembers, { checked: true })

    return (
      <View style={Styles.container}>
        {loading && <ActivityIndicator size="large" />}
        <View
          style={
            this.props.isChild
              ? [Styles.modalView, { paddingLeft: 0, paddingRight: 0, width: '100%' }]
              : Styles.modalView
          }
        >
          {!this.props.isChild && this.renderHeader(favorites)}
          <View style={Styles.width}>
            {this.props.isChild ? (
              <View style={Styles.favoritesHeader}>
                <Text style={[Styles.fontGTBold, Styles.title]}>
                  Favorites
                  {' '}
                  <Text style={[Styles.fontGTRegular]}>
(
                    {members.length}
)
                  </Text>
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Favorites', {
                      onGoBack: () => this.getMembers(),
                    })
                  }}
                >
                  <Text style={[Styles.addOpacity]}>Add</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <Text style={[Styles.heading]}>Favorites</Text>
            )}
            {!this.props.isChild && (
              <Text style={[Styles.fontGTRegular, Styles.description]}>
                Add the important people in your life you want to check in with. You can always add
                more later.
              </Text>
            )}
          </View>
          <View style={Styles.flexView2}>
            <View style={[Styles.search]}>
              <Image source={require('../../assets/images/search.png')} />
              {!this.props.isChild && (
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Search"
                  underlineColorAndroid="transparent"
                  style={Styles.textInput}
                  onChangeText={this._search}
                  value={search}
                />
              )}
            </View>
            <ScrollView>
              {!!favorites.length && this.renderFavorites(favorites)}
              <FlatList extraData={this.state} data={members} renderItem={this._renderList} />
            </ScrollView>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favorites)
