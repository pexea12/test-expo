import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

class GridSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { skillValue, skillText } = this.props
    return (
      <TouchableOpacity style={styles.btn} onPress={() => this.props.onPressCallBack(skillValue)}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.buttonText}>{skillText}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = {
  btn: {
    width: 280,
    borderRadius: 35,
    height: 50,
    marginTop: 20,
    backgroundColor: 'white',
  },
  buttonText: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 16,
    color: '#24204B',
    textAlign: 'center',
    justifyContent: 'center',
  },
}

export default GridSelect
