import React from 'react'
import { Text, TouchableOpacity, Image } from 'react-native'
import { Permissions } from 'expo'

class MicrophoneBtn extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.handleOnPress = this.handleOnPress.bind(this)
  }

  async handleOnPress() {
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)
    this.props.onPressCallBack(status)
  }

  render() {
    return (
      <TouchableOpacity style={styles.touchableHighlight} onPress={this.handleOnPress}>
        <Text style={styles.nextText}>TURN ON</Text>
        <Image source={require('../../assets/images/mic.png')} style={styles.mic} />
      </TouchableOpacity>
    )
  }
}

const styles = {
  mic: {
    width: 23,
    height: 32,
    left: 7,
  },
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  touchableHighlight: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowRadius: 20,
    shadowColor: 'white',
    shadowOpacity: 0.75,
    width: 200,
    margin: 15,
    borderRadius: 25,
    padding: 15,
  },
  nextText: {
    color: '#55599B',
    fontFamily: 'gt-walsheim-bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: 2.0,
    fontSize: 15,
  },
}

export default MicrophoneBtn
