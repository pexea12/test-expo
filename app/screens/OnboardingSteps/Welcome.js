import React from 'react'
import {
  Text, View, TouchableOpacity, ImageBackground,
} from 'react-native'

class Welcome extends React.Component {
  constructor(props) {
    super(props)
    this.handleOnPress = this.handleOnPress.bind(this)
  }

  handleOnPress() {
    this.props.parentNavigation.navigate('login')
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('../../assets/images/welcomeBG.png')}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={{ justifyContent: 'space-around', flex: 1 }}>
            <View style={styles.topBar}>
              <TouchableOpacity onPress={this.handleOnPress}>
                <Text style={{ color: 'white' }}>Login</Text>
              </TouchableOpacity>
            </View>
            <View>
              <Text style={styles.welcomeText}>RESILIENCE</Text>
              <Text style={styles.welcomeText}>GRATITUDE</Text>
              <Text style={styles.welcomeText}>FOCUS</Text>
              <Text style={styles.welcomeText}>Welcome to Atlas</Text>
            </View>
            <View />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#27204E',
  },
  welcomeText: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 28,
    letterSpacing: 11.4,
    top: 25,
    color: 'white',
    textAlign: 'center',
  },
  topBar: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    height: 50,
  },
}

export default Welcome
