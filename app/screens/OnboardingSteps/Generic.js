import React from 'react'
import {
  Text, View, Image, ImageBackground, Animated, TouchableOpacity,
} from 'react-native'
import posed from 'react-native-pose'
import { segmentAnalyzePage } from '../../configs/SegmentAnalytics'

const slowerTween2 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 200,
  delay: 350,
})

const captionTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1000,
  delay: 380,
})

const buttonTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1500,
  delay: 390,
})

const titleConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: slowerTween2,
  },
  exit: {
    scale: 1.0,
    opacity: 0.4,
    transition: slowerTween2,
  },
}

const captionConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: captionTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0,
    transition: captionTween,
  },
}

const buttonConfig = {
  enter: {
    opacity: 1,
    scale: 1.0,
    transition: buttonTween,
  },
  exit: {
    scale: 1.0,
    opacity: 0,
    transition: buttonTween,
  },
}

const FadeOutTitle = posed.View(titleConfig)
const FadeOutCaption = posed.View(captionConfig)
const FadeOutButton = posed.View(buttonConfig)

class Generic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      startAnimation: false,
    }
    this.handleOnPress = this.handleOnPress.bind(this)
  }

  componentWillMount() {
    this.setState({ startAnimation: !this.state.startAnimation })
  }

  componentDidMount() {
    segmentAnalyzePage('anon', 'Onboarding Content')
  }

  handleOnPress() {
    this.props.parentNavigation.navigate('FirstScreen')
  }

  render() {
    const uri = this.props.imageURL
      ? this.props.imageURL
      : require('../../assets/images/welcomeBG.png')

    return (
      <View style={styles.container}>
        <ImageBackground source={uri} style={{ width: '100%', height: '100%' }}>
          <View style={{ justifyContent: 'space-around', flex: 1 }}>
            <View>
              <View style={{ top: -40 }}>
                <TouchableOpacity
                  onPress={() => this.props.parentNavigation.navigate('FirstScreen')}
                >
                  <Text
                    style={{
                      color: 'white',
                      left: 20,
                      fontFamily: 'gt-walsheim-regular',
                      opacity: 0.9,
                    }}
                  >
                    Quit
                  </Text>
                </TouchableOpacity>
                <View style={styles.textArea}>
                  <FadeOutTitle pose={this.state.startAnimation ? 'enter' : 'exit'}>
                    <Text style={styles.titleText}>{this.props.title}</Text>
                  </FadeOutTitle>
                  <FadeOutCaption pose={this.state.startAnimation ? 'enter' : 'exit'}>
                    <Text style={styles.descriptionText}>{this.props.description}</Text>
                  </FadeOutCaption>
                </View>
              </View>
            </View>
            <View />
            <View />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#27204E',
  },
  textArea: {
    top: 50,
    paddingLeft: 25,
    paddingRight: 25,
  },
  titleText: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 25,
    letterSpacing: 7.5,
    top: 25,
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  topBar: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: 50,
  },
  descriptionText: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 20,
    lineHeight: 28,
    top: 45,
    color: 'white',
    textAlign: 'center',
  },
}

export default Generic
