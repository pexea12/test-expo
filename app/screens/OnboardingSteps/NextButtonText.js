import React from 'react'
import { Text } from 'react-native'
import { Permissions } from 'expo'

class NextButtonText extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return <Text>{this.props.text}</Text>
  }
}

const styles = {
  btn: {
    color: '#55599B',
    fontFamily: 'gt-walsheim-medium',
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: 2.0,
    fontSize: 17,
  },
}

export default NextButtonText
