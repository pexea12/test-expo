import React from 'react'
import { TextInput, View } from 'react-native'

class Form extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: null,
      lastName: null,
      schoolEmail: null,
      password: null,
    }

    this.onChangeText = this.onChangeText.bind(this)
  }

  onChangeText(name, value) {
    this.setState({ [name]: value })
    this.props.onChangeCallback(this.state)
  }

  render() {
    const {
      firstName, lastName, schoolEmail, password,
    } = this.state
    return (
      <View style={styles.container}>
        <ImageBackground source={uri} style={{ width: '100%', height: '100%' }}>
          <View>
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="First Name"
              placeholderTextColor="rgba(255,255,255,0.5)"
              maxlength={200}
              onChangeText={value => this.onChangeText('firstName', value)}
              value={firstName}
            />
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="Last Name"
              placeholderTextColor="rgba(255,255,255,0.5)"
              maxlength={200}
              onChangeText={value => this.onChangeText('lastName', value)}
              value={lastName}
            />
            <TextInput
              underlineColorAndroid="transparent"
              keyboardType="email-address"
              placeholder="Email"
              placeholderTextColor="rgba(255,255,255,0.5)"
              maxlength={200}
              autoCapitalize="none"
              onChangeText={value => this.onChangeText('schoolEmail', value)}
              value={schoolEmail}
            />
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="Password (8+ characters)"
              placeholderTextColor="rgba(255,255,255,0.5)"
              secureTextEntry
              maxlength={16}
              onChangeText={value => this.onChangeText('password', value)}
              value={password}
            />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#B05E95',
  },
}

export default Form
