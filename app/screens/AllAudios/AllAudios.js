import React from 'react'
import {
  ImageBackground,
  ScrollView,
  FlatList,
  Text,
  Dimensions,
  View,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'

import { LinearGradient, Audio, Permissions } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import posed from 'react-native-pose'
import ProgressCircle from 'react-native-progress-circle'
import Communications from 'react-native-communications'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { generateId } from '../../configs/Helpers'
import CardPreviewModal from '../CardPreviewModal/CardPreviewModal'
import { listenMembers, updateMyRecording, updatePendingRecordings } from '../../redux/checkIn/actions'
import moment from '../../configs/Moment'
import Styles from './Style'
import { postRecordings, addCompletedCardId } from '../../configs/Firebase'
import Header from '../../components/Header/Header'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const cardIds = [
  'intro',
  'XvqPXFbiuDL4rUzvqIkA',
  'dfhr1tRAutGyo3XX2f2h',
  'nSAOXtp35iyHKcdOe3a7',
  'd4HcYXYL1wT6qggYa1Dz',
]
const iPhoneXPadding = (windowHeight > 800) * 20
const isiPhoneSE = windowHeight < 600

class CardPreview extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {
      loadingAudio: false,
      index: 0,
      playing: false,
      allAudios: [],
      card: props.navigation.state.params.card,
      dontSave: props.navigation.state.params.dontSave,
      saveLoading: false,
      meaningfulValue: props.navigation.state.params.meaningfulValue,
      textFeedback: props.navigation.state.params.textFeedback,
    }

    this.loadAudio = this.loadAudio.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this.submit = this.submit.bind(this)
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)

    if (status !== 'granted') {
      alert('Hey! You have not enabled selected permissions')
      this.props.navigation.goBack()
      return
    }
    this.loadAudio()
  }

  componentWillUnmount() {
    this.unload()
  }

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000)
    const seconds = ((millis % 60000) / 1000).toFixed(0)
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  async loadAudio() {
    const { card } = this.state
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })
    Audio.setIsEnabledAsync(true)
    card.audios.map(async (audioObj, index) => {
      const audio = new Audio.Sound()

      this.setState({ loadingAudio: true })

      try {
        await audio.loadAsync({ uri: audioObj.recording })
        audio.setOnPlaybackStatusUpdate((status) => {
          const totalDuration = this.millisToMinutesAndSeconds(status.durationMillis)
          const currentPosition = this.millisToMinutesAndSeconds(status.durationMillis - status.positionMillis)
          const percent = status.positionMillis / status.durationMillis * 100
          const { allAudios } = this.state
          const allSounds = [...allAudios]
          allSounds[index] = {
            ...status, audio, loadingAudio: false, currentPosition, totalDuration, percent,
          }
          this.setState({ allAudios: allSounds })
        })
        audio.setProgressUpdateIntervalAsync(1000)
      } catch (error) {
        // An error occurred!
        // alert('error' + JSON.stringify(error))
      }
    })
  }

  async play(index) {
    const { allAudios } = this.state

    allAudios.map(async (obj) => {
      await obj.audio.stopAsync()
    })
    if (allAudios[index].percent == 100) {
      allAudios[index].audio.replayAsync()
    } else {
      allAudios[index].audio.setPositionAsync(allAudios[index].positionMillis)
      allAudios[index].audio.playAsync()
    }
  }

  async pause(index) {
    const { allAudios } = this.state

    await allAudios[index].audio.pauseAsync()
  }

  async unload() {
    const { allAudios } = this.state

    allAudios.map(audioObj => audioObj.audio && audioObj.audio.unloadAsync())
  }

  complete = () => {
    this.submit()
    Alert.alert("You've unlocked more episodes! 🎙️", 'Your recording is being saved and will appear here in just a moment.')
    console.log(this.state.pendingRecordings)
    this.props.navigation.navigate('Recordings')
  }

  async submit() {
    let { pendingRecordings = [], updatePendingRecordings } = this.props
    const { card, textFeedback, meaningfulValue } = this.state

    // uploadCompletedIndex
    addCompletedCardId(card)

    const newCard = { ...card }
    newCard.audios.forEach((a, index) => {
      newCard.audios[index].downloadedUri = a.recording
    })

    const uniqueId = generateId()
    newCard.firestoreKey = uniqueId
    newCard.textFeedback = textFeedback
    newCard.meaningfulValue = meaningfulValue

    pendingRecordings.push(newCard)
    updatePendingRecordings(pendingRecordings)

    this.setState({ saveLoading: true })
    try {
      const featuredIndex = cardIds.indexOf(card.id)
      const response = await postRecordings(newCard, textFeedback, meaningfulValue, featuredIndex)
      pendingRecordings = pendingRecordings.filter(recording => recording.firestoreKey !== response)
      updatePendingRecordings(pendingRecordings)
      this.setState({ saveLoading: false })
    } catch (e) {
      this.setState({ saveLoading: false })
      Alert.alert('Something went wrong', "We're unable to save your audios at this moment")
    }
  }

  renderPlayer({ loadingAudio, percent, isPlaying }, index) {
    return (
      <TouchableOpacity
        onPress={isPlaying ? this.pause.bind(this, index) : this.play.bind(this, index)}
      >
        <ProgressCircle
          percent={percent}
          radius={20}
          borderWidth={1}
          color="#ef9a9f"
          shadowColor="#b073b1"
          bgColor="#a9529c"
        >
          {!loadingAudio ? (
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {isPlaying ? (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/pause-button.png')}
                />
              ) : (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/play-button.png')}
                />
              )}
            </View>
          ) : (
            <ActivityIndicator size="large" />
          )}
        </ProgressCircle>
      </TouchableOpacity>
    )
  }

  renderAudios() {
    const { allAudios } = this.state

    return allAudios.length ? allAudios.map((audio, index) => (audio ? (
      <View style={{
        flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', padding: '7%',
      }}
      >
        <Text style={[Styles.cardTitle, {
          fontSize: 14, fontFamily: 'gt-walsheim-bold', letterSpacing: 3.33, marginBottom: 0,
        }, audio.isPlaying && { color: '#ef9a9f' }]}
        >
          {`PART ${index + 1}`}
        </Text>
        {this.renderPlayer(audio, index)}
        <Text style={[{ color: 'white', alignSelf: 'center', fontFamily: 'gt-walsheim-bold' }, audio.isPlaying && { color: '#ef9a9f' }]}>{audio.percent > 0 ? audio.currentPosition : audio.totalDuration}</Text>
      </View>
    ) : <ActivityIndicator size="large" />)) : <ActivityIndicator size="large" />
  }

  getHeightMargin = () => {
    if (windowHeight >= 800) return -windowHeight * 0.15
    if (windowHeight < 600) return -windowHeight * 0.45
    return -windowHeight * 0.20
  }


  renderSaveButton() {
    const { saveLoading } = this.state

    return !saveLoading ? (
      <TouchableOpacity onPress={this.complete}>
        <View
          style={[
            {
              width: 80,
              height: 40,
              borderRadius: 10,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}
        >
          <Text
            style={{
              fontFamily: 'gt-walsheim-bold',
              color: '#3b1c3f',
              fontSize: 14,
              marginBottom: 0,
            }}
          >
            Finish
          </Text>
        </View>
      </TouchableOpacity>
    )
      : (
        <ActivityIndicator size="large" />
      )
  }

  render() {
    const { card, dontSave } = this.state
    const heightMargin = this.getHeightMargin()
    return (
      <View
        style={{
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20,
          paddingBottom: 20,
          justifyContent: 'center',
          backgroundColor: 'rgba(0,0,0,0.85)',
        }}
      >
        <LinearGradient
          style={{
            position: 'absolute',
            width: windowWidth,
            height: windowHeight * 1.1,
            top: -iPhoneXPadding * 1.1,
          }}
          colors={['#6f1a99', '#862298', '#a93097', '#ec939d']}
        />
        <Image
          resizeMode="contain"
          source={require('../../assets/images/allAudios_mountain.png')}
          style={{
            position: 'absolute',
            top: heightMargin,
            width: windowWidth * 1.08,
            opacity: 0.9,
          }}
        />
        <View style={{ flex: 5, top: iPhoneXPadding * 1.1 + 25 }}>
          <View style={{ flex: 0.7, flexDirection: 'row', justifyContent: 'flex-start' }}>
            <View style={{ flex: 0.3 }} />
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  alignSelf: 'center',
                  textAlign: 'center',
                  letterSpacing: 1.8,
                  textTransform: 'uppercase',
                  justifySelf: 'center',
                  color: 'white',
                  fontFamily: 'gt-walsheim-bold',
                  opacity: 0.7,
                  fontSize: 14,
                }}
              >
                {card.title}
              </Text>
            </View>
            <View style={{ flex: 0.3 }} />
          </View>
        </View>
        <View
          style={{
            flex: 9 + isiPhoneSE * 3,
            top: -15 * isiPhoneSE,
            alignItems: 'center',
            justifyContent: 'space-around',
          }}
        >
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-end',
              paddingBottom: iPhoneXPadding,
            }}
          >
            <Text style={[Styles.heading, { fontSize: 27, color: 'white', marginTop: -2 }]}>
              {card.endingScreen.header}
            </Text>
            <Text
              style={{
                top: 15,
                lineHeight: 24,
                fontFamily: 'gt-walsheim-regular',
                textAlign: 'center',
                fontSize: 18,
                opacity: 0.9,
                color: 'white',
              }}
            >
              {card.endingScreen.subheader}
            </Text>
          </View>
          <View
            style={{
              width: windowWidth * 0.9,
              top: 15,
              paddingBottom: 15,
              justifyContent: 'center',
              borderWidth: 0.5,
              borderColor: 'white',
              backgroundColor: 'rgba(216, 216, 216, 0.15)',
              borderRadius: 10,
            }}
          >
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {this.renderAudios()}
            </View>
          </View>
          <View style={{ zIndex: 1000, top: 12, justifyContent: 'center' }}>
            {!dontSave && this.renderSaveButton()}
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  myRecordings: state.checkIn.myRecordings,
  pendingRecordings: state.checkIn.pendingRecordings,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  updateMyRecording,
  updatePendingRecordings,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardPreview)
