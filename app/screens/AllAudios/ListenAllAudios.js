import React from 'react'
import {
  ImageBackground,
  ScrollView,
  FlatList,
  Text,
  Dimensions,
  View,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LinearGradient, Audio, Permissions } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import posed from 'react-native-pose'
import ProgressCircle from 'react-native-progress-circle'
import Communications from 'react-native-communications'
import CardPreviewModal from '../CardPreviewModal/CardPreviewModal'
import { listenMembers } from '../../redux/checkIn/actions'
import Header from '../../components/Header/Header'
import moment from '../../configs/Moment'
import { postRecordings } from '../../configs/Firebase'
import Styles from './Style'
import { segmentAnalyzePage, segmentAnalyzeEvent } from '../../configs/SegmentAnalytics'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const iPhoneXPadding = (windowHeight > 800) * 20

class CardPreview extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  constructor(props) {
    super(props)
    this.state = {
      loadingAudio: false,
      index: 0,
      playing: false,
      allAudios: [],
      card: props.navigation.state.params.card,
      dontSave: props.navigation.state.params.dontSave,
      saveLoading: false,
      meaningfulValue: props.navigation.state.params.meaningfulValue,
      textFeedback: props.navigation.state.params.textFeedback,
    }

    this.loadAudio = this.loadAudio.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentWillMount() {
    const { card } = this.state
    if (card.isDaily) {
      segmentAnalyzePage(this.props.user, 'Relisten Daily', { card: card.title })
    } else {
      segmentAnalyzePage(this.props.user, 'Relisten Episode', { card: card.title })
    }
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING)

    if (status !== 'granted') {
      alert('Hey! You have not enabled selected permissions')
      this.props.navigation.goBack()
      return
    }
    this.loadAudio()
  }

  componentWillUnmount() {
    this.unload()
  }

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000)
    const seconds = ((millis % 60000) / 1000).toFixed(0)
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  async loadAudio() {
    const { card } = this.state
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
    })
    Audio.setIsEnabledAsync(true)
    card.audios.map(async (audioObj, index) => {
      const audio = new Audio.Sound()

      this.setState({ loadingAudio: true })

      try {
        await audio.loadAsync({ uri: audioObj.downloadedUri || audioObj.recording })
        audio.setOnPlaybackStatusUpdate((status) => {
          const totalDuration = this.millisToMinutesAndSeconds(status.durationMillis)
          const currentPosition = this.millisToMinutesAndSeconds(status.durationMillis - status.positionMillis)
          const percent = status.positionMillis / status.durationMillis * 100
          const { allAudios } = this.state
          const allSounds = [...allAudios]
          allSounds[index] = {
            ...status, audio, loadingAudio: false, currentPosition, totalDuration, percent,
          }
          this.setState({ allAudios: allSounds })
        })

        audio.setProgressUpdateIntervalAsync(1000)
      } catch (error) {
        console.log('Error while loading audio =>', error)
        // An error occurred!
        // alert('error' + JSON.stringify(error))
      }
    })
  }

  async play(index) {
    const { allAudios, card } = this.state
    if (card.isDaily) {
      segmentAnalyzeEvent('Daily Replayed', { card: card.title })
    } else {
      segmentAnalyzeEvent(`Ep Part ${index + 1} Replayed`, { card: card.title })
    }
    allAudios.map(async (obj) => {
      await obj.audio.stopAsync()
    })
    if (allAudios[index].percent == 100) {
      allAudios[index].audio.replayAsync()
    } else {
      allAudios[index].audio.setPositionAsync(allAudios[index].positionMillis)
      allAudios[index].audio.playAsync()
    }
  }

  async pause(index) {
    const { allAudios } = this.state

    await allAudios[index].audio.pauseAsync()
  }

  async unload() {
    const { allAudios } = this.state

    allAudios.map(audioObj => audioObj.audio && audioObj.audio.unloadAsync())
  }

  async submit() {
    const { card, textFeedback, meaningfulValue } = this.state
    this.setState({ saveLoading: true })
    try {
      await postRecordings(card, textFeedback, meaningfulValue)
      this.setState({ saveLoading: false })
      this.props.navigation.navigate('Recordings')
    } catch (e) {
      this.setState({ saveLoading: false })
      Alert.alert('Error', 'Something went wrong!')
    }
  }

  renderPlayer({ loadingAudio, percent, isPlaying }, index) {
    return (
      <TouchableOpacity
        onPress={isPlaying ? this.pause.bind(this, index) : this.play.bind(this, index)}
      >
        <ProgressCircle
          percent={percent}
          radius={20}
          borderWidth={1}
          color="#ef9a9f"
          shadowColor="#b073b1"
          bgColor="#a9529c"
        >
          {!loadingAudio ? (
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {isPlaying ? (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/pause-button.png')}
                />
              ) : (
                <Image
                  style={{ width: 15, height: 15 }}
                  source={require('../../assets/images/play-button.png')}
                />
              )}
            </View>
          ) : (
            <ActivityIndicator size="large" />
          )}
        </ProgressCircle>
      </TouchableOpacity>
    )
  }

  renderAudios() {
    const { allAudios } = this.state

    const audioComponents = allAudios.map((audio, index) => (!audio
      ? <ActivityIndicator size="large" />
      : (
        <View style={{
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '7%',
        }}
        >
          <Text style={[
            Styles.cardTitle,
            {
              fontSize: 14,
              fontFamily: 'gt-walsheim-bold',
              letterSpacing: 3.33,
              marginBottom: 0,
            },
            audio.isPlaying && { color: '#ef9a9f' },
          ]}
          >
            {`PART ${index + 1}`}
          </Text>
          {this.renderPlayer(audio, index)}
          <Text style={[
            {
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'gt-walsheim-bold',
            },
            audio.isPlaying && { color: '#ef9a9f' },
          ]}
          >
            { audio.percent > 0 ? audio.currentPosition : audio.totalDuration }
          </Text>
        </View>
      )))

    return allAudios.length > 0
      ? audioComponents
      : <ActivityIndicator size="large" />
  }

  getHeightMargin = () => {
    if (windowHeight >= 800) return -windowHeight * 0.320
    if (windowHeight < 600) return -windowHeight * 0.35
    return -windowHeight * 0.40
  }

  render() {
    const { card, dontSave } = this.state
    const heightMargin = this.getHeightMargin()

    return (
      <View
        style={{
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20,
          paddingBottom: 20,
          justifyContent: 'center',
          backgroundColor: 'rgba(0,0,0,0.85)',
        }}
      >
        <LinearGradient
          style={{
            position: 'absolute',
            width: windowWidth,
            height: windowHeight * 1.1,
            top: -iPhoneXPadding * 1.1,
          }}
          colors={['#6f1a99', '#862298', '#a93097', '#ec939d']}
        />
        <Image
          resizeMode="contain"
          source={require('../../assets/images/defaultListenBG.png')}
          style={{
            position: 'absolute', top: 0, width: windowWidth * 1.08, opacity: 1.0,
          }}
        />
        <View style={{ justifyContent: 'flex-start', top: iPhoneXPadding * 1.1 + 25, zIndex: 100 }}>
          <View style={{ flex: 0.7, flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              style={{ flex: 0.3 }}
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Image
                source={require('../../assets/images/xbutton.png')}
                style={{ width: 21, height: 21 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}>
          <Text
            style={{
              alignSelf: 'center',
              textAlign: 'center',
              letterSpacing: 1.8,
              textTransform: 'uppercase',
              justifySelf: 'center',
              color: 'white',
              fontFamily: 'gt-walsheim-bold',
              opacity: 0.7,
              fontSize: 14,
            }}
          >
            {card.title}
          </Text>
          <View style={{ marginTop: 30 }}>
            <Text
              style={{
                lineHeight: 24,
                fontFamily: 'gt-walsheim-regular',
                textAlign: 'center',
                fontSize: 22,
                opacity: 1.0,
                color: 'white',
              }}
            >
              Give your past thoughts a listen
            </Text>
          </View>
          <View style={{ justifyContent: 'center', top: 30 }}>
            <View
              style={{
                width: windowWidth * 0.9,
                justifyContent: 'center',
                borderWidth: 0.5,
                borderColor: 'white',
                backgroundColor: 'rgba(216, 216, 216, 0.15)',
                padding: 15,
                paddingBottom: 50,
                borderRadius: 10,
              }}
            >
              <Text
                style={{
                  alignSelf: 'center',
                  letterSpacing: 2.1,
                  textTransform: 'uppercase',
                  justifySelf: 'center',
                  color: 'white',
                  fontFamily: 'gt-walsheim-medium',
                  opacity: 0.9,
                  fontSize: 15,
                }}
              >
                YOUR THOUGHTS
              </Text>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                {this.renderAudios()}
              </View>
              <View style={{ top: 28 }}>
                <Text
                  style={{
                    fontFamily: 'gt-walsheim-regular',
                    opacity: 0.8,
                    fontSize: 14,
                    color: 'white',
                    textAlign: 'center',
                  }}
                >
                  {moment(card.createdAt).format('MMM D, YYYY h:mma')}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardPreview)
