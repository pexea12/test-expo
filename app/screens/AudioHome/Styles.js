import { StyleSheet } from 'react-native'


const Styles = StyleSheet.create({
  title: {
    fontFamily: 'gt-walsheim-medium',
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    letterSpacing: 10,
    lineHeight: 21,
    marginTop: 50,
  },

  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginLeft: 15,
    marginRight: 15,
    marginTop: 30,
  },
})

export default Styles
