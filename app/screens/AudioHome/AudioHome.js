import React from 'react'
import {
  View,
  Text,
  ImageBackground,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import map from 'lodash/map'

import { firebase } from '../../configs/Firebase'
import AudioFooter from '../AudioFooter/AudioFooter'
import { onUserChanged } from '../../redux/auth/actions'
import Loader from '../../components/UI/Loader'
import WelcomeModal from '../Dailies/WelcomeModal'
import Banner from '../DailiesModals/Banner'
import Styles from './Styles'
import {
  listenAudioCards,
  listenMyRecordings,
  updateMyRecording,
  updatePendingRecordings,
  listenCompletedCardIds,
  setLoading,
} from '../../redux/checkIn/actions'
import {
  getMorningAudio,
  getNightAudio,
  updateFetchedDate,
} from '../../redux/dailies/actions'
import moment from '../../configs/Moment'
import { processErr } from '../../configs/Helpers'


class AudioHome extends React.Component {
  static navigationOptions() {
    return {
      header: () => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: 'white',
          }}
        />
      ),
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
      swipeEnabled: false,
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      observer: null,
    }
  }

  componentDidMount() {
    this.onCheckAuth()
    // segmentAnalyzePage(this.props.user, 'Home')

    if (firebase.auth().currentUser) {
      this.initializationTasks()
      this.initData()
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.props.user && !prevProps.user) {
      this.initializationTasks()
      await this.initData()
    }
  }

  onCheckAuth() {
    if (this.state.observer) return

    const observer = firebase.auth().onAuthStateChanged((userData) => {
      const {
        navigation,
        user,
      } = this.props

      if (!userData) {
        navigation.navigate('Login')
      } else {
        this.props.onUserChanged(userData.uid)

        if (!user || (userData.uid !== user.id)) {
          navigation.navigate('AudioHome')
        }
      }
    }, (e) => {
      processErr(e)
    })

    this.setState({ observer })
  }

  getFeaturedIndex() {
    const { completedCardIds } = this.props

    //  TODO: Update User State to viewed onboarding when begin is clicked, else render 1
    if (!completedCardIds || completedCardIds.length === 0) {
      return 0
    }
    if (!completedCardIds || completedCardIds.length === 0) {
      return 1
    }
    const completedIds = map(completedCardIds, 'featuredId')
    const filtered = map(completedIds, n => n || 0)
    const max = filtered.reduce((a, b) => Math.max(a, b))
    return max
  }

  getFeaturedCard() {
    const { cards } = this.props
    if (this.getFeaturedIndex() === 0) {
      return cards[0]
    }
    if (this.getFeaturedIndex() === 0) {
      return cards[1]
    }
    if (this.getFeaturedIndex() === 5) {
      return cards[4]
      // TODO: Congratulations Message
    }
    return cards[this.getFeaturedIndex()]
  }

  goToCard = (card) => {
    this.props.navigation.navigate('AudioCard', { card })
  }

  async initData() {
    const {
      fetchedDate,
    } = this.props

    const beginTime = fetchedDate 
      ? moment(fetchedDate).startOf('day') 
      : moment().startOf('day')
    const endTime = fetchedDate 
      ? moment(fetchedDate).endOf('day') 
      : moment().endOf('day')
      
    const now = moment()
    const inRange = fetchedDate
      && now.isBefore(endTime)
      && now.isAfter(beginTime)

    if (!fetchedDate || !inRange) {
      this.props.setLoading(true)
      try {
        await Promise.all([
          this.props.getMorningAudio(),
          this.props.getNightAudio(),
        ])

        this.props.updateFetchedDate(now.valueOf())
      } catch (e) {
        processErr(e, 'Unable to fetch audios')
      }

      this.props.setLoading(false)
    }
  }

  initializationTasks() {
    console.log('initializationTasks')

    this.props.listenMyRecordings()
    this.props.listenAudioCards()
    this.props.listenCompletedCardIds()
  }

  render() {
    const {
      morningDaily,
      nightDaily,
      navigation,
      completedCardIds,
      showInstruction,
      loading,
    } = this.props

    if (showInstruction) {
      return <WelcomeModal />
    }

    const cardIds = completedCardIds.map(item => item.cardId)

    console.log('completed', cardIds)

    return (
      <View>
        <ImageBackground
          source={require('../../assets/images/modalBG.png')}
          style={{ width: '100%', height: '100%' }}
        >
          <Text style={Styles.title}>
            ATLAS
          </Text>

          {
            loading
              ? <Loader />
              : (
                <View style={Styles.cardContainer}>
                  {morningDaily
                  && (
                  <Banner
                    title={morningDaily.audioTitle}
                    subTitle={morningDaily.audioSubtitle}
                    type="morning"
                    touchable
                    onPress={() => this.goToCard(morningDaily)}
                    completed={cardIds.includes(morningDaily.id)}
                  />
                  )
                }
                  {nightDaily
                  && (
                  <Banner
                    title={nightDaily.audioTitle}
                    type="night"
                    subTitle={nightDaily.audioSubtitle}
                    touchable
                    onPress={() => this.goToCard(nightDaily)}
                    completed={cardIds.includes(nightDaily.id)}
                  />
                  )
                }
                </View>
              )
          }
        </ImageBackground>

        <AudioFooter
          navigation={navigation}
          incomplete
          isHome
        />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  cards: state.checkIn.cards,
  myRecordings: state.checkIn.myRecordings,
  pendingRecordings: state.checkIn.pendingRecordings,
  completedCardIds: state.checkIn.completedCardIds,
  morningDaily: state.dailies.morningDaily,
  nightDaily: state.dailies.nightDaily,
  showInstruction: state.dailies.showInstruction,
  loading: state.checkIn.loading,
  fetchedDate: state.dailies.fetchedDate,
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    listenAudioCards,
    listenMyRecordings,
    onUserChanged,
    updateMyRecording,
    updatePendingRecordings,
    listenCompletedCardIds,
    getMorningAudio,
    getNightAudio,
    setLoading,
    updateFetchedDate,
  },
  dispatch,
)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AudioHome)
