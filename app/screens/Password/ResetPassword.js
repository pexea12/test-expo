import React from 'react'
import {
  Text, 
  View, 
  TextInput, 
  TouchableOpacity, 
  Alert, 
  ActivityIndicator,
} from 'react-native'
import Header from '../../components/Header/Header'
import Styles from './Styles'
import { resetPassword } from '../../configs/Firebase'

class ResetPassword extends React.Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)

    this.state = {
      oldPassword: '',
      newPassword: '',
      retype: '',
      loading: false,
      isFocused: false,
    }

    this._navigate = this.navigate.bind(this)
    this._forgot = this.forgot.bind(this)
  }

  onFocus(index) {
    this.setState({ isFocused: index })
  }

  navigate() {
    this.props.navigation.goBack()
  }

  setLoading(bool) {
    this.setState({ loading: bool })
  }

  validate() {
    const { oldPassword, newPassword, retype } = this.state
    if (!oldPassword || !newPassword || !retype) {
      Alert.alert('Error', 'Enter All Fields.')
      return false
    }
    if (newPassword.length < 8) {
      Alert.alert('Minimum length of password field is 8 characters')
      return false
    }
    if (newPassword.length > 16) {
      Alert.alert('Maximum length of password field is 16 characters')
      return false
    }
    if (newPassword != retype) {
      Alert.alert('Error', 'Passwords do not match.')
      return false
    }
    return true
  }

  async forgot() {
    const { oldPassword, newPassword, retype } = this.state
    console.log(oldPassword, newPassword, retype)
    if (this.validate()) {
      this.setLoading(true)
      try {
        const res = await resetPassword(oldPassword, newPassword)
        Alert.alert('', res)
        this.navigate()
      } catch (e) {
        Alert.alert('Error', e.error || e)
      } finally {
        this.setLoading(false)
      }
    }
  }

  render() {
    const { oldPassword, newPassword, retype } = this.state

    return (
      <View style={Styles.topContainer}>
        <Header onCancel={this._navigate} onSave={this._forgot} title="Reset Password" />
        <View style={Styles.container}>
          <View style={Styles.modalView}>
            <View style={[Styles.form, Styles.formReset]}>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Old Password"
                placeholderTextColor="#D8D8D8"
                secureTextEntry
                maxlength={16}
                style={
                  this.state.isFocused == 0
                    ? [Styles.textInput, Styles.textInputFocus]
                    : [Styles.textInput, Styles.textInputGray]
                }
                onChangeText={oldPassword => this.setState({ oldPassword })}
                onFocus={() => this.onFocus(0)}
                value={oldPassword}
              />
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="New Password"
                placeholderTextColor="#D8D8D8"
                secureTextEntry
                maxlength={16}
                style={
                  this.state.isFocused == 1
                    ? [Styles.textInput, Styles.textInputFocus]
                    : [Styles.textInput, Styles.textInputGray]
                }
                onChangeText={newPassword => this.setState({ newPassword })}
                onFocus={() => this.onFocus(1)}
                value={newPassword}
              />
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Retype New Password"
                placeholderTextColor="#D8D8D8"
                secureTextEntry
                maxlength={16}
                style={
                  this.state.isFocused == 2
                    ? [Styles.textInput, Styles.textInputFocus]
                    : [Styles.textInput, Styles.textInputGray]
                }
                onChangeText={retype => this.setState({ retype })}
                onFocus={() => this.onFocus(2)}
                value={retype}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default ResetPassword
