import React from 'react'
import {
  Text, View, TextInput, TouchableOpacity, Alert, ActivityIndicator,
} from 'react-native'
import GradientButton from '../../components/Buttons/GradientButton'
import Styles from './Styles'
import { forgotPassword } from '../../configs/Firebase'

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      loading: false,
      isFocused: false,
    }

    this._navigate = this.navigate.bind(this)
    this._forgot = this.forgot.bind(this)
  }

  onFocus() {
    this.setState({ isFocused: true })
  }

  navigate() {
    this.props.navigation.goBack()
  }

  setLoading(bool) {
    this.setState({ loading: bool })
  }

  async forgot() {
    const { email, password } = this.state
    if (this.validate()) {
      this.setLoading(true)
      try {
        const res = await forgotPassword(email)
        Alert.alert(res)
        this.navigate()
      } catch (e) {
        Alert.alert('Error', e.error)
      } finally {
        this.setLoading(false)
      }
    }
  }

  validate() {
    const { email } = this.state

    if (!email) {
      Alert.alert('Error', 'Enter Email.')
      return false
    }
    return true
  }

  render() {
    const { email, loading } = this.state

    return (
      <View style={Styles.container}>
        {loading && <ActivityIndicator size="large" />}
        <View style={Styles.modalView}>
          <View style={[Styles.form, Styles.marginTop]}>
            <TextInput
              underlineColorAndroid="transparent"
              keyboardType="email-address"
              placeholderTextColor="#9B9B9B"
              placeholder="Email"
              maxlength={200}
              autoCapitalize="none"
              style={
                this.state.isFocused
                  ? [Styles.textInput, Styles.textInputFocus]
                  : [Styles.textInput, Styles.textInputGray]
              }
              onChangeText={email => this.setState({ email })}
              onFocus={() => this.onFocus()}
              value={email}
            />
            <View style={Styles.button}>
              <TouchableOpacity onPress={this._forgot}>
                <GradientButton text="Submit" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default ForgotPassword
