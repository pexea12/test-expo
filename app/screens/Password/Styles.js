const Styles = {
  topContainer: {
    height: '100%',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerKeyboard: {
    justifyContent: 'flex-start',
    paddingTop: 15,
  },
  modalView: {
    width: '90%',
    height: '90%',
    justifyContent: 'flex-start',
    marginTop: '20%',
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  marginTop: {
    marginTop: '20%',
  },
  form: {
    width: '100%',
  },
  formReset: {
    marginTop: '10%',
  },
  textInput: {
    height: 40,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: 'gt-walsheim-regular',
    fontSize: 16,
    borderBottomWidth: 1,
    marginTop: '5%',
  },
  textInputGray: {
    color: '#9B9B9B',
    borderBottomColor: '#D8D8D8',
  },
  textInputFocus: {
    color: '#F6529D',
    borderBottomColor: '#F6529D',
    fontFamily: 'gt-walsheim-medium',
  },
  button: {
    marginTop: '6%',
  },
  fontRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
  },
}

export default Styles
