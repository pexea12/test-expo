const Styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    width: '100%',
    height: '100%',
  },
  flexRow: {
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  inline: {
    flexDirection: 'row',
    marginRight: 10,
  },
  image: {
    width: 17,
    height: 17,
  },
  grayBackground: {
    backgroundColor: '#A8A8A8',
  },
  arrowBack: {
    padding: 10,
    left: -10,
  },
  iconHeart: {
    width: 25,
    height: 25,
  },
  iconComment: {
    width: 22,
    height: 18,
    marginTop: 4,
  },
  iconPeople: {
    width: 19,
    height: 20,
    marginLeft: 10,
    marginTop: 8,
  },
  counter: {
    paddingLeft: 7,
  },
  counterPeople: {
    paddingLeft: 5,
    marginTop: 10,
  },
  textBold: {
    fontFamily: 'gt-walsheim-bold',
    fontSize: 15,
  },
  textRegular: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
  },
  textWidth: {
    maxWidth: '100%',
    paddingRight: 50,
    paddingLeft: 10,
  },
  width: {
    width: '100%',
    alignItems: 'flex-start',
    paddingLeft: 20,
    paddingRight: 20,
  },
  colorRed: {
    color: '#F1636E',
  },
  colorGreen: {
    color: '#2ECC71',
  },
  colorOrange: {
    color: '#F89406',
  },
  colorGray: {
    color: '#4A4A4A',
  },
  colorGrayIcons: {
    color: '#A4A4A4',
  },
  colorPink: {
    color: '#FF8C72',
  },
  colorBlack: {
    color: '#000000',
  },
  borderRed: {
    borderTopColor: 'rgba(241, 99, 110, 0.5)',
  },
  borderGreen: {
    borderTopColor: 'rgba(46, 204, 113, 0.5)',
  },
  borderOrange: {
    borderTopColor: 'rgba(248, 148, 6, 0.5)',
  },
  btnColorRed: {
    color: 'rgba(241, 99, 110, 1.0)',
  },
  btnColorGreen: {
    color: 'rgba(46, 204, 113, 1.0)',
  },
  btnColorOrange: {
    color: 'rgba(248, 148, 6, 1.0)',
  },
  center: {
    textAlign: 'center',
    lineHeight: 25,
  },
  notification: {
    position: 'absolute',
    top: 85,
    right: 15,
  },
  notificationComment: {
    position: 'absolute',
    top: 22,
    right: 15,
    zIndex: 2,
  },
  notificationMenu: {
    position: 'absolute',
    top: 105,
    right: 18,
    zIndex: 3,
  },
  notificationMenuComment: {
    position: 'absolute',
    top: 120,
    right: 28,
    zIndex: 3,
  },
  menuOption: {
    width: 120,
    textAlign: 'center',
    lineHeight: 20,
    marginTop: 3,
    marginBottom: 3,
  },
  triangle: {
    position: 'absolute',
    right: 0,
    width: 26,
    height: 7,
  },
  square: {
    marginTop: 6,
    borderWidth: 1,
    borderColor: '#9B9B9B',
    backgroundColor: '#ffffff',
    color: '#ffffff',
    paddingTop: 10,
    paddingBottom: 10,
    opacity: 1,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1.0,
  },
  downArrow: {
    width: 15,
    height: 9,
    margin: 8,
  },
  circleRed: {
    width: 10,
    height: 10,
    marginRight: 4,
  },
  backgroundWhite: {
    backgroundColor: 'white',
  },
  emoji: {
    width: 15,
    height: 15,
    marginLeft: 5,
    marginTop: 5,
  },
  borderBottom: {
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 1,
    paddingBottom: 15,
    marginBottom: 13,
  },
  marginTop: {
    marginTop: 20,
  },
  description: {
    paddingBottom: 14,
    paddingTop: 12,
    opacity: 0.8,
    paddingLeft: 10,
  },
  time: {
    opacity: 0.8,
    fontFamily: 'maisonNeue-Light',
    color: '#4A4A4A',
    fontSize: 12,
    marginTop: 3,
  },
  back: {
    marginTop: 50,
    marginBottom: 20,
  },
  comment: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 0,
    width: '100%',
    borderTopWidth: 2,
  },
  textInput: {
    backgroundColor: '#ffffff',
    color: '#626262',
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
    paddingTop: 12,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 80,
    width: '100%',
  },
  btnSend: {
    paddingTop: 12,
    paddingBottom: 15,
    paddingRight: 20,
    paddingLeft: 20,
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  textSend: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 14,
    paddingBottom: 5,
  },
  listComments: {
    backgroundColor: '#F7F7F7',
    paddingTop: 15,
    paddingBottom: 65,
  },
  iconsAction: {
    marginBottom: 15,
    paddingLeft: 10,
    position: 'relative',
    zIndex: 0,
  },
  eachComment: {
    backgroundColor: '#ffffff',
    padding: 12,
    marginBottom: 8,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 4,
  },
  nameComment: {
    fontFamily: 'gt-walsheim-medium',
    fontSize: 15,
    color: '#000000',
  },
  contentComment: {
    fontFamily: 'gt-walsheim-regular',
    fontSize: 15,
    color: '#000000',
    marginTop: 8,
    marginBottom: 8,
  },
  timeComment: {
    fontFamily: 'gt-walsheim-light',
    fontSize: 13,
    color: '#626262',
  },
  flex4: {
    flex: 4,
  },
}

export default Styles
