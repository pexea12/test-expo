import React from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  TextInput,
  Keyboard,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'
import keys from 'lodash/keys'
import find from 'lodash/find'

import { listenMembers, listenComments, stopListenComments } from '../../redux/checkIn/actions'
import {
  postComment, updateHearts, deleteCheckIn, reportCheckIn, deleteComment, readCheckIn,
} from '../../configs/Firebase'
import moment from '../../configs/Moment'
import Styles from './Styles'
import BaseStyles from '../BaseStyles/Styles'
import CheckInReport from '../Feed/CheckInReport'

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 30
  return layoutMeasurement.height + contentOffset.y
        >= contentSize.height - paddingToBottom
}

class Comments extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)

    this.state = {
      comments: [],
      modal: false,
      members: this.props.navigation.state.params.members || [],
    }

    this._navigate = this.navigate.bind(this)
    this._submit = this.submit.bind(this)
    this._keyboardDidShow = this.keyboardDidShow.bind(this)
    this._keyboardDidHide = this.keyboardDidHide.bind(this)
  }

  componentWillMount() {
    this.readCheckIn()
    this.props.listenComments(this.props.navigation.state.params.checkIn.id)
    if (this.props.comments) {
      this.updateComments(this.props.comments)
    }
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.members && this.state.members.length != nextProps.members.length) {
      this.setState({ members: nextProps.members }, () => {
        this.updateComments(nextProps.comments)
      })
    }
    if (!this.props.comments || this.props.comments && this.props.comments.length != nextProps.comments.length) {
      this.updateComments(nextProps.comments)
    }
  }

  componentWillUnmount() {
    this.props.stopListenComments()
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow(e) {
    const shortHeight = e.endCoordinates.height
    this.setState({ keyboard: true, shortHeight })
  }

  keyboardDidHide() {
    this.setState({ keyboard: false })
  }

  async readCheckIn() {
    const { checkIn } = this.props.navigation.state.params
    if (!checkIn.read) {
      checkIn.read = {}
    }
    checkIn.read[this.props.user.id] = true
    try {
      await readCheckIn(checkIn.id, checkIn.read)
    } catch (e) {
      console.log(e.message)
    }
  }

  updateComments(allComments) {
    const { user } = this.props
    const { members } = this.state
    const memberIds = map(members, 'id')
    const comments = allComments.map((comment) => {
      const index = memberIds.indexOf(comment.userId)
      if (index != -1) {
        comment.name = `${members[index].first_name} ${members[index].last_name}`
      } else if (comment.userId == user.id) {
        comment.name = `${user.first_name} ${user.last_name} (You)`
      } else {
        comment.name = 'Deleted User'
      }
      return comment
    })
    this.setState({ comments: sortBy(comments, 'createdAt') })
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  option(checkIn) {
    this.setState({ menu: checkIn })
  }

  async delete(item) {
    try {
      if (item.comment) {
        await deleteComment(this.props.navigation.state.params.checkIn.id, item.id)
      } else {
        await deleteCheckIn(item.id)
        this.props.navigation.goBack()
      }
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  async report(checkIn) {
    if (!checkIn.reportedBy) {
      checkIn.reportedBy = {}
    }
    checkIn.reportedBy[this.props.user.id] = true

    try {
      await reportCheckIn(checkIn.id, checkIn.reportedBy)
      this.setState({ modal: true })
    } catch (e) {
      Alert.alert('Error', e.message)
    }

    this.setState({ menu: false })
  }

  toggleHearts(checkIn) {
    const hearts = !!checkIn.hearts && keys(checkIn.hearts)

    let myHeart = false
    if (!hearts) {
      checkIn.hearts = {}
      checkIn.hearts[this.props.user.id] = true
    } else {
      myHeart = hearts.includes(this.props.user.id)
      if (myHeart) {
        delete checkIn.hearts[this.props.user.id]
      } else {
        checkIn.hearts[this.props.user.id] = true
      }
    }
    this.setState({ checkIn })
    this.props.navigation.setParams({ checkIn })
    updateHearts(checkIn.id, checkIn.hearts)
  }

  prepareNotifications(token, members, checkIn) {
    const { user } = this.props
    return { to: token, body: `${user.first_name} ${user.last_name} commented on your post` }
  }

  submit() {
    const { members, checkIn } = this.props.navigation.state.params
    const { comment } = this.state
    const checkInUser = find(members, { id: checkIn.userId })
    const notification = checkInUser ? this.prepareNotifications(checkInUser.token, members, checkIn) : false

    try {
      postComment(
        checkIn.id,
        { comment, createdAt: Date.now(), userId: this.props.user.id },
        notification,
      )
      this.setState({ comment: '' })
      Keyboard.dismiss()
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  renderMenu() {
    const { menu } = this.state
    return (
      <View>
        <Image source={require('../../assets/images/triangle.png')} style={[Styles.triangle]} />
        <View style={Styles.square}>
          {menu.userId != this.props.user.id ? (
            <TouchableOpacity onPress={() => this.report(menu)}>
              <Text style={[Styles.textRegular, Styles.menuOption]}>Report</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => this.delete(menu)}>
              <Text style={[Styles.textRegular, Styles.menuOption]}>Delete</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity onPress={() => this.setState({ menu: false })}>
            <Text style={[Styles.colorGrayIcons, Styles.textRegular, Styles.menuOption]}>
              Cancel
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderComments() {
    const { comments, menu } = this.state
    return comments.map(comment => (
      <View>
        <View style={Styles.eachComment}>
          <Text style={Styles.nameComment}>{comment.name}</Text>
          <Text style={Styles.contentComment}>{comment.comment}</Text>
          <Text style={Styles.timeComment}>{moment(comment.createdAt).fromNow(true)}</Text>
        </View>
        <View style={Styles.notificationComment}>
          {comment.userId == this.props.user.id && (
          <TouchableOpacity style={[Styles.inline, Styles.viewHeight]} onPress={e => this.option(comment)}>
            <Image source={require('../../assets/images/down-arrow.png')} style={[Styles.downArrow]} />
          </TouchableOpacity>
          )}
        </View>
        <View style={Styles.notificationMenuComment}>
          {menu && menu.id == comment.id && this.renderMenu()}
        </View>
      </View>
    ))
  }

  showMembers(shareWith) {
    const { user } = this.props
    const { members } = this.state
    const memberIds = map(members, 'id')
    const memberNames = Object.keys(shareWith).map((memberId) => {
      const index = memberIds.indexOf(memberId)
      if (index != -1) {
        return `${members[index].first_name} ${members[index].last_name}`
      } if (memberId == user.id) {
        return `${user.first_name} ${user.last_name} (You)`
      }
      return 'Deleted User'
    })
    this.props.navigation.navigate('SharedMembers', { memberNames })
  }

  renderCheckIn() {
    const { menu, comments } = this.state
    const { checkIn } = this.props.navigation.state.params
    let color

    if (Number(checkIn.emoji.quotient) <= -0.3) {
      color = Styles.colorRed
      borderColor = Styles.borderRed
      btnTextColor = Styles.btnColorRed
    } else if (Number(checkIn.emoji.quotient) >= 0.3) {
      color = Styles.colorGreen
      borderColor = Styles.borderGreen
      btnTextColor = Styles.btnColorGreen
    } else {
      color = Styles.colorOrange
      borderColor = Styles.borderOrange
      btnTextColor = Styles.btnColorOrange
    }
    const hearts = !!checkIn.hearts && keys(checkIn.hearts)
    const uri = hearts && hearts.includes(this.props.user.id) ? require('../../assets/images/chat-love-filled.png')
      : require('../../assets/images/chat-love-empty.png')

    return (
      <View style={Styles.width}>
        <View style={[Styles.flexRow, Styles.spaceBetween, Styles.back]}>
          <TouchableOpacity
            style={Styles.arrowBack}
            onPress={() => {
              this.props.navigation.goBack()
            }}
          >
            <Image
              source={require('../../assets/images/arrow-back.png')}
              style={BaseStyles.backImage}
            />
          </TouchableOpacity>
          <Text style={Styles.time}>{moment(checkIn.createdAt).fromNow(true)}</Text>
        </View>
        <View style={Styles.flexRow}>
          <View style={[Styles.flexRow, Styles.textWidth]}>
            <Text style={[Styles.textBold, Styles.colorBlack]}>
              {checkIn.anon ? 'Anonymous' : checkIn.name}
            </Text>
            <Text style={[Styles.textRegular, Styles.colorGray]}> is feeling </Text>
            <Text style={[color, Styles.textBold]}>{checkIn.emoji.mood}</Text>
            <Image source={{ uri: checkIn.emoji.emoji_url }} style={Styles.emoji} />
          </View>
        </View>
        {checkIn.privacy != 'everyone' && (
        <View style={[Styles.flexRow]}>
          <TouchableOpacity
            style={Styles.inline}
            onPress={() => {
              this.showMembers(checkIn.shareWith)
            }}
          >
            <Image
              source={require('../../assets/images/chat-people.png')}
              style={Styles.iconPeople}
            />
            <Text style={[Styles.textRegular, Styles.counterPeople, Styles.colorGrayIcons]}>
              {checkIn.privacy}
            </Text>
          </TouchableOpacity>
        </View>
        )}
        <View style={Styles.notification}>
          <TouchableOpacity onPress={e => this.option(checkIn)}>
            <Image
              source={require('../../assets/images/down-arrow.png')}
              style={[Styles.downArrow]}
            />
          </TouchableOpacity>
        </View>
        <View style={Styles.notificationMenu}>
          {menu && menu.id == checkIn.id && this.renderMenu()}
        </View>
        <View style={[Styles.flexRow]}>
          <Text style={[Styles.description, Styles.textRegular]}>{checkIn.description}</Text>
        </View>
        <View style={[Styles.flexRow, Styles.spaceBetween, Styles.iconsAction]}>
          <TouchableOpacity
            style={Styles.inline}
            onPress={() => {
              this.toggleHearts(checkIn)
            }}
          >
            <Image source={uri} style={Styles.iconHeart} />
            <Text
              style={[
                Styles.textRegular,
                Styles.counter,
                !!hearts && hearts.includes(this.props.user.id)
                  ? Styles.colorPink
                  : Styles.colorGrayIcons,
              ]}
            >
              {hearts.length || ''}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={Styles.inline} onPress={() => {}}>
            <Image
              source={require('../../assets/images/chat-comment.png')}
              style={Styles.iconComment}
            />
            <Text style={[Styles.textRegular, Styles.counter, Styles.colorGrayIcons]}>
              {comments.length || 0}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const {
      comment, comments, count, modal, keyboard, shortHeight,
    } = this.state
    return (
      <View style={Styles.container}>
        <View style={Styles.modalView}>
          <View>{this.renderCheckIn()}</View>
          <View style={[Styles.listComments, Styles.flex4]}>
            <ScrollView ref="scrollView">
              {this.props.loading && <ActivityIndicator size="large" />}
              {!!comments.length && this.renderComments()}
              {modal && (
              <CheckInReport
                onClose={() => {
                  this.setState({ modal: false })
                }}
              />
              )}
            </ScrollView>
          </View>
          <View style={[Styles.comment, borderColor, !!keyboard && { bottom: shortHeight }]}>
            <View style={Styles.flexRow}>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Comment..."
                style={Styles.textInput}
                multiline
                onChangeText={comment => this.setState({ comment })}
                value={comment}
              />
              <TouchableOpacity style={Styles.btnSend} onPress={this._submit} disabled={!comment}>
                <Text style={[Styles.textSend, btnTextColor]}>Send</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  loading: state.checkIn.loading,
  comments: state.checkIn.comments,
  members: state.checkIn.members,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenMembers,
  listenComments,
  stopListenComments,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Comments)
