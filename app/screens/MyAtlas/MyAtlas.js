import React from 'react'
import {
  Dimensions,
  ListView,
  AppState,
  Animated,
  Easing,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import sortBy from 'lodash/sortBy'
import map from 'lodash/map'
import {
  LinearGradient,
  Notifications,
} from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import moment from '../../configs/Moment'
import Styles from './Styles'
import BaseStyles from '../BaseStyles/Styles'
import { listenPersonalCards, listenGlobalStacks } from '../../redux/checkIn/actions'
import Footer from '../Footer/Footer'
import SingleCardView from '../SingleCardView/SingleCardView'


const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const categories = ['Basics', 'Stress', 'Asking for Help', 'Thoughts']

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 30
  return layoutMeasurement.height + contentOffset.y
        >= contentSize.height - paddingToBottom
}

const analytics = new Analytics('UA-118205605-4')


class MyAtlas extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    cardStack: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
  };

  static transitionConfig = {
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  }

  constructor(props) {
    super(props)
    this.state = {
      appState: AppState.currentState,
      modal: false,
      personal_cards: [],
      stacks: [],
    }
    this._navigate = this.navigate.bind(this)
  }

  componentWillMount() {
    this.props.listenGlobalStacks()
    this.getStacks(this.props.stacks)
  }

  getStacks(stacks = []) {
    try {
      this.setState({ stacks })
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  componentDidMount() {
    this.props.listenPersonalCards(this.props.user.id)
    this.getCards(this.props.personal_cards)
    this._notificationSubscription = Notifications.addListener(this._handleNotification)
    AppState.addEventListener('change', this._handleAppStateChange)
    analytics.hit(new PageHit('MyAtlas'))
      .then(() => console.log('success'))
      .catch(e => console.log(e.message))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      if (this.props.personal_cards && nextProps.personal_cards) {
        if (this.props.personal_cards.length != nextProps.personal_cards.length) {
          this.getCards(nextProps.personal_cards)
        }
      }
    }
  }

  getCards(allCards = []) {
    const personal_cards = sortBy(allCards, 'createdAt').reverse()
    this.setState({ personal_cards })
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // analytics.event(new Event('PageView', 'Home', this.props.user ? this.props.user.id : null))
      // .then(() => console.log("success"))
      // .catch(e => console.log(e.message));
    }
    this.setState({ appState: nextAppState })
  }

  _handleNotification = (notification) => {
    // if(notification.origin == "selected") {
    //     if(notification.data.isComment) {
    //         this.props.navigation.navigate('Comments', notification.data);
    //     }
    // }
  };

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  navigateCard = (card) => {
    if (card.type == 'wonder') {
      this._navigate('WonderCompleted', { card, user: this.props.user })
    } else {
      this._navigate('ActionCompleted', { card, user: this.props.user })
    }
  };

  navigateCard = (card) => {
    if (card.type == 'wonder') {
      this._navigate('CardviewWonder', { card, user: this.props.user })
    } else {
      this._navigate('CardviewAction', { card, user: this.props.user })
    }
  };

  render() {
    const { modal, personal_cards, stacks } = this.state
    const fake_cards = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    const unique_cards = [...new Set(map(personal_cards, 'title'))]
    const cardWidth = (windowWidth - 60) / 3
    return (
      <View style={Styles.container}>
        <ScrollView style={Styles.homeList} onScroll={this._onScroll}>
          <View style={[Styles.headerHome]}>
            <View
              style={[
                Styles.homeTitle,
                {
                  flexDirection: 'row', flex: 1, justifyContent: 'space-between', width: '100%',
                },
              ]}
            >
              <View style={[Styles.image]}>
                <Image
                  style={{ width: 70, height: 70, flex: 1 }}
                  source={{ uri: this.props.user.profile_picture || '' }}
                />
              </View>
              <Text style={[Styles.mainTitle, { flex: 4, top: 20, marginLeft: 12 }]}>
                {this.props.user.first_name}
                {' '}
                {this.props.user.last_name}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this._navigate('Account')
                }}
              >
                <Image
                  source={require('../../assets/images/settings-icon.png')}
                  style={Styles.iconSettings}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={[Styles.contentContainer, { flex: 1, background: 'white', maxWidth: '100%' }]}
          >
            <View style={{ marginBottom: 30, width: '100%', flex: 1 }}>
              {personal_cards.length == 0 && (
              <View style={Styles.emptyStateWrapper}>
                <Text style={Styles.emptyState}>No cards saved yet!</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Explore')}>
                  <Text
                    style={[
                      Styles.emptyState,
                      {
                        color: '#6B68EE',
                        marginTop: 8,
                        fontFamily: 'gt-walsheim-medium',
                        opacity: 0.8,
                      },
                    ]}
                  >
                      Find your first
                  </Text>
                </TouchableOpacity>
              </View>
              )}
              {personal_cards && personal_cards.length > 0 && (
              <Text style={[Styles.resourceTitle]}>Recent Cards</Text>
              )}
              {personal_cards && (
              <FlatList
                data={personal_cards}
                style={{ flex: 1, width: '100%' }}
                renderItem={({ item: card }) => (
                  <View>
                    <SingleCardView
                      navigation={this.props.navigation}
                      card={card}
                      stacks={stacks}
                      isMyAtlas
                    />
                  </View>
                )}
              />
              )}
            </View>
          </View>
        </ScrollView>
        <Footer navigation={this.props.navigation} isMyAtlas="true" />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  personal_cards: state.checkIn.personal_cards,
  stacks: state.checkIn.stacks,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  listenPersonalCards,
  listenGlobalStacks,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyAtlas)
