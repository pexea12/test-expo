import React from 'react'
import {
  ScrollView,
  FlatList,
  Text,
  Dimensions,
  View,
  Slider,
  processColor,
  TouchableOpacity,
  Animated,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LinearGradient } from 'expo'
import { Analytics, PageHit, Event } from 'expo-analytics'
import find from 'lodash/find'
import posed from 'react-native-pose'
import Communications from 'react-native-communications'
import { listenMembers } from '../../redux/checkIn/actions'
import Header from '../../components/Header/Header'
import moment from '../../configs/Moment'
import { postComment, getMembers } from '../../configs/Firebase'
import Styles from './SavedCardDetailViewStyles'
import CardPreviewModal from '../CardPreviewModal/CardPreviewModal'

const tween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 2000,
})

const tween2 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1000,
  delay: 500,
})
const tween3 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 1200,
  delay: 700,
})
const tween4 = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 500,
  delay: 1000,
})

const fastTween = ({ value, toValue, useNativeDriver }) => Animated.timing(value, {
  toValue,
  useNativeDriver,
  duration: 500,
})

const cliffConfig = {
  state0: {
    opacity: 1.0,
    transition: tween,
  },
  state1: {
    opacity: 0.5,
    transition: tween,
  },
}

const completionConfig = {
  state0: {
    opacity: 1.0,
    transition: tween,
  },
  state1: {
    opacity: 0.0,
    transition: tween,
  },
}

const completionConfig2 = {
  state0: {
    y: windowHeight * 0.05,
    opacity: 1.0,
    transition: fastTween,
  },
  state1: {
    y: windowHeight,
    opacity: 0.0,
    transition: fastTween,
  },
}

const starConfig1 = {
  state0: {
    opacity: 1.0,
    transition: tween,
  },
  state1: {
    opacity: 0.0,
    transition: tween,
  },
}
const starConfig2 = {
  state0: {
    opacity: 1.0,
    transition: tween2,
  },
  state1: {
    opacity: 0.0,
    transition: tween2,
  },
}
const starConfig3 = {
  state0: {
    opacity: 1.0,
    transition: tween3,
  },
  state1: {
    opacity: 0.0,
    transition: tween3,
  },
}

const starConfig4 = {
  state0: {
    opacity: 1.0,
    transition: tween4,
  },
  state1: {
    opacity: 0.0,
    transition: tween4,
  },
}


const OverallWrapper = posed.View(completionConfig)
const TextinputWrapper = posed.View(completionConfig2)

const CliffWrapper = posed.View(cliffConfig)
const StarWrapper = posed.View(starConfig1)
const StarWrapper2 = posed.View(starConfig2)
const StarWrapper3 = posed.View(starConfig3)
const StarWrapper4 = posed.View(starConfig4)


const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height


class SavedCardDetailView extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      animateBox: false,
      members: [],
      isVisible: false,
      displayText: false,
      starVisible: false,
    }
    this._navigate = this.navigate.bind(this)
  }

  navigate(route, params) {
    this.props.navigation.navigate(route, params)
  }

  async getMembers() {
    try {
      this.props.listenMembers()
    } catch (e) {
      Alert.alert('Error', e.message || '')
    }
  }

  componentWillMount() {
    this.getMembers()
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isVisible: !this.state.isVisible,
      })
    }, 400)
    setTimeout(() => {
      this.setState({
        starVisible: !this.state.starVisible,
      })
    }, 460)
  }

  async getMembers() {
    try {
      const members = await getMembers(this.props.user)
      this.setState({ members })
    } catch (e) {
      Alert.alert(e.error)
    }
  }

  renderSteps = ({ item, index }) => (
    <View style={{
      flex: 1,
      paddingTop: 10,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    }}
    >
      <View style={{
        width: 27,
        height: 27,
        right: 6,
        backgroundColor: 'black',
        borderRadius: 20,
        flexDirection: 'column',
        justifyContent: 'center',
      }}
      >
        <Text style={{
          fontFamily: 'gt-walsheim-bold',
          fontSize: 12,
          color: 'white',
          textAlign: 'center',
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
        }}
        >
          {index + 1}
        </Text>
      </View>
      <Text
        selectable
        style={{
          color: '#404040',
          maxWidth: 240,
          fontFamily: 'gt-walsheim-regular',
          fontSize: 15,
          textAlign: 'left',
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
        }}
      >
        {item}
      </Text>
    </View>
  )


  renderBullets = ({ item, index }) => (
    <View style={{
      flex: 1,
      paddingTop: 10,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      width: '95%',
    }}
    >
      <View style={{
        width: 4,
        bottom: -3,
        height: 4,
        right: 6,
        backgroundColor: 'black',
        borderRadius: 4,
        flexDirection: 'column',
        justifyContent: 'center',
      }}
      />
      <Text
        selectable
        style={{
          color: '#404040',
          fontFamily: 'gt-walsheim-regular',
          fontSize: 15,
          textAlign: 'left',
        }}
      >
        {item}
      </Text>
    </View>
  )


  renderBody = ({ item, index }) => {
    let borderTop = {}
    if (index > 0) {
      borderTop = {
        borderTopWidth: 1,
        borderTopColor: '#D8D8D8',
        paddingTop: 10,
        marginTop: 15,
      }
    }
    if (item.type == 'text') {
      return (
        <View style={borderTop}>
          <Text style={{ fontSize: 15, fontFamily: 'gt-walsheim-regular', color: 'black' }}>
            {item.text}
          </Text>
        </View>
      )
    } if (item.type == 'steps') {
      return (
        <View style={borderTop}>
          <Text style={{
            color: '#404040',
            fontFamily: 'gt-walsheim-bold',
            fontSize: 15,
            textAlign: 'left',
          }}
          >
            Steps
          </Text>
          <FlatList
            style={{ backgroundColor: 'clear', left: 10 }}
            data={item.steps}
            renderItem={this.renderSteps}
          />
        </View>
      )
    } if (item.type == 'bullets') {
      return (
        <View style={borderTop}>
          <Text style={{
            color: '#404040',
            fontFamily: 'gt-walsheim-bold',
            fontSize: 15,
            textAlign: 'left',
          }}
          >
            Keep in Mind
          </Text>
          <FlatList
            style={{ backgroundColor: 'clear', left: 10 }}
            data={item.bullets}
            renderItem={this.renderBullets}
          />
        </View>
      )
    }
  }

  prepareNotifications(token) {
    return {
      to: token,
      body: 'Anonymous suggested a card for you! Try it out?',
    }
  }

  submit() {
    // this is the comment
    const { members } = this.state
    const { card, post } = this.props.navigation.state.params
    const checkInUser = find(members, { id: post.userId })
    const notification = checkInUser ? this.prepareNotifications(checkInUser.token) : false
    try {
      postComment(
        post.id,
        { cardId: card.id, createdAt: Date.now(), userId: this.props.user.id },
        notification,
      )
      this.props.navigation.goBack()
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  renderActionCard = card => (
    <View>
      <ScrollView style={{ paddingBottom: 35, zIndex: 1000 }}>
        <View style={{
          padding: 18, paddingBottom: 30, flex: 1, justifyContent: 'space-between', width: '100%', marginTop: 32,
        }}
        >
          <View>
            {card.title && <Text style={[Styles.cardTitle, { fontSize: 19, letterSpacing: 3.33 }]}>{card.title.toUpperCase()}</Text>}
            {card.subtitle && <Text style={[Styles.subtitleTitle]}>{card.subtitle}</Text>}
            {card.content && <Text style={[Styles.subtitleTitle]}>{card.content}</Text>}
            {card.caption && <Text style={[Styles.cardTitle]}>{card.caption}</Text>}
            {card.response && <Text style={[Styles.cardTitle]}>{card.response}</Text>}
            {card.subtitle_motivational && <Text style={[Styles.cardSubtitle, { opacity: 0.85 }]}>{card.subtitle_motivational}</Text>}
            {card.body
                        && (
                        <View style={{
                          backgroundColor: 'white', borderRadius: 8, padding: 17, marginTop: 20,
                        }}
                        >
                          <FlatList
                            style={{ backgroundColor: 'clear' }}
                            data={card.body}
                            renderItem={this.renderBody}
                          />
                        </View>
                        )}
          </View>
        </View>
      </ScrollView>
    </View>
  )

  renderWonderCard = (card) => {
    const { starVisible } = this.state
    return (
      <View>
        <ScrollView style={{ paddingBottom: 35, zIndex: 1000 }}>
          <View
            style={{
              padding: 18,
              paddingBottom: 30,
              justifyContent: 'space-between',
              width: '100%',
            }}
          />
          <StarWrapper
            pose={!starVisible ? 'state1' : 'state0'}
            style={{ padding: 20, height: '100%', justifyContent: 'center' }}
          >
            <Text style={Styles.questionForm}>{card.content}</Text>
          </StarWrapper>
        </ScrollView>
      </View>
    )
  };

  renderWonderCardHelper = (card) => {
    const color1 = card.color1 ? card.color1 : '#F6B05C'
    const color2 = card.color2 ? card.color2 : '#F27069'
    return (
      <LinearGradient
        colors={['#494E94', '#8385EB', '#91FAFE']}
        locations={[0, 0.7, 1]}
        start={[0.55, 0.3]}
        end={[0.7, 1]}
        style={{
          position: 'absolute',
          height: '65%',
          width: '90%',
          top: '15%',
          zIndex: -1000,
          borderRadius: 12,
          backgroundColor: 'black',
        }}
      >
        {this.renderWonderCard(card)}
      </LinearGradient>
    )
  };

  renderActionCardHelper = (card, stack) => {
    const color1 = card.color1 ? card.color1 : '#F6B05C'
    const color2 = card.color2 ? card.color2 : '#F27069'
    return (
      <CardPreviewModal
        navigation={this.props.navigation}
        response={card.response}
        card={card}
        stack={stack}
      />
    )
  };

  submit() {
    // this is the comment
    const { members } = this.state
    const { card, post } = this.props.navigation.state.params
    const checkInUser = find(members, { id: post.userId })
    const notification = checkInUser ? this.prepareNotifications(checkInUser.token) : false
    try {
      postComment(
        post.id,
        { cardId: card.id, createdAt: Date.now(), userId: this.props.user.id },
        notification,
      )
      this.props.navigation.goBack()
    } catch (e) {
      Alert.alert('Error', e.message)
    }
  }

  render() {
    const { description } = this.state
    const {
      card, stack, isComment, stacks,
    } = this.props.navigation.state.params
    const parent_stack = stacks && stacks.find(x => x.id == card.stack)

    const color1 = (parent_stack && parent_stack.color1) ? parent_stack.color1 : '#F6B05C'
    const color2 = (parent_stack && parent_stack.color2) ? parent_stack.color2 : '#F27069'
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.85)' }}>
        <View
          style={{
            alignItem: 'center',
            top: -25,
            width: '95%',
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'center',
          }}
        >
          <CardPreviewModal
            navigation={this.props.navigation}
            response={card.response}
            card={card}
            stack={stack}
          />
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 30,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {isComment ? (
            <TouchableOpacity
              style={[Styles.doneButton, { marginBottom: 12 }]}
              onPress={() => this.submit()}
            >
              <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Comment</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={[Styles.doneButton, { marginBottom: 12 }]}
              onPress={() => Communications.text(
                '',
                "Thought you'd love this card: https://itunes.apple.com/us/app/atlas-edu/id1423178843?mt=8",
              )
              }
            >
              <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Send</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity style={Styles.doneButton} onPress={() => this._navigate('MyAtlas')}>
            <Text style={[Styles.buttonText, { color: color1, letterSpacing: 0 }]}>Done</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SavedCardDetailView)
